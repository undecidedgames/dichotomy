#include "Client.h"


#pragma region Class Constructor & Destructor 

Client::Client()
{
	m_socket = nullptr;
	m_userName = "";
	m_clientNumber = -1;
	m_connected = false;

	m_ready = false;
	m_playerNumber = -1;
}


Client::~Client()
{
}


#pragma endregion


#pragma region Client Management

void Client::connect(TCPsocket socket)
{
	m_socket = socket;
}


void Client::disconnect()
{
	SDLNet_TCP_Close(m_socket);
}


bool Client::isConnected()
{
	return m_connected;
}


void Client::setUserName(std::string userName)
{
	m_userName = userName;
}


std::string Client::getUserName()
{
	return m_userName;
}


TCPsocket Client::getSocket()
{
	return m_socket;
}

#pragma endregion


#pragma region Game Management

void Client::setReady(bool toggle)
{
	m_ready = toggle;
}


bool Client::isReady()
{
	return m_ready;
}


void Client::setPlayerNumber(int number)
{
	m_playerNumber = number;
	//std::cout << m_playerNumber << std::endl;
}


int Client::getPlayerNumber()
{
	return m_playerNumber;
}

#pragma endregion