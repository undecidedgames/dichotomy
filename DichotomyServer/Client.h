#pragma once

#include <string>

#include <iostream>

#ifdef _WIN32
#include <SDL_net.h>
#elif __APPLE__
#include <SDL2_net/SDL_net.h>
#endif

class Client
{
public:
	/* Class Constructor & Destructor */
	Client();
	~Client();

public:
	/* Client Management */
	void connect(TCPsocket socket);
	void disconnect();
	bool isConnected();

	void setUserName(std::string userName);
	std::string getUserName();

	TCPsocket getSocket();

private:
	TCPsocket m_socket;
	std::string m_userName;
	unsigned int m_clientNumber;
	bool m_connected;

public:
	/* Game Managment */
	void setReady(bool toggle);
	bool isReady();

	void setPlayerNumber(int number);
	int getPlayerNumber();

private:
	/* Game Management */
	bool m_ready;

	int m_playerNumber;
};

