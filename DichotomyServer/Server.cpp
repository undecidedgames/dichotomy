#include "Server.h"


#pragma region Class Constructor & Destructor

Server::Server(unsigned int port, unsigned int bufferSize, unsigned int maxSockets)
{
	m_serverStatus = true;

	m_port = port;
	m_bufferSize = bufferSize;
	m_maxSockets = maxSockets;
	m_maxClients = maxSockets - 1;	 // Always (maxSockets - 1), socket for server is required
	m_timeOut = 5000;				// milliseconds
	m_connectedClients = 0;

	m_clientList = std::unique_ptr<Client[]>(new Client[m_maxClients]);
	m_availableSockets = std::unique_ptr<bool[]>(new bool[m_maxClients]);
	m_buffer = std::unique_ptr<char>(new char[m_bufferSize]);

	m_messageCodes[0] = "Connection accepted. (Code 0)";
	m_messageCodes[1] = "Connection denied. Server full. (Code 1)";
	m_messageCodes[2] = "Game session ready. (Code 2)";

	m_userCommands[0] = "/shutdown";
	m_userCommands[1] = "/ready";
	m_userCommands[2] = "/pause";
	m_userCommands[3] = "/resume";
	m_userCommands[4] = "/end";

	m_gameInSession = false;
	m_gamePaused = false;
}


Server::~Server(void)
{
	// Close open client sockets
	for (unsigned int index = 0; index < m_maxClients; index++)
	{
		m_clientList[index].disconnect();
	}

	// Free socket set and close server socket
	SDLNet_FreeSocketSet(m_socketSet);
	SDLNet_TCP_Close(m_serverSocket);
	SDLNet_Quit();
}

#pragma endregion


#pragma region Public Methods (Init, Update)

void Server::init()
{
	// Initialise SDL_net
	if (SDLNet_Init() == -1)
	{
		std::cout << "SDLNet_Init: " << SDLNet_GetError() << std::endl;
		exit(EXIT_FAILURE);
	}

	// Initialise socket set and resolve host
	initSocketSet();
	resolveHost();

	// Initialise client sockets
	for (size_t index = 0; index < m_maxClients; index++)
	{
		m_clientList[index] = Client();
		m_availableSockets[index] = true;
	}

	openServerSocket();
	addSocketToSet(m_socketSet, m_serverSocket);
	
	std::cout << "Waiting for incoming connections." << std::endl << std::endl;
}


bool Server::update()
{
	// Run server indefinitely
	do {
		acceptConnections();	// Accept incoming connections
		handleActivity();		// Handle connection activity

		// Set up game session once players have signified they are ready
		if (m_gameInSession == false)
		{
			if (playersReady() == true)
			{
				copyToBuffer(m_messageCodes[2].c_str());

				sendData((unsigned int)0, getBufferData());
				sendData((unsigned int)1, getBufferData());

				startSession();
			}
		}
		else
		{
			startSession();
			runGameSession();
		}

	} while (m_serverStatus);

	return m_serverStatus;
}

#pragma endregion


#pragma region Server Activity

void Server::acceptConnections()
{
	// Check for activity on socket set 
	if (m_socketSet != NULL) 
	{
		SDLNet_CheckSockets(m_socketSet, m_timeOut);
	}

	// Check for activity on server socket
	if (checkSocketActivity(m_serverSocket) == true)
	{
		// If server is not full connect new clients
		if (m_connectedClients < m_maxClients)
		{
			connectClient();
		}
		else // Server is full:
		{
			handleFullServer();
		}
	}
}


void Server::handleActivity()
{
	// Loop to check all connected clients for activity
	for (size_t clientNumber = 0; clientNumber < m_maxClients; ++clientNumber)
	{
		if (checkSocketActivity(m_clientList[clientNumber].getSocket()) == true)
		{
			// Check for incoming data, if no data received then client has disconnected
			if (receiveData(clientNumber) == false)
			{
				disconnectClient(clientNumber);
			}
			else // If client is sending data, check for commands, print buffer and broadcast to network
			{
				parseForUserCommands(clientNumber);

				printBuffer(clientNumber);

				broadcastSend(clientNumber);
			}
		}				
	} 
}


void Server::handleFullServer()
{
	std::cout << "New connection rejected. Server is currently full." << std::endl;

	// Accept the connection to handle situation
	TCPsocket tempSock = SDLNet_TCP_Accept(m_serverSocket);

	// Send client the server is full error code
	copyToBuffer(m_messageCodes[1].c_str());
	sendData(tempSock, getBufferData());

	// Close the connection
	SDLNet_TCP_Close(tempSock);
}

#pragma endregion


#pragma region Client Management

void Server::connectClient()
{
	// Find available socket 
	int availableSocket = -99;
	for (size_t index = 0; index < m_maxClients; index++)
	{
		if (m_availableSockets[index] == true)
		{
			m_availableSockets[index] = false;  // Socket is now taken
			availableSocket = index;            // Store the index
			break;                      
		}
	}

	// Accept the client connection and add socket to the set
	m_clientList[availableSocket].connect(SDLNet_TCP_Accept(m_serverSocket));
	SDLNet_TCP_AddSocket(m_socketSet, m_clientList[availableSocket].getSocket());

	m_connectedClients++;

	// Send confirmation to client
	copyToBuffer(m_messageCodes[0].c_str());
	sendData(availableSocket, getBufferData());

	// Get username from client
	receiveData(availableSocket);
	m_clientList[availableSocket].setUserName(getBufferData());
	
	// Print details of new client
	std::cout << "Client connected: " << m_clientList[availableSocket].getUserName() << " (client #" << availableSocket << ") (" << convertIP(m_clientList[availableSocket].getSocket()) << ")" << std::endl;

	printConnectedClients();
}


void Server::disconnectClient(unsigned int client)
{
	std::cout << "Client disconneted: " << m_clientList[client].getUserName() << " (client #" << client << ") (" << convertIP(m_clientList[client].getSocket()) << ")" << std::endl;

	// Remove socket from set, close and reset socket for new connection
	SDLNet_TCP_DelSocket(m_socketSet, m_clientList[client].getSocket());
	m_clientList[client].disconnect();
	m_availableSockets[client] = true;

	m_connectedClients--;

	printConnectedClients();
}


void Server::printConnectedClients()
{
	// Print details of currently connected clients

	std::cout << "(" << m_connectedClients << "/" << m_maxClients << ") client(s) currently connected." << std::endl << std::endl;
	std::cout << "Currenctly connected clients:" << std::endl;

	for (unsigned int client = 0; client < m_connectedClients; ++client)
	{
		std::cout << m_clientList[client].getUserName() << " (client #" << client << ") (" << convertIP(m_clientList[client].getSocket()) << ")" << std::endl;
	}
}

#pragma endregion


#pragma region Data Transmission Methods

bool Server::receiveData(unsigned int client)
{
	bool received = false;

	if (SDLNet_TCP_Recv(m_clientList[client].getSocket(), (void *)getBufferData(), m_bufferSize) > 0)
	{
		received = true;
	}

	return received;
}


void Server::sendData(unsigned int client, const char* buffer)
{
	if (SDLNet_TCP_Send(m_clientList[client].getSocket(), (void *)getBufferData(), m_messageLength) < m_messageLength)
	{
		std::cout << "SDLNet_TCP_Send: " << SDLNet_GetError() << std::endl;
	}
}


void Server::sendData(TCPsocket socket, const char* buffer)
{
	if (SDLNet_TCP_Send(socket, (void *)getBufferData(), m_messageLength) < m_messageLength)
	{
		std::cout << "SDLNet_TCP_Send: " << SDLNet_GetError() << std::endl;
	}
}


void Server::broadcastSend(unsigned int originClient)
{
	// Add username of origin client to beginning of message buffer
	std::stringstream ss;
	ss << ">" << m_clientList[originClient].getUserName() << " (client #" << originClient << "): " << getBufferData();
	copyToBuffer(ss.str().c_str());

	// Send message to other clients except client who sent it
	for (size_t clientNumber = 0; clientNumber < m_maxClients; ++clientNumber)
	{
		if (m_messageLength > 1 && clientNumber != originClient && m_availableSockets[clientNumber] == false)
		{
			//std::cout << "Broadcasting message " << "(" << m_messageLength << " bytes) from: " << m_clientList[originClient].getUserName() << " (client #" << originClient << ") to: " << m_clientList[index].getUserName() << " (client #" << index << ")" << std::endl << std::endl
			sendData(clientNumber, getBufferData());
		}
	}	
}


void Server::receiveGameData(unsigned int client)
{
	std::array<float, 3> tempArray;

	std::string tempString = m_buffer.get();

	tempArray[0] = tempString.at(11);
	tempArray[1] = tempString.at(13);
	tempArray[2] = tempString.at(15);
}


void Server::sendGameData(unsigned int client)
{
	std::stringstream sendStream;

	//sendStream << "playerPos(" << m_clientList[client].player.getPosition()[0] << ", " << m_clientList[client].player.getPosition()[1] << ", " << m_clientList[client].player.getPosition()[2] << ")";

	copyToBuffer(sendStream.str().c_str());
}

#pragma endregion


#pragma region Socket Methods

void Server::initSocketSet()
{
	// Allocate socket set
	if (!(m_socketSet = SDLNet_AllocSocketSet(m_maxSockets)))
	{
		std::cout << "SDLNet_AllocSocketSet: " << SDLNet_GetError() << "\n";
		exit(EXIT_FAILURE);
	}
	else
	{
		std::cout << "Allocated socket set: " << m_maxSockets << " sockets, " << m_maxClients << " client sockets." << std::endl << std::endl;
	}
}


void Server::resolveHost()
{
	// Resolve server host. Server details now held in m_serverIP, server will listen on m_port
	if (SDLNet_ResolveHost(&m_serverIP, NULL, m_port) < 0)
	{
		std::cout << "SDLNet_ResolveHost: " << SDLNet_GetError() << std::endl;
	}
	else
	{
		std::cout << "Resolved server host to: " << convertIP(m_serverIP) << std::endl << std::endl;
	}
}


void Server::openServerSocket()
{
	// Open server socket
	if (!(m_serverSocket = SDLNet_TCP_Open(&m_serverIP)))
	{
		std::cout << "SDLNet_TCP_Open: " << SDLNet_GetError() << "\n";
		exit(EXIT_FAILURE);
	}
	else
	{
		std::cout << "Server socket opened." << std::endl << std::endl;
	}
}


void Server::addSocketToSet(SDLNet_SocketSet socketSet, TCPsocket socket)
{
	SDLNet_TCP_AddSocket(socketSet, socket);
}


bool Server::checkSocketActivity(TCPsocket socket)
{
	bool hasActivity = false;

	if (socket != NULL)
	{
		// Check if server has received any data (to use SDLNet_SocketReady() a socket in a set must have had SDLNet_CheckSockets() called it on before)
		// SDLNet_CheckSockets() is called inside acceptConnections()
		if (SDLNet_SocketReady(socket) != 0)
		{
			hasActivity = true;
		}
	}

	return hasActivity;
}

#pragma endregion


#pragma region Buffer Methods

const char* Server::getBufferData()
{
	return m_buffer.get();
}


void Server::printBuffer(unsigned int client)
{
	std::cout << m_clientList[client].getUserName() << " (client #" << client << "):" << getBufferData() << std::endl << std::endl;
}


void Server::copyToBuffer(const char* data)
{
	strcpy(m_buffer.get(), data);
	m_messageLength = strlen(m_buffer.get()) + 1;	// + 1 for teminating character
}

#pragma endregion


#pragma region User Commands

void Server::parseForUserCommands(unsigned int client)
{
	/* /shutdown command */
	if (strcmp(getBufferData(), m_userCommands[0].c_str()) == 0)
	{
		m_serverStatus = false;

		std::cout << "Disconnecting all clients and shutting down." << std::endl << std::endl;
	}

	/* /ready command */
	if (strcmp(getBufferData(), m_userCommands[1].c_str()) == 0)
	{
		m_clientList[client].setReady(true);
	}

	/* /pause command */
	if (strcmp(getBufferData(), m_userCommands[2].c_str()) == 0)
	{
		pauseSession();
	}

	/* /resume command */
	if (strcmp(getBufferData(), m_userCommands[3].c_str()) == 0)
	{
		resumeSession();
	}

	/* /end command */
	if (strcmp(getBufferData(), m_userCommands[4].c_str()) == 0)
	{
		endSession();
	}
}

#pragma endregion


#pragma region IP Methods

std::string Server::convertIP(IPaddress address)
{
	// Used for printing IP address using IPaddress data type

	std::stringstream ss;
	Uint8 * dotQuad;

	dotQuad = (Uint8*)&address.host;

	ss << (unsigned short)dotQuad[0] << "." << (unsigned short)dotQuad[1] << '.' << (unsigned short)dotQuad[2] << '.' << (unsigned short)dotQuad[3] << ':' << SDLNet_Read16(&address.port);

	return ss.str();
}


std::string Server::convertIP(TCPsocket socket)
{
	// Used for printing IP address using TCPsocket data type

	std::stringstream ss;
	Uint8 * dotQuad;

	IPaddress tempIP;

	tempIP.operator=(*SDLNet_TCP_GetPeerAddress(socket));

	// Get IP in dot-quad format, break up 32-bit unsigned host address, split it into array of four 8-but unsigned numbers
	dotQuad = (Uint8*)&tempIP.host;

	// Cast to ints, read last 16 bits for port number
	ss << (unsigned short)dotQuad[0] << "." << (unsigned short)dotQuad[1] << '.' << (unsigned short)dotQuad[2] << '.' << (unsigned short)dotQuad[3] << ':' << SDLNet_Read16(&tempIP.port);

	return ss.str();
}

#pragma endregion


#pragma region Game Management

void Server::startSession()
{
	m_gameInSession = true;

	// Assign player number and notify client
	setPlayerNumber(0, 0);
	copyToBuffer("player0");
	sendData((unsigned int)0, getBufferData());

	setPlayerNumber(1, 1);
	copyToBuffer("player1");
	sendData((unsigned int)1, getBufferData());
}


void Server::pauseSession()
{
	m_gamePaused = true;
}


void Server::resumeSession()
{
	m_gamePaused = false;
}


void Server::endSession()
{
	m_gameInSession = false;
}


void Server::runGameSession()
{
	std::cout << "running game session!" << std::endl;
}


bool Server::playersReady()
{
	// Check if all players are ready

	bool ready = false;

	int numberPlayersReady = 0;

	for (unsigned int player = 0; player < m_connectedClients; ++player)
	{
		if (playerReady(player) == true)
		{
			++numberPlayersReady;
		}
	}
	
	// If all connected clients are ready, return true
	if (numberPlayersReady == m_connectedClients)
	{
		ready = true;
	}

	return ready;
}


bool Server::playerReady(unsigned int player)
{
	// Check if individual player is ready

	bool ready = false;

	if (m_clientList[player].isReady() == true)
	{
		ready = true;
	}

	return ready;
}


void Server::setPlayerNumber(unsigned int player, int number)
{
	m_clientList[player].setPlayerNumber(number);
}


int Server::getPlayerNumber(unsigned int player)
{
	return m_clientList[player].getPlayerNumber();
}

#pragma endregion