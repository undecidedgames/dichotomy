//
//  Source.cpp
//	SDLServer
//
//  Created by Martin Grant on 13/02/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "Server.h"

int main(int argc, char * argv[])
{
	Server m_dichotomyServer(1234, 512, 3);	// port, buffer size, max sockets

	// Initalise server
	m_dichotomyServer.init();

	// Run server until set to close
	while (m_dichotomyServer.update())
		continue;

	return 0;
}