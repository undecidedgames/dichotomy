#pragma once

#include <iostream>
#include <sstream>
#include <memory>
#include <string>
#include <array>

#ifdef _WIN32
#include <SDL_net.h>
#elif __APPLE__
#include <SDL2_net/SDL_net.h>
#endif

#include "Client.h"

class Server
{
public:
	/* Class Constructor & Destructor */
	Server(unsigned int port, unsigned int bufferSize, unsigned int maxSockets);
	~Server(void);

public:
	/* General Public Methods */
	void init();
	bool update();

private:
	/* General Server Member Variables */
	bool m_serverStatus;

	unsigned int m_port;         
	unsigned int m_bufferSize;    
	unsigned int m_maxSockets;    
	unsigned int m_maxClients; 
	unsigned int m_timeOut;
	unsigned int m_connectedClients;

	IPaddress m_serverIP;
	TCPsocket m_serverSocket;
	SDLNet_SocketSet m_socketSet;
	
	std::unique_ptr<Client[]> m_clientList;
	std::unique_ptr<bool[]> m_availableSockets;
	std::unique_ptr<char> m_buffer;
	int m_messageLength;

	std::array<std::string, 3> m_messageCodes;
	std::array<std::string, 5> m_userCommands;

private:
	/* Server Activity */
	void acceptConnections();
	void handleActivity();	
	void handleFullServer();

	/* Client Management */
	void connectClient();
	void disconnectClient(unsigned int client);
	void printConnectedClients();

	/* Data Transmission Methods */
	bool receiveData(unsigned int client);
	void sendData(unsigned int client, const char* buffer);
	void sendData(TCPsocket socket, const char* buffer);
	void broadcastSend(unsigned int originClient);
	void receiveGameData(unsigned int client);
	void sendGameData(unsigned int client);

	/* Socket Methods */
	void initSocketSet();
	void resolveHost();
	void openServerSocket();
	void addSocketToSet(SDLNet_SocketSet socketSet, TCPsocket socket);
	bool checkSocketActivity(TCPsocket socket);
	
	/* Buffer Methods */
	const char* getBufferData();
	void printBuffer(unsigned int client);
	void copyToBuffer(const char* data);

	/* User Commands */
	void parseForUserCommands(unsigned int client);

	/* IP Methods */
	std::string convertIP(IPaddress address);
	std::string convertIP(TCPsocket socket);

private:
	/* Game Management */
	void startSession();
	void pauseSession();
	void resumeSession();
	void endSession();
	void runGameSession();

	bool playersReady();
	bool playerReady(unsigned int player);

	void setPlayerNumber(unsigned int player, int number);
	int getPlayerNumber(unsigned int player);

private:
	/* Game Management */
	bool m_gameInSession;
	bool m_gamePaused;

};

