#include "ErrorManager.h"

void ErrorManager::logError(const char * errorMessage)
{
	time_t t = time(0);
    struct tm * currentTime = localtime(&t);

	int year = currentTime->tm_year + 1900;
	int month = currentTime->tm_mon + 1;
	int day = currentTime->tm_mday;
	int hour = currentTime->tm_hour;
	int min = currentTime->tm_min;
	int sec = currentTime->tm_sec;

	std::ostringstream intToString;   
	intToString << "Error Logs/Error Log - " << day << "-" << month << "-" << year << " - " << hour << "." << min << "." << sec << ".txt"; 
	std::string str = intToString.str();
	const char * fileName =  str.c_str();
	
	#ifdef _WIN32
	CreateDirectoryA("Error Logs", NULL);
	#endif
	
	SDL_RWops *rw = SDL_RWFromFile(fileName, "w");
	if(rw != NULL) {
		size_t len = SDL_strlen(errorMessage);
		if (SDL_RWwrite(rw, errorMessage, 1, len) != len) {
			printf("Couldn't fully write string\n");
		} else {
			printf("Wrote %d 1-byte blocks\n", len);
		}
		SDL_RWclose(rw);
	}

	std::cout << "Error logged to: " << fileName << std::endl << "Details: " << errorMessage << std::endl;
}
