// phong-tex.vert
// Vertex shader for use with a Phong or other reflection model fragment shader
// Calculates and passes on V, L, N vectors for use in fragment shader
#version 330
precision highp float;

layout (location=0) in vec3 VS_position;
layout (location=1) in vec3 VS_normal;
layout (location=2) in vec3 VS_texCoord;
layout (location=3) in vec3 VS_tangent;
layout (location=4) in vec3 VS_bitangent;

uniform sampler2D diffuseTexture;
uniform sampler2D normalTexture;
uniform sampler2D specularTexture;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

smooth out vec3 ex_V;
smooth out vec2 ex_texCoord;
smooth out mat3 ex_tbnMatrix;

void main(void) 
{
	mat4 modelViewMatrix = viewMatrix * modelMatrix;

	mat3 normalMatrix = transpose(inverse(mat3(modelViewMatrix)));
	
	// vertex into eye space
	ex_V = vec3(modelViewMatrix * vec4(VS_position,1.0f));
			
	// surface normal in eye space
	vec3 ex_N = normalMatrix * VS_normal;

	// tangent in eye space
	vec3 ex_T = normalMatrix * VS_tangent;

	// bitangent in eye space
	vec3 ex_B = normalMatrix * VS_bitangent;
	
	ex_tbnMatrix = mat3(ex_T, ex_B, ex_N);

	// texture UV (passthrough)
	ex_texCoord = VS_texCoord.xy;

	// fragment position in clip space
    gl_Position = projectionMatrix * vec4(ex_V,1.0f);
	//gl_Position = vec4(0.0f,0.0f,-0.5f,1.0f);
}