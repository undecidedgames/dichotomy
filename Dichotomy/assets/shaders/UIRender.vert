// passThrough.vert
// To be used with a -1,1 screen aligned quad.

#version 330
precision highp float;

in vec2 in_Position;

uniform mat4 modelMatrix;

smooth out vec2 texCoord;

void main(void) 
{
	texCoord = (in_Position + 1) * 0.5;
	texCoord.y = 1 - texCoord.y;
	gl_Position = modelMatrix * vec4(in_Position,1.0f,1.0f);
}