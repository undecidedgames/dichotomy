#version 330
precision highp float;

uniform sampler2D g_diffuse;
uniform sampler2D g_normal;
uniform sampler2D g_specular;
uniform sampler2D g_position;
uniform sampler2D g_phong;
uniform sampler2D g_luminance;
uniform int type;
uniform float inverseGamma;
uniform float exposure;

in vec2 texCoord;

layout (location = 0) out vec4 out_colour;

void main()
{
	float alpha = texture2D(g_diffuse, texCoord).a;
	if(alpha < 0.005f) discard;
	vec4 in_color;
	if (type==0) // Optimised Haarm-Peter Duiker tonemapping
	{
		in_color = vec4(texture2D(g_phong, texCoord)) * exposure;
		vec3 x = max(vec3(0.0),in_color.rgb-0.004);
		in_color = vec4((x*(6.2*x+.5))/(x*(6.2*x+1.7)+0.06),alpha);
   	}
	if (type==1) in_color = vec4(texture2D(g_phong, texCoord).rgb, alpha); // Pre-tonemapping
	if (type==2) in_color = vec4(texture2D(g_diffuse, texCoord)); // Albedo
	if (type==3) in_color = vec4(texture2D(g_normal, texCoord).rgb, alpha); // Normals
	if (type==4) in_color = vec4(texture2D(g_specular, texCoord).rgb, alpha); // Specular 
	if (type==5) in_color = vec4(texture2D(g_normal, texCoord).aaa,alpha); // Glossiness
	if (type==6) in_color = vec4(texture2D(g_position, texCoord).rgb,alpha); // Positions
	if (type==7) in_color = vec4(texture2D(g_phong, texCoord).aaa,alpha); // Luminance (greyscale brightness)
	
	// output gamma correct fragment color
	if(type != 0 ) out_colour = vec4(pow(in_color.rgb,vec3(inverseGamma)),alpha); // if rendering the tonemapped scene, don't correct for gamma as it's built into the tonemapping algorithm
	else out_colour = in_color;

}