// phong-tex.vert
// Vertex shader for use with a Phong or other reflection model fragment shader
// Calculates and passes on V, L, N vectors for use in fragment shader
#version 330
precision highp float;

layout (location=0) in vec3 VS_position;
layout (location=1) in vec3 VS_normal;
layout (location=2) in vec3 VS_texCoord;
layout (location=3) in vec3 VS_tangent;
layout (location=4) in vec3 VS_bitangent;

uniform sampler2D diffuseTexture;
uniform sampler2D normalTexture;
uniform samplerBuffer model_matrix_tbo;
uniform sampler2D specularTexture;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

smooth out vec3 ex_V;
smooth out vec2 ex_texCoord;
smooth out vec3 ex_N;
smooth out vec3 ex_T;
smooth out vec3 ex_B;

void main(void) 
{
	// fetch modelMatrix from tbo
	vec4 col1 = texelFetch(model_matrix_tbo,gl_InstanceID * 4);
	vec4 col2 = texelFetch(model_matrix_tbo,gl_InstanceID * 4 + 1);
	vec4 col3 = texelFetch(model_matrix_tbo,gl_InstanceID * 4 + 2);
	vec4 col4 = texelFetch(model_matrix_tbo,gl_InstanceID * 4 + 3);

	mat4 modelMatrix = mat4(col1,col2,col3,col4);
	mat4 modelViewMatrix = viewMatrix * modelMatrix;

	mat3 normalMatrix = transpose(inverse(mat3(modelViewMatrix)));
	
	// vertex into eye space
	ex_V = vec3(modelViewMatrix * vec4(VS_position,1.0f));
			
	// surface normal in eye space
	ex_N = normalize(normalMatrix * VS_normal);

	// tangent in eye space
	ex_T = normalize(normalMatrix * VS_tangent);

	// bitangent in eye space
	ex_B = normalize(normalMatrix * VS_bitangent);

	// texture UV (passthrough)
	ex_texCoord = VS_texCoord.xy;

	// fragment position in clip space
    gl_Position = projectionMatrix * vec4(ex_V,1.0f);
	//gl_Position = vec4(0.0f,0.0f,-0.5f,1.0f);
}