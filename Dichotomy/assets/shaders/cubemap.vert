#version 330
precision highp float;

layout (location=0) in vec3 VS_position;
layout (location=1) in vec3 VS_normal;
layout (location=2) in vec3 VS_texCoord;
layout (location=3) in vec3 VS_tangent;
layout (location=4) in vec3 VS_bitangent;

uniform mat4 projectionMatrix;
uniform mat3 viewMatrix;
 
smooth out vec3 ex_texCoord;

void main() 
{
	// pass-through texture UV
	ex_texCoord = vec3(VS_position.xyz);
	
	// get vertex position in clipspace then force it to draw at the farclip so that it looks correct on 3d displays
	vec4 position = projectionMatrix * mat4(viewMatrix) * vec4(VS_position,1.0f);
		
	//gl_Position = vec4(position.xyw, position.w + 0.001f);
	gl_Position = position;
}