// Fragment shader for use with defered rendering and multiple light sources
// slightly modified version of code from OpenGL Programming Guide 8th ed, page 378
#version 330
precision highp float;
#define pi 3.1415926535897932384626433832795

struct LightProperties {
int isEnabled;
int isLocal;
int isSpot;
vec3 ambient; // a single pointlight will be used to control the ambient level
vec3 color;
vec3 position;
vec3 halfVector;
vec3 coneDirection;
float spotCosCutoff;
float spotExponent;
float range;
float falloff;
};

uniform LightProperties LightArray[15];
uniform sampler2D g_diffuse;
uniform sampler2D g_normal;
uniform sampler2D g_specular;
uniform sampler2D g_position;
uniform samplerCube g_envMap;
uniform samplerCube g_iradMap;
uniform mat3 inverseViewMatrix;
uniform vec2 screenSize;
uniform int LightArraySize;

in vec2 texCoord;

layout (location = 0) out vec4 out_phong;

vec3 FresnelSchlickWithRoughness(vec3 SpecularColor,vec3 E, vec3 N, float Gloss)
{
    return SpecularColor + (max(vec3(Gloss,Gloss,Gloss), SpecularColor) - SpecularColor) * pow(1 - clamp(dot(E, N),0.0f,1.0f), 5);
}

vec3 FresnelSchlick(vec3 specularColor,vec3 E,vec3 H)
{
    return specularColor + (1.0f - specularColor) * pow(1.0f - clamp(dot(E, H),0.0f,1.0f), 5);
}

void main()
{	
	// calculate texture coordinate
	vec2 uv;
	//const vec2 screenSize = vec2(1280,720); // this needs changed to take current screen resolution
	uv = vec2(gl_FragCoord.xy / screenSize);
	

	// get fragment diffuse
	vec4 fragDiffuse = texture(g_diffuse,uv);
	
	// get fragment specularColor
	vec3 specularColor = texture(g_specular,uv).rgb;

	// get fragment position
	vec3 fragPosition = texture(g_position,uv).xyz;
	vec3 normalisedFragPosition = normalize(fragPosition);
	
	// get fragment normal with specular shininess packed into the alpha channel
	vec4 Normal = texture(g_normal,uv);
	vec3 fragNormal = Normal.rgb;
	fragNormal = normalize(fragNormal);
	float glossiness = Normal.a;
	float specularCosinePower = glossiness;
	specularCosinePower = pow(10 * (specularCosinePower) + 1,2); // map to [2..2048]
	
	// get envmap texel
	vec3 reflected = reflect(normalisedFragPosition.xyz,fragNormal.xyz);
	reflected = inverseViewMatrix * reflected;
	vec3 envMap = texture(g_envMap,reflected).rgb;
	vec3 envVector = inverseViewMatrix * fragNormal;
	
	vec3 iradMap = texture(g_iradMap,envVector).rgb;
	
	
	vec3 scatteredLight = vec3(0.0f);
	vec3 reflectedLight = vec3(0.0f);
	float specular = 0.0f;
	vec3 fresnel = vec3(0.0f);

	vec3 reflectVec = vec3(0.0f);
	vec3 lightDir = vec3(0.0f);
	float spotCos = 0.0f;
	float specularOut = 0.0f;
	float diffuseOut = 0.0f;
		
	// loop over all lights
	for (int index = 0; index < LightArraySize; ++index)
	{
		// check light is enabled
		if (LightArray[index].isEnabled == 1) 
		{
			vec3 halfVector;
			// initialise definite per-light variables
			vec3 lightDirection = vec3(0.0f);
			float attenuation = 1.0f;
			
			// check if light is local, directional lights are not local
			if (LightArray[index].isLocal == 1)
			{
				// calculate vector from fragment to light, store the distance then normalise the vector
				lightDirection = LightArray[index].position - fragPosition;
				float lightDistance = length(lightDirection);
				
				if (lightDistance > LightArray[index].range) // if distance to light is greater than light's range, skip to next light.
					continue;
					
				lightDirection = lightDirection / lightDistance;
				
				// calculate attenuation
				//attenuation =  1.0f / (LightArray[index].constantAttenuation + 
				//						LightArray[index].linearAttenuation  * lightDistance +
				//						LightArray[index].quadraticAttenuation * lightDistance * lightDistance );
										
				attenuation = pow(max(0,1 - (lightDistance / LightArray[index].range)), LightArray[index].falloff + 1);				
										
										
				
				// check if light is spot
				if(LightArray[index].isSpot == 1)
				{
					vec3 coneForce = LightArray[index].coneDirection;
					// calcuate dot between light vector and the spotlight cone
					float spotCos = dot(lightDirection, normalize(-coneForce));

					// if the difference is not within the angle of the cone, attenuation is 0 otherwise calculate attenuation
					if (spotCos < LightArray[index].spotCosCutoff) attenuation = 0.0;
					else
					{
						attenuation *= pow(spotCos, LightArray[index].spotExponent);
					}
				}
				halfVector = normalize(lightDirection + (-normalisedFragPosition));
			}
			else
			{
				lightDirection = normalize(LightArray[index].position);
				halfVector = normalize(lightDirection + (-normalisedFragPosition));
			}

			float NdotL = clamp(dot(fragNormal, lightDirection),0.0f,1.0f); // (N.L)
			
			fresnel = FresnelSchlick(specularColor,halfVector,-normalisedFragPosition);
						
			specular = clamp(dot(fragNormal, halfVector),0.0f,1.0f); // (N.H)
			specular = pow(specular, specularCosinePower); // (H.H)^gloss
			specular = specular * ((specularCosinePower + 2)/ (8*pi)); // (gloss+2/8pi)(H.H)^gloss , this is the normalisation step
			
			
			// albedo * light (the albedo multiplication is done per-fragment instead of per-light, this line simply additively stacks the lights intensities)
			scatteredLight +=	LightArray[index].ambient +
								((LightArray[index].color * pi) * attenuation * NdotL);
			
			// fresnel * normalisation * (n.h)^specPower * light
			reflectedLight += 	(LightArray[index].color * pi * attenuation) * fresnel * specular * NdotL; // temp specular color to match stone
			
		}
		
	}
	
	
	//float NdotL = dot(fragNormal, -normalisedFragPosition);
	//vec3 environmentalFresnel = specularColor + (1 - specularColor) * pow(1 - dot(fragNormal, -normalisedFragPosition), 5);
	vec3 environmentalReflection = 	((specularCosinePower + 2 )/ (8 * pi)) *  
									envMap * FresnelSchlickWithRoughness(specularColor, -normalisedFragPosition, fragNormal, glossiness) * 
									//environmentalFresnel * 
									clamp(dot(fragNormal, -normalisedFragPosition),0.0f,1.0f); 
									//NdotL;
	//reflectedLight + 
	vec3 albedo = (fragDiffuse.rgb/pi);
	// fragment color is the sum of the total scattered and reflected light
	out_phong = vec4(	albedo * scatteredLight+
						reflectedLight + 
						albedo * iradMap +
						environmentalReflection,
						fragDiffuse.a);
		
	float luminance= 0.2126f * out_phong.r + 0.7152f * out_phong.g + 0.0722f * out_phong.b;
	if(fragDiffuse.a < 0.005f) luminance = 0.5f;
	out_phong.a = luminance;
}
