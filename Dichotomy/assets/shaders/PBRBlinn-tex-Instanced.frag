// Phong fragment shader phong-tex.frag matched with phong-tex.vert
#version 330

// Some drivers require the following
precision highp float;

layout (location = 0) out vec4 out_diffuse;
layout (location = 1) out vec4 out_normal;
layout (location = 2) out vec4 out_specular;
layout (location = 3) out vec4 out_position;

smooth in vec2 ex_texCoord;
smooth in vec3 ex_V;
smooth in mat3 ex_tbn;

uniform sampler2D diffuseTexture;
uniform sampler2D normalTexture;
uniform sampler2D specularTexture;
uniform samplerBuffer model_matrix_tbo;

 
void main(void) 
{
	// diffuse colour (already gamma correct)
	out_diffuse = texture2D(diffuseTexture, ex_texCoord);
	out_specular = vec4(texture2D(specularTexture,ex_texCoord).rgb,1.0f);
	
	// normal map
	vec3 normalMap = texture2D(normalTexture,ex_texCoord).xyz;
	normalMap = 2 * normalMap - 1.0f;
	normalMap = normalize(normalMap);
	normalMap = ex_tbn * normalMap;
	
	float shininess = texture2D(normalTexture,ex_texCoord).a; // get shininess from normals alpha channel
	out_normal = vec4((normalMap), shininess); 

	out_position = vec4(ex_V,1.0f);
}