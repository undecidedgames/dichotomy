// Phong fragment shader phong-tex.frag matched with phong-tex.vert
#version 330

// Some drivers require the following
precision highp float;

layout (location = 0) out vec4 out_diffuse;
layout (location = 1) out vec4 out_normal;
layout (location = 2) out vec4 out_specular;
layout (location = 3) out vec4 out_position;

smooth in vec2 ex_texCoord;
smooth in vec3 ex_V;
smooth in vec3 ex_N;
smooth in vec3 ex_T;
smooth in vec3 ex_B;

uniform sampler2D diffuseTexture;
uniform sampler2D normalTexture;
uniform samplerBuffer model_matrix_tbo;
uniform sampler2D specularTexture;
 
void main(void) 
{
	// diffuse colour (already gamma correct)
	out_diffuse = texture2D(diffuseTexture, ex_texCoord);
	out_specular = vec4(texture2D(specularTexture,ex_texCoord).rgb,1.0f);
		
	// tangent space to object space matrix
	mat3 tbn = mat3(ex_T, ex_B, ex_N);

	// normal map
	vec3 normalMap = texture2D(normalTexture,ex_texCoord).xyz;
	normalMap = 2 * normalMap - 1.0f;
	normalMap = tbn * normalMap;
	normalMap = normalMap;
	out_normal = vec4((normalMap), 2.0f);

	out_position = vec4(ex_V,1.0f);
}