// passThrough.vert
// To be used with a -1,1 screen aligned quad.

#version 330
precision highp float;

in vec2 in_Position;

out vec2 texCoord;

void main(void) 
{
	texCoord = (in_Position + 1) * 0.5;
	gl_Position = vec4(in_Position,0.0f,1.0f);
}