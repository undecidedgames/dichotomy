#version 330
precision highp float; 

uniform samplerCube cubeMap;
uniform sampler2D g_diffuse;

smooth in vec3 ex_texCoord;

layout (location = 0) out vec4 out_phong;
 
void main (void) 
{
	vec3 fragColor = texture(cubeMap, ex_texCoord).rgb;
	
	// fetch already gamma corrected cubemap texel
	const vec2 screenSize = vec2(1280,720); // this needs changed to take current screen resolution
	vec2 uv = vec2(gl_FragCoord.xy / screenSize);
	float alphaMask = texture(g_diffuse,uv).a;

	out_phong = vec4(fragColor,1-alphaMask);
}