// Fragment shader for use with defered rendering and multiple light sources
// slightly modified version of code from OpenGL Programming Guide 8th ed, page 378
#version 330
precision highp float;

struct LightProperties {
int isEnabled;
int isLocal;
int isSpot;
vec3 ambient; // a single pointlight will be used to control the ambient level
vec3 color;
vec3 position;
vec3 halfVector;
vec3 coneDirection;
float spotCosCutoff;
float spotExponent;
float range;
float falloff;
};

uniform LightProperties LightArray[6];
uniform sampler2D g_diffuse;
uniform sampler2D g_normal;
uniform sampler2D g_specular;
uniform sampler2D g_position;

in vec2 texCoord;

layout (location = 0) out vec4 out_phong;

void main()
{	
	const vec2 screenSize = vec2(1280,720); // this needs changed to take current screen resolution
	
	// calculate texture coordinate
	vec2 uv = texCoord;

	// get fragment diffuse
	vec4 fragDiffuse = texture(g_diffuse,uv);
	
	// get fragment specularColor
	vec3 specularColor = texture(g_specular,uv).rgb;

	// get fragment position
	vec3 fragPosition = texture(g_position,uv).xyz;
	
	// get fragment normal with specular shininess packed into the alpha channel
	vec4 Normal = texture(g_normal,uv);
	vec3 fragNormal = Normal.rgb;
	float specluarExponent = Normal.a;
	
	vec3 scatteredLight = vec3(0.0f);
	vec3 reflectedLight = vec3(0.0f);

	vec3 reflectVec = vec3(0.0f);
	vec3 lightDir = vec3(0.0f);
	float spotCos = 0.0f;
	float specularOut = 0.0f;
	float diffuseOut = 0.0f;
		
	// loop over all lights
	for (int index = 0; index < 6; index++)
	{
		// check light is enabled
		if (LightArray[index].isEnabled == 1) 
		{
			
			// initialise definite per-light variables
			vec3 lightDirection = vec3(0.0f);
			float attenuation = 1.0f;
				
			// check if light is local, directional lights are not local
			if (LightArray[index].isLocal == 1)
			{
				// calculate vector from fragment to light, store the distance then normalise the vector
				lightDirection = LightArray[index].position - fragPosition;
				float lightDistance = length(lightDirection);
				lightDirection = lightDirection / lightDistance;
				
				// calculate attenuation
				//attenuation =  1.0f / (LightArray[index].constantAttenuation + LightArray[index].quadraticAttenuation * lightDistance * lightDistance );
				attenuation = pow(max(0,1 - (lightDistance / LightArray[index].range)), LightArray[index].falloff + 1);	
				
				// check if light is spot
				if(LightArray[index].isSpot == 1)
				{
					vec3 coneForce = LightArray[index].coneDirection;
					// calcuate dot between light vector and the spotlight cone
					float spotCos = dot(lightDirection, normalize(-coneForce));

					// if the difference is not within the angle of the cone, attenuation is 0 otherwise calculate attenuation
					if (spotCos < LightArray[index].spotCosCutoff) attenuation = 0.0;
					else
					{
						attenuation *= pow(spotCos, LightArray[index].spotExponent);
					}
				}
				
			}
			else
			{
				lightDirection = normalize(LightArray[index].position);
			}

			// calculate reflect vector	
			reflectVec = normalize(reflect(-lightDirection,fragNormal));

			// calculate diffuse and specular contributions
			float diffuse = max(0.0, dot(fragNormal, lightDirection));
			float specular = max(0.0, dot(reflectVec,  normalize(-fragPosition)));
						
			if (diffuse == 0.0) 
			{
				specular = 0.0;
			}
			else 
			{
				specular = pow(specular, specluarExponent) * 1.0f;
			}
		
			// add this lights contribution to the per-fragment values
			scatteredLight +=	LightArray[index].ambient +
								(LightArray[index].color * diffuse * attenuation);

			reflectedLight +=	LightArray[index].color * specular * attenuation;
		}

	}
	
	// fragment color is the sum of the total scattered and reflected light multiplied by it's diffuse color
	out_phong = vec4(min(fragDiffuse.rgb * (scatteredLight + reflectedLight), vec3(1.0f)),fragDiffuse.a);
}
				 

