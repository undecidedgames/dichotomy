#version 330
precision highp float;

uniform sampler2D UIElement;

smooth in vec2 texCoord;

layout (location = 0) out vec4 out_colour;

void main()
{
	vec4 in_color = vec4(texture2D(UIElement, texCoord));
	//if(in_color.a < 0.1f) discard;
	out_colour = in_color;
}