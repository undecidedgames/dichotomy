#include "Pathfinding.h"


Pathfinding::Pathfinding()
{
}


Pathfinding::~Pathfinding()
{
}


// Compares Nodes total cost to determine if left node is less than the right node
bool compareNodes(std::shared_ptr<MapNode> lhs, std::shared_ptr<MapNode> rhs)
{
    return ( lhs->getTotalCost() < rhs->getTotalCost() );
}


bool Pathfinding::findPath(std::shared_ptr<MapNode> startNode, std::shared_ptr<MapNode> targetNode, std::shared_ptr<std::vector<std::vector<std::shared_ptr<MapNode>>>> map)
{
	m_path.clear();

	m_foundGoal = false;
	m_currentCost = 0;

	//std::array<MapNode, 8> neighbouringNodes;	// each tiles 8 neighbours

	std::shared_ptr<MapNode> currentNode = startNode;	// Set currentNode

	// Calculate costs (F, G, H)

	calculateHeuristicCost(currentNode, targetNode);	// H
	calculateTotalCost(currentNode);					// F

	m_openNodes.push_back(currentNode);	// Add CurrentNode to list of open nodes

	while(!m_openNodes.empty())	// While there are still possible Nodes to explore
	{
		// Sort openNodes by total cost (Lowest F is at the front)
		std::sort( m_openNodes.begin(), m_openNodes.end(), compareNodes );
		// Reverse sorted openNodes (Lowest F now at the back)
		std::reverse( m_openNodes.begin(), m_openNodes.end() );

		currentNode = m_openNodes.back();	// Current node is the node in the openNodes with the lowest F
		m_openNodes.pop_back();	// Pop current node from list of open nodes
		m_closedNodes.push_back(currentNode);	// Add current node to list of closed nodes

		if(currentNode->getPosition() == targetNode->getPosition())
		{
			m_foundGoal = true;
			break;	// Path has been found so exit loop
		}

		// Add Valid Neighbours		/** TODO: Possibly change this into two for loops **/
		isValidNeighbour(currentNode, map->at(currentNode->getPosition().x+1).at(currentNode->getPosition().y+1), targetNode, true);	// Top Right
		isValidNeighbour(currentNode, map->at(currentNode->getPosition().x+1).at(currentNode->getPosition().y), targetNode, false);		// Right
		isValidNeighbour(currentNode, map->at(currentNode->getPosition().x+1).at(currentNode->getPosition().y-1), targetNode, true);	// Bottom Right
		isValidNeighbour(currentNode, map->at(currentNode->getPosition().x).at(currentNode->getPosition().y-1), targetNode, false);		// Bottom
		isValidNeighbour(currentNode, map->at(currentNode->getPosition().x-1).at(currentNode->getPosition().y-1), targetNode, true);	// Bottom Left
		isValidNeighbour(currentNode, map->at(currentNode->getPosition().x-1).at(currentNode->getPosition().y), targetNode, false);		// Left
		isValidNeighbour(currentNode, map->at(currentNode->getPosition().x-1).at(currentNode->getPosition().y+1), targetNode, true);	// Top Left
		isValidNeighbour(currentNode, map->at(currentNode->getPosition().x).at(currentNode->getPosition().y+1), targetNode, false);		// Top
		
		m_currentCost = currentNode->getCostFromStart();	// Get Current Cost (G)

		if(m_openNodes.empty())
		{
			m_foundGoal = false;
		}
	}

	if(m_foundGoal)
	{
		while(m_closedNodes.empty() == false)
		{
			//Reset Node values
			m_closedNodes.back()->setCostFromStart(0.0f);
			m_closedNodes.back()->setHeuristicCost(0.0f);
			m_closedNodes.back()->setTotalCost(0.0f);
			std::shared_ptr<MapNode> temp = m_closedNodes.back();	// Set temp to equal back of closedNode
			m_path.push_back(temp->getVec3Position());									// Add node to path
			m_closedNodes.pop_back();								// Remove back of closedNode
		}


	}
	
	while(m_openNodes.empty() == false)
	{
		//Reset Node values
		m_openNodes.back()->setCostFromStart(0.0f);
		m_openNodes.back()->setHeuristicCost(0.0f);
		m_openNodes.back()->setTotalCost(0.0f);

		// Remove waypoint from openNodes
		m_openNodes.pop_back();
	}

	return m_foundGoal;
}


std::vector< glm::vec3 > Pathfinding::getPath()
{
	return m_path;
}


void Pathfinding::calculateCostFromStart(std::shared_ptr<MapNode> currentNode)
{
	// Current node cost (G) = current node weight + previous total current cost
	currentNode->setCostFromStart( float((currentNode->getWeight() + m_currentCost)) );	// G
}


void Pathfinding::calculateHeuristicCost(std::shared_ptr<MapNode> currentNode, std::shared_ptr<MapNode> targetNode)
{
	// Calculates the Euclidean distance between current and target nodes
	// Then calculates the cost to travel from current to target nodes
	float dx = abs(currentNode->getPosition().x - targetNode->getPosition().x);
    float dy = abs(currentNode->getPosition().y - targetNode->getPosition().y);

    currentNode->setHeuristicCost(sqrt(dx * dx + dy * dy) );	// H
}


void Pathfinding::calculateTotalCost(std::shared_ptr<MapNode> currentNode)
{
	// F = G + H
	currentNode->setTotalCost( (currentNode->getCostFromStart() + currentNode->getHeuristicCost()) );	// F
}


void Pathfinding::isValidNeighbour(std::shared_ptr<MapNode> currentNode, std::shared_ptr<MapNode> neighbourNode, std::shared_ptr<MapNode> targetNode, bool isDiagonal)
{
	float newWeight = 0;

	if(isDiagonal == true)
	{
		newWeight = neighbourNode->getWeight() * 0.5;
		neighbourNode->setWeight(newWeight);
	}

	// Calculate costs (F, G, H)
	calculateCostFromStart(neighbourNode);				// G
	calculateHeuristicCost(neighbourNode, targetNode);	// H
	calculateTotalCost(neighbourNode);					// F

	neighbourNode->setWeight(-newWeight);

	bool isInOpenNodes = false;
	bool isInClosedNodes = false;

	if(neighbourNode->getAccessability() == true)	// Is node accessable
	{
		if(m_openNodes.empty() == false)
		{
			size_t index;
			for(index = 0; index < m_openNodes.size(); ++index)	// Is neighborNode in closedNodes
			{
				if(neighbourNode->getPosition() == m_openNodes[index]->getPosition())
				{
					// if (neighbor node is in closed set and current cost to neighbor < cached cost to neighbor)
					if(currentNode->getCostFromStart() < neighbourNode->getCostFromStart())
					{
						// Move node to back of the vector then remove the node from the vector
						std::swap(m_openNodes[index],m_openNodes.back());
						m_openNodes.pop_back();	// remove neighbor from closed
					}

					isInOpenNodes = true;
				}
			}
			for(index = 0; index < m_closedNodes.size(); ++index)	// Is currentNode in closedNodes
			{
				if( neighbourNode->getPosition() == m_closedNodes[index]->getPosition() )
				{
					// if (neighbor node is in closed set and current cost to neighbor < cached cost to neighbor)
					if(currentNode->getCostFromStart() < neighbourNode->getCostFromStart())
					{
						// Move node to back of the vector then remove the node from the vector
						std::swap(m_closedNodes[index],m_closedNodes.back());
						m_closedNodes.pop_back();	// remove neighbor from closed
					}

					isInClosedNodes = true;
				}
			}
			if(isInClosedNodes == false && isInOpenNodes == false)
			{
				m_openNodes.push_back(neighbourNode);				// add currentNode to openNodes
			}
		}
		else
		{
			m_openNodes.push_back(neighbourNode);				// add currentNode to openNodes
		}
	}
}
