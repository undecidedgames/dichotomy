#pragma once

#include <memory>

#include "Service.h"
#include "../Input/Input.h"

class ServiceManager
{
public:
	/* Class Constructor & Destructor */
	ServiceManager(void);
	~ServiceManager(void);

public:
	/* Service Management */
	void registerService(std::shared_ptr<Input> newService);
	std::shared_ptr<Input> getInput();

private:
	std::shared_ptr<Input> m_inputService;
};

