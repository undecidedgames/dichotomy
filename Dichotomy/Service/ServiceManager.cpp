#include "ServiceManager.h"


#pragma region Class Constructor & Destructor

ServiceManager::ServiceManager(void)
{
}


ServiceManager::~ServiceManager(void)
{
}

#pragma endregion


#pragma region Service Management

void ServiceManager::registerService(std::shared_ptr<Input> newService)
{
	m_inputService = newService;
}


std::shared_ptr<Input> ServiceManager::getInput()
{
	return m_inputService;
}

#pragma endregion