#include "ShaderManager.h"


ShaderManager::ShaderManager(std::shared_ptr<ConfigManager> configManager)
{
	std::cout << "RenderManager constructor called." << std::endl;

	m_configManager = configManager;
	std::cout << "shaderManager constructor called" << std::endl;
	loadShader("assets/shaders/phong-tex-Instanced.vert","assets/shaders/phong-tex-Instanced.frag", "phong-tex-instanced");
	loadShader("assets/shaders/PBRBlinn-tex-Instanced.vert","assets/shaders/PBRBlinn-tex-Instanced.frag", "PBRBlinn-tex-instanced");
	loadShader("assets/shaders/PBRBlinn-tex.vert","assets/shaders/PBRBlinn-tex.frag", "PBRBlinn-tex");
	loadShader("assets/shaders/passThrough.vert","assets/shaders/passThroughColor.frag","passThroughColor");
	loadShader("assets/shaders/passThrough.vert","assets/shaders/multiLight-Phong.frag","multiLightPhong");
	loadShader("assets/shaders/passThrough.vert","assets/shaders/multiLight-PBRBlinn.frag","multiLightPBRBlinn");
	loadShader("assets/shaders/cubemap.vert","assets/shaders/cubemap.frag","cubemap");
	loadShader("assets/shaders/UIRender.vert","assets/shaders/UIRender.frag","UIRender");
	
	glUseProgram(m_shadermap["phong-tex-instanced"]);
	GLint baseImageLoc = glGetUniformLocation(m_shadermap["phong-tex-instanced"], "diffuseTexture");
	GLint normalMapLoc = glGetUniformLocation(m_shadermap["phong-tex-instanced"], "normalTexture");
	GLint specularMapLoc = glGetUniformLocation(m_shadermap["phong-tex-instanced"], "specularTexture");
	GLint FBOLoc = glGetUniformLocation(m_shadermap["phong-tex-instanced"], "model_matrix_tbo");
	glUniform1i(baseImageLoc, 0); 
	glUniform1i(normalMapLoc, 1);
	glUniform1i(specularMapLoc,2);
	glUniform1i(FBOLoc, 3);

	glUseProgram(m_shadermap["PBRBlinn-tex-instanced"]);
	baseImageLoc = glGetUniformLocation(m_shadermap["PBRBlinn-tex-instanced"], "diffuseTexture");
	normalMapLoc = glGetUniformLocation(m_shadermap["PBRBlinn-tex-instanced"], "normalTexture");
	specularMapLoc = glGetUniformLocation(m_shadermap["PBRBlinn-tex-instanced"], "specularTexture");
	FBOLoc = glGetUniformLocation(m_shadermap["PBRBlinn-tex-instanced"], "model_matrix_tbo");
	glUniform1i(baseImageLoc, 0); 
	glUniform1i(normalMapLoc, 1);
	glUniform1i(specularMapLoc,2);
	glUniform1i(FBOLoc, 3);

	glUseProgram(m_shadermap["PBRBlinn-tex"]);
	baseImageLoc = glGetUniformLocation(m_shadermap["PBRBlinn-tex"], "diffuseTexture");
	normalMapLoc = glGetUniformLocation(m_shadermap["PBRBlinn-tex"], "normalTexture");
	specularMapLoc = glGetUniformLocation(m_shadermap["PBRBlinn-tex"], "specularTexture");
	glUniform1i(baseImageLoc, 0);
	glUniform1i(normalMapLoc, 1);
	glUniform1i(specularMapLoc, 2);

	
	glUseProgram(m_shadermap["passThroughColor"]);
	GLint diffuseLoc = glGetUniformLocation(m_shadermap["passThroughColor"], "g_diffuse");
	GLint normalLoc = glGetUniformLocation(m_shadermap["passThroughColor"], "g_normal");
	GLint specularLoc = glGetUniformLocation(m_shadermap["passThroughColor"], "g_specular");
	GLint positionLoc = glGetUniformLocation(m_shadermap["passThroughColor"], "g_position");
	GLint phongLoc = glGetUniformLocation(m_shadermap["passThroughColor"], "g_phong");
	glUniform1i(diffuseLoc, 0);
	glUniform1i(normalLoc, 1);
	glUniform1i(specularLoc, 2);
	glUniform1i(positionLoc, 3);
	glUniform1i(phongLoc, 4);

	glUseProgram(m_shadermap["multiLightPhong"]);
	diffuseLoc = glGetUniformLocation(m_shadermap["multiLightPhong"], "g_diffuse");
	normalLoc = glGetUniformLocation(m_shadermap["multiLightPhong"], "g_normal");
	specularLoc = glGetUniformLocation(m_shadermap["multiLightPhong"], "g_specular");
	positionLoc = glGetUniformLocation(m_shadermap["multiLightPhong"], "g_position");
	glUniform1i(diffuseLoc, 0);
	glUniform1i(normalLoc, 1);
	glUniform1i(specularLoc, 2);
	glUniform1i(positionLoc, 3);
	
	glUseProgram(m_shadermap["multiLightPBRBlinn"]);
	diffuseLoc = glGetUniformLocation(m_shadermap["multiLightPBRBlinn"], "g_diffuse");
	normalLoc = glGetUniformLocation(m_shadermap["multiLightPBRBlinn"], "g_normal");
	specularLoc = glGetUniformLocation(m_shadermap["multiLightPBRBlinn"], "g_specular");
	positionLoc = glGetUniformLocation(m_shadermap["multiLightPBRBlinn"], "g_position");
	GLint envMapLoc = glGetUniformLocation(m_shadermap["multiLightPBRBlinn"], "g_envMap");
	GLint iradMapLoc = glGetUniformLocation(m_shadermap["multiLightPBRBlinn"], "g_iradMap");
	glUniform1i(diffuseLoc, 0);
	glUniform1i(normalLoc, 1);
	glUniform1i(specularLoc, 2);
	glUniform1i(positionLoc, 3);
	glUniform1i(envMapLoc, 4);
	glUniform1i(iradMapLoc, 5);
		
	glUseProgram(m_shadermap["cubemap"]);
	GLint cubemapLoc = glGetUniformLocation(m_shadermap["cubemap"], "cubemap");
	diffuseLoc = glGetUniformLocation(m_shadermap["cubemap"], "g_diffuse");
	glUniform1i(cubemapLoc, 0);
	glUniform1i(diffuseLoc, 1);

	glUseProgram(m_shadermap["UIRender"]);
	GLint UIElement = glGetUniformLocation(m_shadermap["UIRender"], "UIElement");
	glUniform1i(UIElement, 0);
}


ShaderManager::~ShaderManager(void)
{
	cout << "shaderManager destructor called" << std::endl;
}


GLuint ShaderManager::getShader(string shaderName)
{
	// check if shaderName exists in the map, if yes and only as single instance exists return the value.
	// Else print error and return NULL.
	if (m_shadermap.count(shaderName) == 1)
	{
		return m_shadermap[shaderName];
	}
	else 
	{
		cout << shaderName.c_str() << " not found in map." << endl;
		return NULL;
	}
}


bool ShaderManager::loadShader(char *vertName, char *fragName, string shaderName) 
{
	GLuint p, f, v;

	char *vs, *fs;

	v = glCreateShader(GL_VERTEX_SHADER);
	f = glCreateShader(GL_FRAGMENT_SHADER);	
	

	// load shaders & get length of each
	GLint vlen;
	GLint flen;
	
	vs = loadFile(vertName,vlen);
	fs = loadFile(fragName,flen);
	

	const char* vv = vs;
	const char* ff = fs;
	

	glShaderSource(v, 1, &vv,&vlen);
	glShaderSource(f, 1, &ff,&flen);
	

	GLint compiled;

	glCompileShader(v);
	glGetShaderiv(v, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		std::cout << "Vertex shader not compiled." << std::endl;
		printShaderError(v);
		return false;
	} 

	glCompileShader(f);
	glGetShaderiv(f, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		std::cout << "Fragment shader not compiled." << std::endl;
		printShaderError(f);
		return false;
	}
	
	p = glCreateProgram();

	glAttachShader(p,v);
	glAttachShader(p,f);
	
	glBindAttribLocation(p,0,"VS_position");
	glBindAttribLocation(p,1,"VS_normal");
	glBindAttribLocation(p,2,"VS_texCoord");
	glBindAttribLocation(p,3,"VS_tangent");
	glBindAttribLocation(p,4,"VS_bitangent");

	glLinkProgram(p);

	glDetachShader(p,v);
	glDetachShader(p,f);

	glUseProgram(p);

	delete [] vs;
	delete [] fs; 
	
	m_shadermap[shaderName]=p;

	return true;
}


bool ShaderManager::loadShader(char *vertName, char *geoName, char *fragName, string shaderName) 
{
	GLuint p, f, v, g;

	char *vs, *fs, *gs;

	v = glCreateShader(GL_VERTEX_SHADER);
	g = glCreateShader(GL_GEOMETRY_SHADER);
	f = glCreateShader(GL_FRAGMENT_SHADER);	
	

	// load shaders & get length of each
	GLint vlen;
	GLint glen;
	GLint flen;
	
	vs = loadFile(vertName,vlen);
	gs = loadFile(geoName,glen);
	fs = loadFile(fragName,flen);
	

	const char* vv = vs;
	const char* gg = gs;
	const char* ff = fs;
	

	glShaderSource(v, 1, &vv,&vlen);
	glShaderSource(g, 1, &gg,&glen);
	glShaderSource(f, 1, &ff,&flen);
	

	GLint compiled;

	glCompileShader(v);
	glGetShaderiv(v, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		std::cout << "Vertex shader not compiled." << std::endl;
		printShaderError(v);
		return false;
	} 

	glCompileShader(g);
	glGetShaderiv(g, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		std::cout << "Goemetry shader not compiled." << std::endl;
		printShaderError(g);
		return false;
	} 

	glCompileShader(f);
	glGetShaderiv(f, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		std::cout << "Fragment shader not compiled." << std::endl;
		printShaderError(f);
		return false;
	}
	
	p = glCreateProgram();

	glAttachShader(p,v);
	glAttachShader(p,g);
	glAttachShader(p,f);
	
	glBindAttribLocation(p,0,"VS_position");
	glBindAttribLocation(p,1,"VS_normal");
	glBindAttribLocation(p,2,"VS_texCoord");
	glBindAttribLocation(p,3,"VS_tangent");
	glBindAttribLocation(p,4,"VS_bitangent");

	glLinkProgram(p);
	glUseProgram(p);

	delete [] vs;
	delete [] gs; 
	delete [] fs; 
	
	m_shadermap[shaderName]=p;

	return true;
}


char* ShaderManager::loadFile(const char *fname, GLint &fSize) 
{
	int size;
	char * memblock;

	// file read based on example in cplusplus.com tutorial
	// ios::ate opens file at the end
	std::ifstream file (fname, std::ios::in|std::ios::binary|std::ios::ate);
	if (file.is_open()) {
		size = (int) file.tellg(); // get location of file pointer i.e. file size
		fSize = (GLint) size;
		memblock = new char [size];
		file.seekg (0, std::ios::beg);
		file.read (memblock, size);
		file.close();
		std::cout << "file " << fname << " loaded" << std::endl;
	}
	else {
		std::cout << "Unable to open file " << fname << std::endl;
		fSize = 0;
		// should ideally set a flag or use exception handling
		// so that calling function can decide what to do now
		return nullptr;
	}
	return memblock;
}


// from rt3d.h by Daniel Livingstone
void ShaderManager::printShaderError(const GLint shader) 
{
	int maxLength = 0;
	int logLength = 0;
	GLchar *logMessage;

	// Find out how long the error message is
	if (!glIsShader(shader))
		glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
	else
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

	if (maxLength > 0) { // If message has some contents
		logMessage = new GLchar[maxLength];
		if (!glIsShader(shader))
			glGetProgramInfoLog(shader, maxLength, &logLength, logMessage);
		else
			glGetShaderInfoLog(shader,maxLength, &logLength, logMessage);
		std::cout << "Shader Info Log:" << std::endl << logMessage << std::endl;
		delete [] logMessage;
	}
}
