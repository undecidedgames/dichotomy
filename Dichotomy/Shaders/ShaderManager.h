#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <memory>
#include <GL/glew.h>

#include "../Config/ConfigManager.h"

using namespace std;

/**
*	A manager class that deals with openGL shader programs.
*	@author B00216209
*/
class ShaderManager
{
public:

	/**
	*	Default Constructor 
	*	Loads a few hard-coded shader names and sets some texture uniform binding points. In future this will all be done via config file variables.
	*	@param configManager A shared_ptr to the configManger
	*/
	ShaderManager(std::shared_ptr<ConfigManager> configManager);

	/**
	*	Default Destructor
	*/
	~ShaderManager();

	/**
	*	Loads a new 2 component (vertex + Fragment) shader program.
	*	Creates a vertex and fragment shader, loads the file, compiles the shaders, attaches the shaders to a program object, sets bind attribute locations, links the shader program and adds it to the map.
	*	@param vertName A char pointer containing the name of the vertex shader.
	*	@param fragName A char pointer containing the name of the fragment shader.
	*	@param shaderName A string to use as the key in the map which will return the program ID.
	*/
	bool loadShader(char* vertName, char* fragName, string shaderName);

	/**
	*	Loads a new 3 component (vertex + Geometry + Fragment) shader program.
	*	Creates a vertex shader, geomtry shader and fragment shader, loads the file, compiles the shaders, attaches the shaders to a program object, sets bind attribute locations, links the shader program and adds it to the map.
	*	@param vertName A char pointer containing the name of the vertex shader.
	*	@param geoName A char pointer containing the name of the geometry shader.
	*	@param fragName A char pointer containing the name of the fragment shader.
	*	@param shaderName A string to use as the key in the map which will return the program ID.
	*/
	bool loadShader(char* vertName, char* geoName, char* fragName, string shaderName);

	/**
	*	Gets a shader ID using a string as a key into the map.
	*	@param shaderName The string used as key into the shader map
	*	@returns A GLuint identifying the shader program that belongs to the key given.
	*/
	GLuint getShader(string shaderName);

private:

	/**
	*	Loads a file and returns a char* of the contents
	*	@param fname the path to the file to load
	*	@param fSize A reference to a glint that will be filled with the size of the char* returned by this function
	*/
	char* loadFile(const char* fname, GLint& fSize);

	/**
	*	Prints a shader Error log.
	*	@param shader the shader program ID to print it's error log if one exists.
	*	@author Daniel Livingstone
	*/
	void printShaderError(const GLint shader);
	
	unordered_map<std::string, GLuint> m_shadermap; /**< An unordered map of strings to GLuints, used to map shader names to openGL program ID's */
	std::shared_ptr<ConfigManager> m_configManager; /**< A shared_ptr to the configManager */
};

