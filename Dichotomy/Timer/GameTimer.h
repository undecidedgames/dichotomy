#pragma once

#include <iostream>

#include <SDL.h>

class GameTimer
{
public:
	GameTimer();
	~GameTimer();

public:
	void startTimer();
	void startCooldown();
	void setFramesPerSec(float frames);
	bool pollTimeStep();
	bool checkCooldown();
	void resetCooldownTimer();

private:
	float m_framesPerSec; 
	Uint32 m_lastFrameTime, m_lastCooldownTime;
	Uint32 m_currentFrameTime, m_currentCooldownTime;
	bool m_timeStep; // will return true every length of time it takes to complete a frame
};
