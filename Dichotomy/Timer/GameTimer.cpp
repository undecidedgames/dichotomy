#include "GameTimer.h"


GameTimer::GameTimer()
{
}

GameTimer::~GameTimer()
{
}
// first 3 functions are to ensure fps and tick rate is not dependant on hardware
void GameTimer::startTimer()
{
	m_lastFrameTime = SDL_GetTicks();
	m_currentFrameTime = m_lastFrameTime;
}



void GameTimer::setFramesPerSec(float frames)
{
	m_framesPerSec = (1/frames); // frames per sec
}


bool GameTimer::pollTimeStep()
{
	m_currentFrameTime = SDL_GetTicks(); // update current time every poll

	if((m_currentFrameTime - m_lastFrameTime) >= (m_framesPerSec*1000)) // if the difference between current and last time(in seconds) is greater or equals the timestep
	{
		m_lastFrameTime = m_currentFrameTime; // reset last time to current time
		
		return true;
	}
		return false;
}

// this should be done in screen init
void GameTimer::startCooldown()
{
	m_lastCooldownTime = SDL_GetTicks();
	m_currentCooldownTime = m_lastCooldownTime;
}
bool GameTimer::checkCooldown()
{
	m_currentCooldownTime = SDL_GetTicks();
	if((m_currentCooldownTime - m_lastCooldownTime) >= 1000)
	{
		return true;
	}
	return false;
}

void GameTimer::resetCooldownTimer()
{
	m_lastCooldownTime = m_currentCooldownTime;

}