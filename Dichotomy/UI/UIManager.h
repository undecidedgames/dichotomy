#pragma once

#include <unordered_map>
#include <memory>
#include <iostream>
#include <sstream>

#include <SDL_ttf.h>

#include "UIElement.h"
#include "HUD.h"
#include "Form.h"
#include "Text.h"
#include "TextManager.h"
#include "TextBox.h"
#include "../Config/ConfigManager.h"
#include "../Service/ServiceManager.h"

/////////// Used from: http://sdl.beuc.net/sdl.wiki/SDL_Average_FPS_Measurement
// How many frames time values to keep
// The higher the value the smoother the result is...
// Don't make it 0 or less :)
#define FRAME_VALUES 20
///////////

class UIManager
{
public:
	/* Class Constructor & Destructor */
	UIManager(std::shared_ptr<ServiceManager> serviceManager, std::shared_ptr<ConfigManager> configManager);
	~UIManager(void);

public:
	/* General Public Methods*/
	void init();
	void update();

public:
	/* Form & Element Methods */
	void createForm(const char* ID, int positionX, int positionY, unsigned int width, unsigned int height, std::shared_ptr<ServiceManager> serviceManager);
	void createElement(const char* formID, int type, const char* elementID, const char* font, unsigned char red, unsigned char green, unsigned char blue, int positionX, int positionY, unsigned int width, unsigned int height);

	std::shared_ptr<HUD> getHUD();

	std::shared_ptr<Form> getForm(const char* formID);

	/* Debug Methods */
	void initFPSTimer();
	void updateFPSTimer();

private:
	std::unordered_map<const char*, std::shared_ptr<Form>> m_FormList;
	std::shared_ptr<HUD> m_HUD;

	std::shared_ptr<ConfigManager> m_configManager;
	std::shared_ptr<ServiceManager> m_serviceManager;

	std::shared_ptr<TextManager> m_textManager;

	unsigned int m_screenWidth;
	unsigned int m_screenHeight;

private:
	//////// // Used from: http://sdl.beuc.net/sdl.wiki/SDL_Average_FPS_Measurement
	// An array to store frame times:
	Uint32 frametimes[FRAME_VALUES];

	// Last calculated SDL_GetTicks
	Uint32 frametimelast;

	// total frames rendered
	Uint32 framecount;

	// the value you want
	int framespersecond;
	////////

	int frametime;
	

};

