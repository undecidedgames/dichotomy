#include "Text.h"


#pragma region Class Constructor & Destructor

Text::Text(const char* ID, TTF_Font* font, unsigned char red, unsigned char green, unsigned char blue, int positionX, int positionY, int width, int height, unsigned int screenWidth, unsigned int screenHeight)
{
	std::cout << "Text object '" << ID << "' constructed" << std::endl;

	m_ID = ID;
	m_font = font;
	m_textColour.r = red;
	m_textColour.g = green;
	m_textColour.b = blue;
	m_positionX = positionX;
	m_positionY = positionY;
	m_screenHeight = screenHeight;
	m_screenWidth = screenWidth;
	//m_width = width;
	//m_height = height;
	m_texture = 0;

	// generate texture handle, setup text texture parameters.
	glGenTextures(1, &m_texture);
	GLfloat border[] = {0.0f, 0.0f, 0.0f, 0.0f};
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	
}


Text::~Text()
{
	std::cout << "Text object '" << m_ID << "' destructor called" << std::endl;
	glDeleteTextures(1, &m_texture);
}

#pragma endregion


#pragma region General Public Methods

void Text::init()
{
	
}


void Text::update()
{
}

#pragma endregion


#pragma region Text & Texture Methods

void Text::setText(const char* text)
{
	m_text = text;
	generateTexture();
}


void Text::setTextColour(unsigned char red, unsigned char green, unsigned char blue)
{
	m_textColour.r = red;
	m_textColour.g = green;
	m_textColour.b = blue;

	generateTexture();
}


void Text::generateTexture()
{
	// Generates texture to use in OpenGL context
	
	SDL_Surface* textSurface;
	
	if(!(textSurface = TTF_RenderText_Blended(m_font, m_text, m_textColour))) 
	{
		std::cout << "TTF_RenderText_Blended: " << TTF_GetError() << std::endl;
	} 

	GLuint width = textSurface->w;
	GLuint height = textSurface->h;
	GLuint colours = textSurface->format->BytesPerPixel;

	GLuint format, internalFormat;

	if (colours == 4) 
	{   // alpha
		if (textSurface->format->Rshift == 0)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} 
	else 
	{   // no alpha
		if (textSurface->format->Rshift == 0)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;
		
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, GL_UNSIGNED_BYTE, textSurface->pixels);

	SDL_FreeSurface(textSurface);

	m_width = width;
	m_height = height;

	updateModelMatrix();
}

#pragma endregion