#pragma once

#include <iostream>
#include <memory>
#include <sstream>

#include <SDL.h>

#include "Text.h"

class HUD
{
public:
	HUD();
	~HUD();

public:
	void init();
	void update();
};

