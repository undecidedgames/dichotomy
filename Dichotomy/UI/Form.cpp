#include "Form.h"


#pragma region Class Constructor & Destructor 

Form::Form(const char* ID, int positionX, int positionY, unsigned int width, unsigned int height, unsigned int screenWidth, unsigned int screenHeight, std::shared_ptr<ServiceManager> serviceManager)
{
	std::cout << "Form object '" << ID << "' constructor called" << std::endl;

	m_ID = ID;
	m_positionX = positionX;
	m_positionY = positionY;
	m_screenHeight = screenHeight;
	m_screenWidth = screenWidth;
	m_width = width;
	m_height = height;
	m_serviceManager = serviceManager;
}


Form::~Form()
{
	std::cout << "Form object '" << m_ID << "' destructor called" << std::endl;
}

#pragma endregion


#pragma region General Public Methods

void Form::init()
{
}


void Form::update()
{
	std::shared_ptr<Input> inputService = m_serviceManager->getInput();

	for(auto iterator = m_ElementList.begin(); iterator != m_ElementList.end(); ++iterator)
	{
		iterator->second->update();
		iterator->second->detectMouseOver(inputService->getMousePosition());
	}
}

#pragma endregion


#pragma region Element Methods

void Form::createElement(int type, const char* ID, TTF_Font* font, unsigned char red, unsigned char green, unsigned char blue, int positionX, int positionY, unsigned int width, unsigned int height)
{
	std::shared_ptr<UIElement> tempElement;

	switch (type)
	{
		case 0:
			tempElement = std::shared_ptr<UIElement>(new Text(ID, font, red, green, blue, positionX, positionY, width, height, m_screenWidth, m_screenHeight));
			break;
		case 1:
			tempElement = std::shared_ptr<UIElement>(new TextBox(ID, red, green, blue, positionX, positionY, width, height, m_screenWidth, m_screenHeight));
			break;
	}

	m_ElementList.insert(std::pair<const char*, std::shared_ptr<UIElement>>(ID, tempElement));
}


std::shared_ptr<UIElement> Form::getElement(const char* ID)
{
	return m_ElementList[ID];
}

void Form::setText(const char* text)
{
	//m_text = text;
}

#pragma endregion