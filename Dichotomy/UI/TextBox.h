#pragma once

#include <iostream>

#include <GL/glew.h>
#include <SDL_ttf.h>

#include "UIElement.h"

class TextBox : public UIElement
{
public:
	/* Class Constructor & Destructor */
	TextBox(const char* ID, unsigned char red, unsigned char green, unsigned char blue, int positionX, int positionY, int width, int height, unsigned int screenWidth, unsigned int screenHeight);
	~TextBox();

public:
	/* General Public Methods */
	void init();
	void update();

	/* Text & Texture Methods */
	void setText(const char* text) { }
	void setTextColour(unsigned char red, unsigned char green, unsigned char blue) { }
	void generateTexture();

};

