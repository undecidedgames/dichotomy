#pragma once

#include <unordered_map>
#include <memory>
#include <string>

#include <SDL_ttf.h>

#include "../Config/ConfigManager.h"

class TextManager
{
public:
	/* Class Constructor & Destructor */
	TextManager(std::shared_ptr<ConfigManager> configManager);
	~TextManager(void);

public:
	/* SDL_TTF Font Method */
	TTF_Font* getFont(const char* font);

private:
	std::unordered_map<const char*, TTF_Font*> m_fontList;

	std::shared_ptr<ConfigManager> m_configManager;

	/* Load font files */
	void loadFont(const char* font, const char* fileName, unsigned int pointSize);
};

