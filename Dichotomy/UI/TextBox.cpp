#include "TextBox.h"


#pragma region Class Constructor & Destructor

TextBox::TextBox(const char* ID, unsigned char red, unsigned char green, unsigned char blue, int positionX, int positionY, int width, int height, unsigned int screenWidth, unsigned int screenHeight)
{
	std::cout << "TextBox object '" << ID << "' constructed" << std::endl;

	m_ID = ID;
	m_positionX = positionX;
	m_positionY = positionY;
	m_screenHeight = screenHeight;
	m_screenWidth = screenWidth;
	//m_width = width;
	//m_height = height;
	m_texture = 0;

	// generate texture handle, setup text texture parameters.
	glGenTextures(1, &m_texture);
	GLfloat border[] = {0.0f, 0.0f, 0.0f, 0.0f};
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	

	//generateTexture();
}


TextBox::~TextBox()
{
	std::cout << "TextBox object '" << m_ID << "' destructor called" << std::endl;
	glDeleteTextures(1, &m_texture);
}

#pragma endregion


#pragma region General Public Methods

void TextBox::init()
{
	
}


void TextBox::update()
{
}

#pragma endregion


void TextBox::generateTexture()
{
	// Generates a texture for the OpenGL context to use to render element
	
	SDL_Surface* textBoxSurface = NULL;

	textBoxSurface = SDL_CreateRGBSurface(0, 500, 500, 16, 0, 0, 0, 0);
	SDL_FillRect(textBoxSurface, NULL, SDL_MapRGB(textBoxSurface->format, 255, 0, 0));

	if (textBoxSurface == NULL)
	{
		std::cout << "SDL_CreateRGBSurface: " << SDL_GetError();
	}

	GLuint width = textBoxSurface->w;
	GLuint height = textBoxSurface->h;
	GLuint colours = textBoxSurface->format->BytesPerPixel;

	GLuint format, internalFormat;

	if (colours == 4) 
	{   // alpha
		if (textBoxSurface->format->Rshift == 0)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} 
	else 
	{   // no alpha
		if (textBoxSurface->format->Rshift == 0)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;
		
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, GL_UNSIGNED_BYTE, textBoxSurface->pixels);

	SDL_FreeSurface(textBoxSurface);

	m_width = width;
	m_height = height;

	updateModelMatrix();
}