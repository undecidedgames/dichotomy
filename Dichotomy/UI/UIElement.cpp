#include "UIElement.h"


const char* UIElement::getID() 
{
	return m_ID;
}


bool UIElement::getStatus() 
{
	return m_status;
}


int UIElement::getPositionX() 
{
	return m_positionX;
}


int UIElement::getPositionY() 
{
	return m_positionY;
}


int UIElement::getWidth() 
{
	return m_width;
}


int UIElement::getHeight() 
{
	return m_height;
}


GLuint UIElement::getTexture() 
{
	return m_texture;
}


glm::mat4 UIElement::getModelMatrix() 
{
	return m_modelMatrix;
}


void UIElement::updateModelMatrix() 
{ 
	glm::vec3 translationVec =	glm::vec3(-1.0f,1.0f,0.0f) +  // move quad center to the top left of the screen
	glm::vec3((GLfloat)m_width/(GLfloat)m_screenWidth,-((GLfloat)m_height/(GLfloat)m_screenHeight),0.0f) + // move quad so that it's fully inside the screen
	glm::vec3((GLfloat)m_positionX*2/(GLfloat)m_screenWidth,-((GLfloat)m_positionY*2/(GLfloat)m_screenHeight),0.0f); // move quad to it's final location
	m_modelMatrix = glm::mat4(1.0f); // create identity matrix
	m_modelMatrix = glm::translate(m_modelMatrix,translationVec); //  move it to it's final location (this happens after scale)
	m_modelMatrix = glm::scale(m_modelMatrix,glm::vec3((GLfloat)m_width/(GLfloat)m_screenWidth,(GLfloat)m_height/(GLfloat)m_screenHeight,0.0f)); // scale to given size
}

void UIElement::detectMouseOver(std::pair<int, int> mousePosition)
{
	// Detects if mouse is hovering above the UI element

	if (mousePosition.first > m_positionX && mousePosition.first < m_positionX + (int)m_width) 
	{
		if (mousePosition.second > m_positionY && mousePosition.second < m_positionY + (int)m_height)
		{
			m_mouseOver = true;
		} else m_mouseOver = false;
	} else m_mouseOver = false;
}


bool UIElement::getMouseOver()
{
	// Returns boolean based on the mouse being over a UI element or not
	return m_mouseOver;
}