#pragma once

#include <iostream>
#include <string>
#include <utility>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>

#include <SDL.h>

class UIElement
{
public:
	/* Virtual Destructor */
	virtual ~UIElement() {};

	/* General Public Methods */
	virtual void init() = 0;
	virtual void update() = 0;

public:
	/* Virtual Methods for Text Elements */
	virtual void setText(const char* text) = 0;
	virtual void setTextColour(unsigned char red, unsigned char green, unsigned char blue) = 0;

public:
	const char* getID();
	bool getStatus();
	int getPositionX();
	int getPositionY();
	int getWidth();
	int getHeight();
	GLuint getTexture();
	glm::mat4 getModelMatrix();
	void updateModelMatrix();

	void detectMouseOver(std::pair<int, int> mousePosition);
	bool getMouseOver();

protected:
	const char* m_ID;
	bool m_status;
	int m_positionX;
	int m_positionY;
	unsigned int m_width; 
	unsigned int m_height;
	unsigned int m_screenWidth;
	unsigned int m_screenHeight;
	GLuint m_texture;
	glm::mat4 m_modelMatrix;
	bool m_mouseOver;

};

