#pragma once

#include <iostream>

#include <GL/glew.h>
#include <SDL_ttf.h>

#include "UIElement.h"

class Text : public UIElement
{
public:
	/* Class Constructor & Destructor */
	Text(const char* ID, TTF_Font* font, unsigned char red, unsigned char green, unsigned char blue, int positionX, int positionY, int width, int height, unsigned int screenWidth, unsigned int screenHeight);
	~Text();

public:
	/* General Public Methods */
	void init();
	void update();

	/* Text & Texture Methods */
	void setText(const char* text);
	void setTextColour(unsigned char red, unsigned char green, unsigned char blue);
	void generateTexture();

private:
	SDL_Color m_textColour;
	const char* m_text;
	TTF_Font* m_font;	
};

