#include "UIManager.h"


#pragma region Class Destructor & Destructor

UIManager::UIManager(std::shared_ptr<ServiceManager> serviceManager, std::shared_ptr<ConfigManager> configManager)
{
	std::cout << "UIManager constructor called" << std::endl;

	m_configManager = configManager;
	m_serviceManager = serviceManager;

	m_screenWidth = m_configManager->m_propertyTree.get<unsigned int>("window.width");
	m_screenHeight = m_configManager->m_propertyTree.get<unsigned int>("window.height");

	m_HUD = std::shared_ptr<HUD>(new HUD());
	m_textManager = std::shared_ptr<TextManager>(new TextManager(m_configManager));
}


UIManager::~UIManager(void)
{
	std::cout << "UIManager destructor called" << std::endl;
}

#pragma endregion


#pragma region General Public Methods

void UIManager::init()
{
	initFPSTimer();

	m_HUD->init();

	createForm("testForm", 0, 600, 100, 100, m_serviceManager);
	createForm("FPS", 0, 0, 100, 100, m_serviceManager);

	createElement("testForm", 0, "testText", "default", 255, 255, 255, 10, 10, 100, 100);
	getForm("testForm")->getElement("testText")->setText(m_configManager->m_propertyTree.get<std::string>("levelManager.startingScreen").c_str());

	createElement("FPS", 0, "FrameTime", "default", 255, 255, 255, 10, 120, 100, 100);
	getForm("FPS")->getElement("FrameTime")->setText("FrameTime");

	createElement("FPS", 0, "FPS", "default", 255, 255, 255, 10, 220, 100, 100);
	getForm("FPS")->getElement("FPS")->setText("FPS");

	createForm("StartScreen", 0, 0, 0, 0, m_serviceManager);
	createElement("StartScreen", 0, "GameTitle", "default200", 255, 255, 255, 314, 200, 100, 100);
	getForm("StartScreen")->getElement("GameTitle")->setText("DICHOTOMY");
	createElement("StartScreen", 0, "StartGame", "default100", 255, 255, 255, 450, 500, 100, 100);
	getForm("StartScreen")->getElement("StartGame")->setText("Start Game");
	createElement("StartScreen", 0, "Options", "default100", 255, 255, 255, 450, 600, 100, 100);
	getForm("StartScreen")->getElement("Options")->setText("Options");

	createForm("OptionsScreen", 0, 0, 0, 0, m_serviceManager);
	createElement("OptionsScreen", 0, "ScreenTitle", "default100", 255, 255, 255, 519, 10, 100, 100);
	getForm("OptionsScreen")->getElement("ScreenTitle")->setText("Options");
	
	createElement("OptionsScreen", 0, "Back", "default", 255, 255, 255, 50, 50, 100, 100);
	getForm("OptionsScreen")->getElement("Back")->setText("Back");

	createForm("GraphicsSettings", 0, 0, 0, 0, m_serviceManager);
	createElement("GraphicsSettings", 0, "VSync", "default", 255, 255, 255, 50, 100, 100, 100);
	getForm("GraphicsSettings")->getElement("VSync")->setText("V-Sync:");

	createElement("GraphicsSettings", 0, "VSyncVal", "default", 255, 255, 255, 110, 100, 100, 100);
	getForm("GraphicsSettings")->getElement("VSyncVal")->setText("default");

	createForm("ChatBox", 0, 0, 0, 0, m_serviceManager);
	createElement("ChatBox", 1, "ChatBox", nullptr, NULL, NULL, NULL, 500, 500, NULL, NULL);
}
  

void UIManager::update()
{
	for(auto iterator = m_FormList.begin(); iterator != m_FormList.end(); ++iterator)
	{
		iterator->second->update();
	}
	m_HUD->update();

	updateFPSTimer();
/*	std::stringstream ss;
	ss << "FPS: " << frametime;
	getForm("FPS")->getElement("FPS")->setText(ss.str().c_str());
	std::stringstream ss2;
	ss2 << "Frame Time: " << framespersecond << " ms";
	getForm("FPS")->getElement("FrameTime")->setText(ss2.str().c_str());*/
}

#pragma endregion


#pragma region Form & Element Methhods

void UIManager::createForm(const char* ID, int positionX, int positionY, unsigned int width, unsigned int height, std::shared_ptr<ServiceManager> serviceManager)
{
	std::shared_ptr<Form> tempForm = std::shared_ptr<Form>(new Form(ID, positionX, positionY, width, height, m_screenWidth, m_screenHeight, serviceManager));

	m_FormList.insert(std::pair<const char*, std::shared_ptr<Form>>(ID, tempForm));
}


void UIManager::createElement(const char* formID, int type, const char* elementID, const char* font, unsigned char red, unsigned char green, unsigned char blue, int positionX, int positionY, unsigned int width, unsigned int height)
{
	m_FormList[formID]->createElement(type, elementID, m_textManager->getFont(font), red, green, blue, positionX, positionY, width, height);
}


std::shared_ptr<HUD> UIManager::getHUD()
{
	return m_HUD;
}


std::shared_ptr<Form> UIManager::getForm(const char* formID)
{
	return m_FormList[formID];
}

#pragma endregion


#pragma region Debug Methods

void UIManager::initFPSTimer()
{
	// Used from: http://sdl.beuc.net/sdl.wiki/SDL_Average_FPS_Measurement

	// Set all frame times to 0ms.
    memset(frametimes, 0, sizeof(frametimes));
    framecount = 0;
    framespersecond = 0;
    frametimelast = SDL_GetTicks();
}

void UIManager::updateFPSTimer() 
{
	// Used from: http://sdl.beuc.net/sdl.wiki/SDL_Average_FPS_Measurement

	Uint32 frametimesindex;
	Uint32 getticks;
	Uint32 count;
	Uint32 i;

	// frametimesindex is the position in the array. It ranges from 0 to FRAME_VALUES.
	// This value rotates back to 0 after it hits FRAME_VALUES.
	frametimesindex = framecount % FRAME_VALUES;

	// store the current time
	getticks = SDL_GetTicks();

	// save the frame time value
	frametimes[frametimesindex] = getticks - frametimelast;

	// save the last frame time for the next fpsthink
	frametimelast = getticks;

	// increment the frame count
	framecount++;

	// Work out the current framerate

	// The code below could be moved into another function if you don't need the value every frame.

	// I've included a test to see if the whole array has been written to or not. This will stop
	// strange values on the first few (FRAME_VALUES) frames.
	if (framecount < FRAME_VALUES) {

		count = framecount;

	} else {

		count = FRAME_VALUES;

	}

	// add up all the values and divide to get the average frame time.
	framespersecond = 0;
	for (i = 0; i < count; i++) {

		framespersecond += frametimes[i];

	}

	framespersecond /= count;

	// now to make it an actual frames per second value...
	//framespersecond = 1000.f / framespersecond;
	frametime = 1000.0f / framespersecond;
}

#pragma endregion