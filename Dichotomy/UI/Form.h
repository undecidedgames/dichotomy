#pragma once

#include <memory>
#include <unordered_map>

#include "UIElement.h"
#include "Text.h"
#include "TextBox.h"
#include "../Service/ServiceManager.h"

class Form : public UIElement
{
public:
	/* Class Constructor & Destructor */
	Form(const char* ID, int positionX, int positionY, unsigned int width, unsigned int height, unsigned int screenWidth, unsigned int screenHeight, std::shared_ptr<ServiceManager> serviceManager);
	~Form();

public:
	/* General Public Methods */
	void init();
	void update();

	/* Text Methods */
	void setText(const char* text);
	void setTextColour(unsigned char red, unsigned char green, unsigned char blue) { }
	
	/* Element Methods */
	void createElement(int type, const char* ID, TTF_Font* font, unsigned char red, unsigned char green, unsigned char blue, int positionX, int positionY, unsigned int width, unsigned int height);
	std::shared_ptr<UIElement> getElement(const char* ID);

private:
	std::unordered_map<const char*, std::shared_ptr<UIElement>> m_ElementList;
	std::shared_ptr<ServiceManager> m_serviceManager;	

};

