#include "TextManager.h"


#pragma region Class Constructor & Destructor

TextManager::TextManager(std::shared_ptr<ConfigManager> configManager)
{
	std::cout << "TextManager constructor called" << std::endl;

	if (TTF_Init() < 0) std::cout << "TTF_Init: " << TTF_GetError() << std::endl;

	m_configManager = configManager;

	loadFont("default", "default", 30);
	loadFont("default100", "default", 100);
	loadFont("default200", "default", 200);
}


TextManager::~TextManager(void)
{
	std::cout << "TextManager destructor called" << std::endl;

	for (auto iterator = m_fontList.begin(); iterator != m_fontList.end(); ++iterator)
	{
		TTF_CloseFont(iterator->second);
	}

	TTF_Quit();
}

#pragma endregion


TTF_Font* TextManager::getFont(const char* font)
{
	return m_fontList[font];
}


void TextManager::loadFont(const char* font, const char* fileName, unsigned int pointSize)
{
	// Load font files and add to text map, any size font for any font file can be set using pointSize
	std::string fileString = "textManager.";
	fileString.append(fileName);
	std::string configString = m_configManager->m_propertyTree.get<std::string>(fileString);
	std::string fullPath = "assets/fonts/" + configString;

	m_fontList[font] = TTF_OpenFont(fullPath.c_str(), pointSize);
	std::cout << "Font loaded: " << font << " (" << configString << ")" << std::endl;
}