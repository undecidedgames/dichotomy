#include "NetworkManager.h"


#pragma region Class Constructor & Destructor

NetworkManager::NetworkManager(std::string host, unsigned int port, unsigned int bufferSize)
{
	std::cout << "NetworkManager constructor called" << std::endl;

	// Create client object
	m_dichotomyClient = std::unique_ptr<Client>(new Client(host, port, bufferSize));

	m_connectedToServer = false;
}


NetworkManager::~NetworkManager(void)
{
	std::cout << "NetworkManager destructor called" << std::endl;

	SDLNet_Quit();
}

#pragma endregion


#pragma region General Public Methods

void NetworkManager::init()
{
	// Initialise SDL and client object
	if (SDLNet_Init() == -1)
	{
		std::cout << "SDLNet_Init: " << SDLNet_GetError() << std::endl;
	} else m_dichotomyClient->init();
}

#pragma endregion


#pragma region Network Management

bool NetworkManager::connectToServer()
{
	m_connectedToServer = m_dichotomyClient->connectToServer();

	return m_connectedToServer;
}


void NetworkManager::update()
{
	// Whilst connected to network, update client
	if (isConnected()) m_dichotomyClient->update();
}


bool NetworkManager::isConnected()
{
	// Return boolean if client is connected or not
	return m_dichotomyClient->getStatus();
}

#pragma endregion


#pragma region Game Management

void NetworkManager::sendGameData(glm::vec3 playerPosition)
{
	// Send game data in a glm::vec3 type used for positions
	m_dichotomyClient->sendGameData(playerPosition);
}


glm::vec3 NetworkManager::receiveGameData()
{
	// Receive game data in a glm::vec3 type used for positions
	return m_dichotomyClient->receiveGameData();
}


bool NetworkManager::gameInSession()
{
	// Return boolean if client is in a game session
	return m_dichotomyClient->gameInSession();
}


int NetworkManager::getPlayerNumber()
{
	// Return player number assigned by server 
	return m_dichotomyClient->getPlayerNumber();
}

#pragma endregion