#pragma once

#include <memory>

#include <glm/glm.hpp>

#ifdef _WIN32
#include <SDL_net.h>
#elif __APPLE__
#include <SDL2_net/SDL_net.h>
#endif

#include "Client.h"

class NetworkManager
{
public:
	/* Class Constructor & Destructor */
	NetworkManager(std::string host, unsigned int port, unsigned int bufferSize);
	~NetworkManager(void);

public:
	/* General Public Methods*/
	void init();

public:
	/* Network Management */
	bool connectToServer();
	void update();
	bool isConnected();
	
public:
	/* Game Management */
	void sendGameData(glm::vec3 playerPosition);
	glm::vec3 receiveGameData();
	bool gameInSession();
	int getPlayerNumber();

private:
	std::unique_ptr<Client> m_dichotomyClient;

	bool m_connectedToServer;
	bool m_gameInSession;
};

