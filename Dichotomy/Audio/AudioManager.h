#pragma once

#include <bass.h>
#include <string>
#include <unordered_map>
#include "Sample.h"
#include "../Config/ConfigManager.h"
#include <memory>

typedef std::unordered_map<std::string, Sample*> samplemap;
typedef std::unordered_map<std::string, HCHANNEL> channelmap;

class AudioManager
{
public:
	AudioManager();
	~AudioManager();
	void init(std::shared_ptr<ConfigManager> configManager);
	void playSample(std::string sampleName,float volume,std::string channelId);
	void setVolume(std::string sampleName, float volume);
	void fade(std::string SampleName,int fadeIn);
	void stopSample(std::string sampleName);
	float getSampleVolume(std::string sampleName);
	void loadMusic(std::string sampleName,const char *fname);
	void loadSounds(std::string sampleName,const char *fname, int simultaneousPlay);
	bool getPlaying(std::string sampleName);
	void setPlaying (std::string SampleName, bool play);
	void update();
	void setSampleFade(std::string sampleName, int index);
	
private:
	samplemap m_sampleMap;
	channelmap m_channelMap;
	float m_incValue;


};

