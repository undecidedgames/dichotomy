#include "AudioManager.h"
#include <iostream>


AudioManager::AudioManager()
{
}


AudioManager::~AudioManager()
{
}

void AudioManager::init(std::shared_ptr<ConfigManager> configManager)
{
	// initalises the device to the currently used sound device on system at a specified frequency with 0 means no flags and 0 means default window,
	//null to use default class identifier
	// default 16 bit and stereo these can be changed later by adding flags
	BASS_Init(1,24100,0,0,NULL);
	m_incValue=0.010f;

	// load up audio files
	std::string filePath = "assets/music/" + configManager->m_propertyTree.get<std::string>("audioManager.battleTrack");
 	loadMusic("background",filePath.c_str());

	filePath = "assets/music/" + configManager->m_propertyTree.get<std::string>("audioManager.ambient");
	loadMusic("ambient",filePath.c_str());

	filePath = "assets/music/" + configManager->m_propertyTree.get<std::string>("audioManager.attack");
	loadSounds("attack",filePath.c_str(),2);

	filePath = "assets/music/" + configManager->m_propertyTree.get<std::string>("audioManager.pain");
	loadSounds("pain",filePath.c_str(),2);

	filePath = "assets/music/" + configManager->m_propertyTree.get<std::string>("audioManager.footsteps");
	loadSounds("footsteps",filePath.c_str(),1);

	filePath = "assets/music/" + configManager->m_propertyTree.get<std::string>("audioManager.lever");
	loadSounds("lever",filePath.c_str(),1);

	filePath = "assets/music/" + configManager->m_propertyTree.get<std::string>("audioManager.doorOpen");
	loadSounds("doorOpen",filePath.c_str(),1);

	filePath = "assets/music/" + configManager->m_propertyTree.get<std::string>("audioManager.weaponSwing");
	loadSounds("weaponSwing",filePath.c_str(),1);

	filePath = "assets/music/" + configManager->m_propertyTree.get<std::string>("audioManager.npcPain");
	loadSounds("npcPain",filePath.c_str(),3);

}

void AudioManager::loadMusic(std::string sampleName, const char *fname)
{
	//use the musics sample constructor and store in map
	 m_sampleMap[sampleName] = new Sample(fname, 0.0f, sampleName);
	
}

void AudioManager::loadSounds(std::string sampleName, const char *fname, int simultaneousPlay)
{
	//use the sound sample constructor and store in map
	m_sampleMap[sampleName] = new Sample(fname,0.0f,sampleName,simultaneousPlay);
}


void AudioManager::setVolume(std::string sampleName, float volume)
{
	//set volume to sample in map
	m_sampleMap[sampleName]->setVolume(volume);
	
	
}

float AudioManager::getSampleVolume(std::string sampleName)
{
	// returns current volume for sample
	float tmp;
	BASS_ChannelGetAttribute(m_sampleMap[sampleName]->getSample(), BASS_ATTRIB_VOL,&tmp ); // gets current volume of sample
	m_sampleMap[sampleName]->setVolume(tmp);
	return tmp;
}

void AudioManager::fade(std::string sampleName,int fadeIn)
{
	if(fadeIn==2)
	{
		m_sampleMap[sampleName]->setVolume((m_sampleMap[sampleName]->getVolume() + m_incValue)); // adds inc if fade in is true
	}
	if(fadeIn==1)
	{
		m_sampleMap[sampleName]->setVolume((m_sampleMap[sampleName]->getVolume() - m_incValue)); // minus inc if fade in is false
		
	}
	BASS_ChannelSetAttribute(m_sampleMap[sampleName]->getSample(), BASS_ATTRIB_VOL,m_sampleMap[sampleName]->getVolume()); // set the new volume
}


void AudioManager::playSample(std::string sampleName,float volume, std::string channelId)
{
	m_channelMap[channelId] = BASS_SampleGetChannel(m_sampleMap[sampleName]->getSample(),false); // get a channel for the sample
	BASS_ChannelPlay(m_channelMap[channelId], FALSE); // play it on the channel
	setVolume(sampleName,volume);
	m_sampleMap[sampleName]->setPlaying(true);
}


void AudioManager::stopSample(std::string sampleName)
{
	BASS_SampleStop(m_sampleMap[sampleName]->getSample()); // stop sample
	m_sampleMap[sampleName]->setPlaying(false);
}


bool AudioManager::getPlaying(std::string sampleName)
{
	// return if playing
	return m_sampleMap[sampleName]->isPlaying();
}


void AudioManager::update()
{
	// get volume if fading up put sound upwards 
	float temp = m_sampleMap["background"]->getVolume();
	if (m_sampleMap["background"]->getFade() == 2)
	{
		fade("background", 2);
	}
	
	// if fade down decrease sound
	if(m_sampleMap["background"]->getFade() == 1)
	{
		fade("background",1);

		// if volume less than 0.05 just stop playing
		if(temp < 0.05f)
		{
			stopSample("background");
			setSampleFade("background",0);
			playSample("ambient",1.0f,"1");
		}
	}
}


void AudioManager::setSampleFade(std::string sampleName, int index)
{
	// set if fading up down or not fading at all - 2 is up, 1 is down 0 is not at all
	m_sampleMap[sampleName]->setFade(index);
}


void AudioManager::setPlaying(std::string sampleName, bool play)
{
	// set if playing
	m_sampleMap[sampleName]->setPlaying(play);
}