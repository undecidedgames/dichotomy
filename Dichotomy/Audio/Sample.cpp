#include "Sample.h"

Sample::Sample(const char* fname, float volume, std::string id)
{
	// sample constructor for music
	m_volume = volume;
	
	m_sample = BASS_SampleLoad(false,fname,0,0,1, BASS_SAMPLE_LOOP);
	m_loop = true;
	bool m_isPlaying = false;
	setVolume(volume);
	m_id = id; // incase a class needs to access name 

}


Sample::Sample(const char* fname, float volume, std::string id, int simultaneousPlay)
{
	// sample constructor for sound effects
	m_volume = volume;
	
	m_sample = BASS_SampleLoad(false,fname,0,0,simultaneousPlay,NULL);
	m_loop = false;
	bool m_isPlaying = false;
	setVolume(volume);
	m_id = id; // incase a class needs to access name 

}


void Sample::setVolume(float vol)
{
	if(m_id == "background")
	{
		if (vol > 0.20f) vol = 0.20f; // cap values

		if (vol < 0.0f) vol = 0.0f; // cap values
	}
	BASS_ChannelSetAttribute(m_sample, BASS_ATTRIB_VOL, vol); // set volume to the float parameter of the sample passed in
	m_volume = vol;
}


void Sample::setPlaying(bool playing)
{
	m_isPlaying = playing;
}


bool Sample::isPlaying()
{
	return m_isPlaying;
}


HSTREAM Sample::getSample()
{
	return m_sample;
}


float Sample::getVolume()
{
	return m_volume;
}


void Sample::setFade(int index)
{
	m_fadeIn = index;
}


int Sample::getFade()
{
	return m_fadeIn;
}