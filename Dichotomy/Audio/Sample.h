#pragma once

#include <string>

#include <bass.h>

class Sample
{
public:
	Sample(const char* fname, float volume, std::string id);
	Sample(const char* fname, float volume, std::string id, int simultaneousPlay);
	Sample();
	~Sample();

public:
	void setPlaying(bool playing);
	void setVolume(float vol);
	bool isPlaying();
	float getVolume();
	void setFade(int index);
	int getFade();
	HSTREAM getSample();
	
private:
	int m_fadeIn;
	bool m_isPlaying;
	std::string m_id;
	float m_volume;
	HSTREAM m_sample;
	HCHANNEL m_channel;
	bool m_loop;
};
