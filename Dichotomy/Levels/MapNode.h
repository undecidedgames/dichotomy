#pragma once

#include <string>
#include <sstream>
#include <vector>
#include <memory>

#include <glm/glm.hpp>
#include <GL/glew.h>

class AbstractActor;
class DynamicProp;
class Prop;
class Tile;

class MapNode
{
public:
	MapNode(const unsigned int x, const unsigned int y, const unsigned int cost);
	~MapNode();

public:
	void addActor(std::shared_ptr<AbstractActor> actor);
	void removeActor(std::shared_ptr<AbstractActor> actor);

	void addDynamicProp(std::shared_ptr<DynamicProp> dynamicProp);

	bool getAccessability();
	void setAccessability(const bool isAccessable);
	
	glm::vec2 getPosition();
	void setPosition(const float x, const float y);

	glm::vec3 getVec3Position();

	unsigned int getCost();

	unsigned int getWeight();
	void setWeight(unsigned int weight);

	unsigned int getCostFromStart();
	void setCostFromStart(float costFromStart);

	float getHeuristicCost();
	void setHeuristicCost(float heuristicCost);

	float getTotalCost();
	void setTotalCost(float totalCost);

	const bool MapNode::operator< (const MapNode& rhs) { return (this->m_totalCost < rhs.m_totalCost ); }

	
	std::string getInfo();

// private: // TODO: add this <-
	std::vector<std::shared_ptr<AbstractActor>> m_actors;

	std::shared_ptr<DynamicProp> m_dynamicProp;

	unsigned int m_weight;
	unsigned int m_cost;

	float m_costFromStart;	// G
	float m_heuristicCost;	// H
	float m_totalCost;		// F

	bool m_isAccessable;

	glm::vec2 m_position;
};