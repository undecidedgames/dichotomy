#pragma once

#include <memory>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <algorithm>
#include <SDL.h>
#include "../Renderables/Actors/ActorManager.h"
#include "../Config/ConfigManager.h"
#include "../Renderables/Props/Tile.h"
#include "../Renderables/Props/Prop.h"
#include "../Levels/MapNode.h"
#include "../Renderables/Props/Door.h"
#include "../Renderables/Props/Trap.h"
#include "../Renderables/Props/Trigger.h"
#include "../Audio/AudioManager.h"

using namespace std;

struct LightProperties {
	glm::vec3 ambient;
	GLint isEnabled;
	glm::vec3 color;
	GLint isLocal;
	glm::vec3 position;
	GLint isSpot;
	glm::vec3 halfVector;
	GLfloat spotCosCutoff;
	glm::vec3 coneDirection;
	GLfloat spotExponent;
	GLfloat range;
	GLfloat falloff;
};

class Level
{
public:
	Level(std::shared_ptr<ConfigManager> configManager, string levelName, std::shared_ptr<AudioManager> audioManager);
	~Level(void);

	void init();
	void update();
	void render();

	std::string getLevelName();
	void reloadLevel(bool reloadActors);
	bool loadLevel();

	string m_levelName;
	vector<vector<std::shared_ptr<MapNode>>> m_mapNodeGrid;

	vector<std::shared_ptr<Tile>> m_tiles;
	vector<std::shared_ptr<Prop>> m_props;
	
	vector<vector<std::shared_ptr<Trigger>>> m_triggers;
	vector<vector<std::shared_ptr<Door>>> m_doors;
	vector<vector<std::shared_ptr<Trap>>> m_traps;
	vector<std::shared_ptr<DynamicProp>> m_dynamicProps;

	std::vector<LightProperties> m_lightsViewSpace;
	std::vector<LightProperties> m_lightsWorldSpace;

	void addLight(LightProperties light);
	void updateLightVectors(glm::mat4 viewMatrix);
	void setLightArray(const GLuint program, LightProperties* lights, GLint size);
	void printMapNodeGrid();

	std::shared_ptr<ActorManager> m_actorManager;
	GLuint lightCount;
	
private:
	bool loadTiles(string filename);
	bool loadProps(string filename);
	bool loadActors(string filename);
	void initTriggers();
	void updateMapNodes();

	SDL_Surface* loadImage(string filename);

	Uint32 getPixel(SDL_Surface *surface, int x, int y);

	void comparePixelToTile(Uint32 pixel, SDL_PixelFormat* fmt, GLuint x, GLuint y);
	void comparePixelToProp(Uint32 pixel, SDL_PixelFormat* fmt, GLuint x, GLuint y);
	void comparePixelToActor(Uint32 pixel, SDL_PixelFormat* fmt, GLuint x, GLuint y);
	void generateTileMap();
	void generatePropMap();
	void generateActorMap();
			
	std::shared_ptr<ConfigManager> m_configManager;

	vector<string> m_tileList;
	vector<string> m_propList;
	vector<string> m_actorList;
	std::shared_ptr<AudioManager> m_audioManager;
	void viewFrustumCull();
	std::shared_ptr<Door> createDoor(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const bool toggleable);
	std::shared_ptr<Trap> createTrap(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const bool continuous, const bool active);
	std::shared_ptr<Trigger> createTrigger(const string&  modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const int targetID, const bool toggleable, const bool solid);
};

