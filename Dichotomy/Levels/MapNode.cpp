#include "../Levels/MapNode.h"

MapNode::MapNode(const unsigned int x, const unsigned int y, const unsigned int cost) : m_cost(cost), m_position(x,y)
{
	m_isAccessable = false;
	m_dynamicProp = nullptr;

	m_weight = m_cost;
	m_costFromStart = 0;
	m_heuristicCost = 0;
	m_totalCost = 0;
}


MapNode::~MapNode()
{
	m_actors.clear();
}


void MapNode::addActor(std::shared_ptr<AbstractActor> actor)
{
	m_actors.push_back(actor);
}


void MapNode::removeActor(std::shared_ptr<AbstractActor> actor)
{
	std::vector<std::shared_ptr<AbstractActor>>::iterator result = std::find(m_actors.begin(), m_actors.end(), actor);
	
	if(result != m_actors.end())
	{
		m_actors.erase(result);	
	}
}


void MapNode::addDynamicProp(std::shared_ptr<DynamicProp> dynamicProp)
{
	m_dynamicProp = dynamicProp;
}


bool MapNode::getAccessability()
{
	return m_isAccessable;
}


void MapNode::setAccessability(const bool isAccessable)
{
	m_isAccessable = isAccessable;
}


glm::vec2 MapNode::getPosition()
{
	return m_position;
}


void MapNode::setPosition(const GLfloat x, const GLfloat y)
{
	m_position = glm::vec2(x,y);
}


glm::vec3 MapNode::getVec3Position()
{
	return glm::vec3(m_position.x, 0.0f, m_position.y);
}


unsigned int MapNode::getCost()
{
	return m_cost;
}


unsigned int MapNode::getWeight()
{
	return m_weight;
}


void MapNode::setWeight(unsigned int weight)
{
	m_weight += weight; 
}


unsigned int MapNode::getCostFromStart()
{
	return m_costFromStart;
}

void MapNode::setCostFromStart(float costFromStart)
{
	m_costFromStart = costFromStart;
}


float MapNode::getHeuristicCost()
{
	return m_heuristicCost;
}


void MapNode::setHeuristicCost(float heuristicCost)
{
	m_heuristicCost = heuristicCost;
}


float MapNode::getTotalCost()
{
	return m_totalCost;
}


void MapNode::setTotalCost(float totalCost)
{
	m_totalCost = totalCost;
}


std::string MapNode::getInfo()
{
	std::stringstream ss;
	std::string node;
	
	if (m_isAccessable == true)
	{
		ss << m_weight;
		node = ss.str();
	}
	else node = "#";
	
	/*if (m_actors.size() != 0) 
	{
		if(m_actors.size() <= 9)
		{ 
			ss << m_actors.size();
			
			node = ss.str();
		}
		else node = "A";
	}*/

	if (m_dynamicProp != nullptr) node = "D";
			
	return node;
}