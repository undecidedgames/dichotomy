#pragma once

#include <vector>
#include <string>
#include <memory>
#include <unordered_map>
#include <SDL.h>

#include "Level.h"
#include "../Config/ConfigManager.h"
#include "../Renderables/Actors/NPCActor.h"
#include "../Renderables/Actors/PlayerActor.h"
#include "../Audio/AudioManager.h"

using namespace std;

/**
*	A manager class that deals with level loading, storage and updating.
*	@author B00216209 
*/
class LevelManager
{
public:
	/**
	*	Contructor, Cout's a message on start and initialises the shared pointers to config and audio managers.
	*	@param configManager A shared_ptr to the config manager
	*	@param audioManager	A shared_ptr to the audio manager
	*/
	LevelManager(std::shared_ptr<ConfigManager> configManager, std::shared_ptr<AudioManager> audioManager);

	/**
	*	Default destructor
	*/
	~LevelManager();

	/**
	*	Creates a shared pointer to a new level object and adds it to the level map
	*	@param levelName a string used as a key to the newly created level object in the levlel map
	*/
	void loadLevel(std::string levelName);

	/**
	*	Sets the current level shared_ptr to the level that matches the levelName in the map.
	*	@param levelName a string used as a key to get a level object from the map
	*/
	void setCurrentLevel(std::string levelName);

	/**
	*	Calls update() on the level set as current level
	*/
	void updateCurrentLevel();

	/**
	*	Returns a shared_ptr to the current level
	*	@returns a shared_ptr to the current level
	*/
	std::shared_ptr<Level> getCurrentLevel();

private:
	std::shared_ptr<Level> m_currentLevel; /**< The current level */
	std::unordered_map<std::string, std::shared_ptr<Level>> m_levelMap; /**< Map of levels to level names */
	std::shared_ptr<ConfigManager> m_configManager; /**< Shared pointer to the config manager */
	std::shared_ptr<AudioManager> m_audioManager; /**< Shared pointer to the audio manager */
};