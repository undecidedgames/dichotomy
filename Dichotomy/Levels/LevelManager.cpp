#include "LevelManager.h"


LevelManager::LevelManager(std::shared_ptr<ConfigManager> configManager, std::shared_ptr<AudioManager> audioManager)
{
	std::cout << "Level manager constructor" << std::endl;
	
	m_configManager = configManager;
	m_audioManager = audioManager;
}


LevelManager::~LevelManager(void)
{
	std::cout << "Level manager destructor" << std::endl;
}


void LevelManager::loadLevel(std::string levelName)
{
	std::shared_ptr<Level> tempLevel = std::shared_ptr<Level>(new Level(m_configManager, levelName, m_audioManager));
	
	tempLevel->init();

	m_levelMap[levelName] = tempLevel;
}


std::shared_ptr<Level> LevelManager::getCurrentLevel()
{
	return m_levelMap["currentLevel"];
}


void LevelManager::setCurrentLevel(std::string levelName)
{
	m_levelMap["currentLevel"] = m_levelMap[levelName];	
}
