#include "Level.h"


Level::Level(std::shared_ptr<ConfigManager> configManager, string levelName, std::shared_ptr<AudioManager> audioManager)
{
	std::cout << "Level constructor called: " << levelName << std::endl;

	m_actorManager = std::shared_ptr<ActorManager>(new ActorManager(audioManager));
	m_configManager = configManager;
	m_audioManager = audioManager;
	m_levelName = levelName;
}

Level::~Level(void)
{
	std::cout << "Level destructor called: " << m_levelName << std::endl;

	m_tiles.clear();
	m_props.clear();
	m_triggers.clear();
	m_traps.clear();
	m_doors.clear();
	
	m_tileList.clear();
	m_propList.clear();
	m_actorList.clear();

	m_lightsWorldSpace.clear();
	m_lightsViewSpace.clear();

	m_mapNodeGrid.clear();
}


void Level::init()
{
	for(size_t i = 0; i < 255; ++i)
	{
		vector<std::shared_ptr<Trigger>> temp;
		m_triggers.push_back(temp);
		vector<std::shared_ptr<Door>> temp2;
		m_doors.push_back(temp2);
		vector<std::shared_ptr<Trap>> temp3;
		m_traps.push_back(temp3);
	}
	
	LightProperties lights1;
	lights1.isEnabled = 0;
	lights1.isLocal = 0;
	lights1.isSpot = 0;
	lights1.ambient = glm::vec3(0.0f,0.0f,0.0f); // this light controls the global ambient
	lights1.color = glm::vec3(0.0f,0.0f,0.0f);
	lights1.position = glm::vec3(0.0f,0.0f,1.0f);
	lights1.halfVector = glm::vec3(0.0f);
	lights1.coneDirection = glm::vec3(0.0f);
	lights1.spotCosCutoff = 0.0f;
	lights1.spotExponent = 0.0f;
	lights1.range = 0.0f;
	lights1.falloff = 0.0f;
	addLight(lights1); // this is to get around the current bug that causes the light at index 0 to be ignored on AMD cards.

	generateTileMap();
	generatePropMap();
	generateActorMap();
	loadLevel();

	for(std::shared_ptr<Tile> x : m_tiles)
	{
		x->updateTBO();
	}
	for(std::shared_ptr<Prop> x : m_props)
	{
		x->updateTBO();
	}
}


void Level::reloadLevel(bool reloadActors)
{
	m_tiles.clear();
	m_props.clear();
	m_triggers.clear();
	m_traps.clear();
	m_doors.clear();
	m_dynamicProps.clear();
	m_tileList.clear();
	m_propList.clear();
	m_actorList.clear();
	m_mapNodeGrid.clear();

	m_lightsWorldSpace.clear();
	m_lightsViewSpace.clear();

	for(size_t i = 0; i < 255; ++i)
	{
		vector<std::shared_ptr<Trigger>> temp;
		m_triggers.push_back(temp);
		vector<std::shared_ptr<Door>> temp2;
		m_doors.push_back(temp2);
		vector<std::shared_ptr<Trap>> temp3;
		m_traps.push_back(temp3);
	}
	
	LightProperties lights1;
	lights1.isEnabled = 0;
	lights1.isLocal = 0;
	lights1.isSpot = 0;
	lights1.ambient = glm::vec3(0.0f,0.0f,0.0f); // this light controls the global ambient
	lights1.color = glm::vec3(0.0f,0.0f,0.0f);
	lights1.position = glm::vec3(0.0f,0.0f,1.0f);
	lights1.halfVector = glm::vec3(0.0f);
	lights1.coneDirection = glm::vec3(0.0f);
	lights1.spotCosCutoff = 0.0f;
	lights1.spotExponent = 0.0f;
	lights1.range = 0.0f;
	lights1.falloff = 0.0f;
	addLight(lights1); // this is to get around the current bug that causes the light at index 0 to be ignored on AMD cards.

	generateTileMap();
	generatePropMap();
	generateActorMap();

	loadTiles("assets/levels/" + m_levelName + "/" + m_levelName + "Tiles.bmp");
	loadProps("assets/levels/" + m_levelName + "/" + m_levelName + "Props.bmp");
		
	for(std::shared_ptr<Tile> x : m_tiles)
	{
		x->updateTBO();
	}

	for(std::shared_ptr<Prop>  x : m_props)
	{
		x->updateTBO();
	}

	if(reloadActors)
	{
		//m_actorManager.reset( new ActorManager(m_audioManager));
		m_actorManager->getActorVector()->clear();
		generateActorMap();
		loadActors("assets/levels/" + m_levelName + "/" + m_levelName + "Actors.bmp");
	}
}


bool Level::loadLevel()
{
	if(	loadTiles("assets/levels/" + m_levelName + "/" + m_levelName + "Tiles.bmp") &&
		loadProps("assets/levels/" + m_levelName + "/" + m_levelName + "Props.bmp") &&
		loadActors("assets/levels/" + m_levelName + "/" + m_levelName + "Actors.bmp")) 
		return true;
	else return false;
}


SDL_Surface* Level::loadImage(string levelName)
{
	const char* filePath = levelName.c_str();
	SDL_Surface* temp = SDL_LoadBMP(filePath);
	if (!temp) {
		//std::cout << "Error loading bitmap: " << IMG_GetError() << std::endl;
		return NULL;
	}
	else return temp;
}


bool Level::loadTiles(string levelName)
{
	if (m_tileList.size() == 0) 
	{
		return false;
	}

	SDL_Surface* tileImage = loadImage(levelName);
	if(tileImage == NULL)
		return false;
	else
	{
		SDL_LockSurface(tileImage);
		for(int i = 0; i < tileImage->w; ++i) // for each row
		{
			for (int j = 0; j < tileImage->h; ++j) // for each column
			{
				Uint32 pixel = getPixel(tileImage, i, j); // get the pixel
				comparePixelToTile(pixel, tileImage->format, i, j); // compare it to the tile map and create any necessary tiles
			}
		}
		SDL_UnlockSurface(tileImage);
	}

	// resize node grid to fit the current image
	m_mapNodeGrid.resize(tileImage->w);
	for(int index = 0; index < tileImage->w; ++index)
	{
		m_mapNodeGrid[index].resize(tileImage->h);
	}

	for(int i = 0; i < m_mapNodeGrid.size(); ++i)
	{
		for(int j = 0; j < m_mapNodeGrid[i].size(); ++j)
		{
			m_mapNodeGrid[i][j] = std::shared_ptr<MapNode>(new MapNode(i,j,1));
		}
	}

	for(std::shared_ptr<Tile> tile : m_tiles)
	{
		if(tile->getHalfBounds().y < 0.049f)
		{
			GLfloat position[2];
			for(glm::vec3 tmpPos : tile->m_positions)
			{
				position[0] = (GLuint)tmpPos.x;
				position[1] = (GLuint)tmpPos.z;
				m_mapNodeGrid[position[0]][position[1]]->setAccessability(true);
			}
		}
	}
	return true;
}


bool Level::loadProps(string levelName)
{
	if (m_propList.size() == 0) 
	{
		return false;
	}
	
	SDL_Surface* propImage = loadImage(levelName);
	if(propImage == NULL)
		return false;
	else
	{
		SDL_LockSurface(propImage);
		for(int i = 0; i < propImage->w; ++i) // for each row
		{
			for (int j = 0; j < propImage->h; ++j) // for each column
			{
				Uint32 pixel = getPixel(propImage, i, j); // get the pixel
				comparePixelToProp(pixel, propImage->format, i, j); // compare it to the tile map and create any necessary tiles
			}
		}
		SDL_UnlockSurface(propImage);
	}

	initTriggers();

	for(vector<std::shared_ptr<Trigger>> triggerVector : m_triggers)
	{
		m_dynamicProps.insert(m_dynamicProps.end(),triggerVector.begin(),triggerVector.end());
		triggerVector.clear();
	}

	for(vector<std::shared_ptr<Door>> doorVector : m_doors)
	{
		m_dynamicProps.insert(m_dynamicProps.end(),doorVector.begin(),doorVector.end());
		doorVector.clear();
	}

	for(vector<std::shared_ptr<Trap>> trapVector : m_traps)
	{
		m_dynamicProps.insert(m_dynamicProps.end(),trapVector.begin(),trapVector.end());
		trapVector.clear();
	}

	for(std::shared_ptr<Prop> tempProp : m_props)
	{
		if(tempProp->getHalfBounds().y > 0.049f)
		{
			GLfloat position[2];
			GLuint xAdditional = (GLuint)std::floor(tempProp->getHalfBounds().x);
			GLuint zAdditional = (GLuint)std::floor(tempProp->getHalfBounds().z);
			for(glm::vec3 tmpPos : tempProp->m_positions)
			{
				position[0] = tmpPos.x;
				position[1] = tmpPos.z;
				m_mapNodeGrid[(size_t)position[0]][(size_t)position[1]]->setAccessability(false);

				for(size_t count = 0; count < xAdditional; ++count)
				{
					if(position[0] - (count+1) >= 0)
					{
						m_mapNodeGrid[position[0] - (count+1)][position[1]]->setAccessability(false);
					}

					if(position[0] + (count+1) <= (GLuint)propImage->w)
					{
						m_mapNodeGrid[position[0] + (count+1)][position[1]]->setAccessability(false);
					}
				}

				for(size_t count = 0; count < zAdditional; ++count)
				{
					if(position[1] - (count+1) >= 0)
					{
						m_mapNodeGrid[position[0]][position[1] - (count+1)]->setAccessability(false);
					}

					if(position[1] + (count+1) <= (GLuint)propImage->h)
					{
						m_mapNodeGrid[position[0]][position[1] + (count+1)]->setAccessability(false);
					}
				}

			}
		}
	}
		
	for(std::shared_ptr<DynamicProp> dProp : m_dynamicProps)
	{
		GLuint position[2];
		GLuint xAdditional = (GLuint)std::floor(dProp->getHalfBounds().x);
		GLuint zAdditional = (GLuint)std::floor(dProp->getHalfBounds().z);
		bool isSolid = dProp->getSolid();
				
		position[0] = (GLuint)dProp->getPosition().x;
		position[1] = (GLuint)dProp->getPosition().z;
				
		m_mapNodeGrid[position[0]][position[1]]->setAccessability(true);
		m_mapNodeGrid[position[0]][position[1]]->addDynamicProp(dProp);
								

		for(size_t count = 0; count < xAdditional; ++count)
		{
			if(position[0] - (count+1) >= 0)
			{
				m_mapNodeGrid[position[0] - (count+1)][position[1]]->setAccessability(true);
				m_mapNodeGrid[position[0] - (count+1)][position[1]]->addDynamicProp(dProp);
			}

			if(position[0] + (count+1) <= (GLuint)propImage->w)
			{
				m_mapNodeGrid[position[0] + (count+1)][position[1]]->setAccessability(true);
				m_mapNodeGrid[position[0] - (count+1)][position[1]]->addDynamicProp(dProp);
			}
		}

		for(size_t count = 0; count < zAdditional; ++count)
		{
			if(position[1] - (count+1) >= 0)
			{
				m_mapNodeGrid[position[0]][position[1] - (count+1)]->setAccessability(true);
				m_mapNodeGrid[position[0] - (count+1)][position[1]]->addDynamicProp(dProp);
			}

			if(position[1] + (count+1) <= (GLuint)propImage->h)
			{
				m_mapNodeGrid[position[0]][position[1] + (count+1)]->setAccessability(true);
				m_mapNodeGrid[position[0] - (count+1)][position[1]]->addDynamicProp(dProp);
			}
		}
	}
	return true;
}


bool Level::loadActors(string levelName)
{
	if (m_actorList.size() == 0) 
	{
		return false;
	}
		
	SDL_Surface* actorImage = loadImage(levelName);
	if(actorImage == NULL)
		return false;
	else
	{
		SDL_LockSurface(actorImage);
		for(int i = 0; i < actorImage->w; ++i) // for each row
		{
			for (int j = 0; j < actorImage->h; ++j) // for each column
			{
				Uint32 pixel = getPixel(actorImage, i, j); // get the pixel
				comparePixelToActor(pixel, actorImage->format, i, j); // compare it to the tile map and create any necessary tiles
			}
		}
		SDL_UnlockSurface(actorImage);
	}
	updateMapNodes();

	return true;
}

/** bool initTriggers()
*	For each trigger, add all doors, traps and triggers (other than itself) as targets.
*	Then call setTargets which tells all the targets that something now targets them (increments their activation count) .
**/
void Level::initTriggers()
{
	for(vector<std::shared_ptr<Trigger>> triggerVector : m_triggers)
	{
		for(std::shared_ptr<Trigger> trigger : triggerVector)
		{
			for(std::shared_ptr<Trigger> targetTrigger : m_triggers[trigger->getTargetID()])
			{
				if (trigger->getID() != targetTrigger->getTargetID()) trigger->addTarget(targetTrigger);
			}

			for(std::shared_ptr<Door> targetDoor : m_doors[trigger->getTargetID()])
			{
				trigger->addTarget(targetDoor);
			}

			for(std::shared_ptr<Trap> targetTrap : m_traps[trigger->getTargetID()])
			{
				trigger->addTarget(targetTrap);
			}

			trigger->setTargets();
		}
	}
}

void Level::updateMapNodes()
{
	// for each actor in the vector
	for(size_t index = 0; index < m_actorManager->getActorVector()->size(); ++index)
	{
		std::shared_ptr<AbstractActor> tempActor = m_actorManager->getActor(index);

		// Get it's parameters for computing the nodes it should be in
		glm::vec2 position = glm::vec2(tempActor->getPosition().x,tempActor->getPosition().z);
		GLuint radius = 1 + (GLuint)(tempActor->getHalfBounds().x / 0.5);
		glm::vec2 startNode = glm::floor(position);
						
		// Remove this actor from each of it's old nodes
		for(size_t i = 0; i < tempActor->getMapNodes().size() ; ++i)
		{
			shared_ptr<MapNode> tempNode = tempActor->getMapNodes().at(i);
			tempNode->removeActor(tempActor);
			tempNode->setWeight(-1);
			if(tempNode->getPosition() == startNode) tempNode->setWeight(-3);
		}
		// Clear the vector of old nodes
		tempActor->getMapNodes().clear();

		// Add updated nodes to vector and tell each node that this actor is now on it.
		for(GLuint column = (GLuint)startNode.x - radius; column <= ((GLuint)startNode.x + radius) && column < m_mapNodeGrid.size() && column >= 0; ++column)
		{
			for(GLuint row = (GLuint)startNode.y - radius; row <= ((GLuint)startNode.y + radius) && row < m_mapNodeGrid[column].size() && row >= 0; ++row)
			{
				m_mapNodeGrid[column][row]->addActor(tempActor);
				m_mapNodeGrid[column][row]->setWeight(1);
				tempActor->addMapNode(m_mapNodeGrid[column][row]);
			}
		}
		m_mapNodeGrid[startNode.x][startNode.y]->setWeight(3);
	}

	for(size_t index = 1; index <= 2; ++index)
	{
		std::shared_ptr<PlayerActor> tempActor = m_actorManager->getPlayer(index);

		// Get it's parameters for computing the nodes it should be in
		glm::vec2 position = glm::vec2(tempActor->getPosition().x,tempActor->getPosition().z);
		GLuint radius = 1 + (GLuint)(tempActor->getHalfBounds().x / 1);
		glm::vec2 startNode = glm::floor(position);
						
		// Remove this actor from each of it's old nodes
		for(size_t i = 0; i < tempActor->getMapNodes().size() ; ++i)
		{
			shared_ptr<MapNode> tempNode = tempActor->getMapNodes().at(i);
			tempNode->removeActor(tempActor);
		}
		// Clear the vector of old nodes
		tempActor->getMapNodes().clear();

		// Add updated nodes to vector and tell each node that this actor is now on it.
		for(GLuint column = (GLuint)startNode.x - radius; column <= ((GLuint)startNode.x + radius) && column < m_mapNodeGrid.size() && column >= 0; ++column)
		{
			for(GLuint row = (GLuint)startNode.y - radius; row <= ((GLuint)startNode.y + radius) && row < m_mapNodeGrid[column].size() && row >= 0; ++row)
			{
				m_mapNodeGrid[column][row]->addActor(tempActor);
				tempActor->addMapNode(m_mapNodeGrid[column][row]);
			}
		}
	}
}



void Level::comparePixelToTile(Uint32 pixel, SDL_PixelFormat* fmt, GLuint x, GLuint y) // unfinished function (need to update for new config.info layout)
{
	Uint8 red, blue, green;
	SDL_GetRGB(pixel,fmt, &red, &green, &blue); // split the pixel into it's components
	char colorChar[10];
	int n = sprintf_s(colorChar,10,"%03u%03u%03u",red,green,blue); // converts the RGB Uint8 values to a single decimal string representation aka 255000255 for magenta
	if (n != 9) std::cout << "pixelRGB to string failed: comparePixelToTile() line 4" << std::endl;
	string colorString(colorChar); // convert the char array to a string for ease of comparison with values taken from config.info
	auto result = std::find(m_tileList.begin(),m_tileList.end(),colorString);

	if (result != m_tileList.end()) // Does this color exist in the tile list
	{
		bool found = false;
		int index = 0;
		for(GLuint i=0; i < m_tiles.size(); ++i) // If yes iterate over the vector of tiles and see if it already exists
		{
			if(m_tiles[i]->getModelPath() == m_configManager->m_propertyTree.get<string>("levelManager." + m_levelName + ".tiles." + colorString +".modelPath"))
			{
				index = i;
				found = true;
				break; // no point continuing the loop once it's found
			}
		}

		glm::vec3 position = glm::vec3(x,0.0f,y); // position in texture determines world-space position

		glm::vec3 rotation = glm::vec3(	m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".tiles." + colorString +".rotation.x"),
			m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".tiles." + colorString +".rotation.y"),
			m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".tiles." + colorString +".rotation.z"));

		if(found) m_tiles[index]->addTile(position,rotation); //  if found, add a new instance at the current coordinates
		else //  If not found, create a new tile object
		{
			glm::vec3 halfBounds = glm::vec3(	m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".tiles." + colorString +".halfBounds.x"),
				m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".tiles." + colorString +".halfBounds.y"),
				m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".tiles." + colorString +".halfBounds.z"));



			string modelPath = m_configManager->m_propertyTree.get<string>("levelManager." + m_levelName + ".tiles." + colorString +".modelPath");
			string texturePath = m_configManager->m_propertyTree.get<string>("levelManager." + m_levelName + ".tiles." + colorString +".texturePath");
			//int phase = m_configManager->m_propertyTree.get<int>("levelManager." + m_levelName + ".tiles." + colorString +".phase");
			std::shared_ptr<Tile> temp = std::shared_ptr<Tile>(new Tile(modelPath,texturePath, position,rotation, halfBounds, 2));
			temp->addTile(position,rotation);
			m_tiles.push_back(temp); // push it into the vector
		}
	}	
}


void Level::comparePixelToProp(Uint32 pixel, SDL_PixelFormat* fmt, GLuint x, GLuint y) // unfinished function (need to update for new config.info layout)
{
	Uint8 red, blue, green;
	int n;

	SDL_GetRGB(pixel,fmt, &red, &green, &blue); // split the pixel into it's components

	char colorChar[10];
	if(green == 000)
		n = sprintf_s(colorChar, 10, "%03u%03u%03u", red, green, blue); // converts the RGB Uint8 values to a single decimal string representation aka 255000255 for magenta
	else
		n = sprintf_s(colorChar, 10, "000000%03u", blue); // converts the RGB Uint8 values to a single decimal string representation aka 255000255 for magenta
		
	if (n != 9) std::cout << "pixelRGB to string failed: comparePixelToProp() line 4" << std::endl;
	string colorString(colorChar); // convert the char array to a string for ease of comparison with values taken from config.info
	auto result = std::find(m_propList.begin(),m_propList.end(),colorString);

	if (result != m_propList.end()) // Does this color exist in the tile list
	{
		if(green == 000)	// This pixel represents a static prop
		{
			bool found = false;
			int index = 0;
			for(GLuint i=0; i < m_props.size(); ++i) // If yes iterate over the vector of tiles and see if it already exists
			{
				if(m_props[i]->getModelPath() == m_configManager->m_propertyTree.get<string>("levelManager." + m_levelName + ".props." + colorString +".modelPath"))
				{
					index = i;
					found = true;
					break; // no point continuing the loop once it's found
				}
			}

			glm::vec3 position = glm::vec3(x,0.0f,y); // position in texture determines world-space position

			if(m_configManager->m_propertyTree.get<bool>("levelManager." + m_levelName + ".props." + colorString +".hasLight"))
			{
				LightProperties tempLight;
				tempLight.isEnabled = m_configManager->m_propertyTree.get<bool>("levelManager." + m_levelName + ".props." + colorString +".light.isEnabled");
				tempLight.isLocal = m_configManager->m_propertyTree.get<bool>("levelManager." + m_levelName + ".props." + colorString +".light.isLocal");
				tempLight.isSpot = m_configManager->m_propertyTree.get<bool>("levelManager." + m_levelName + ".props." + colorString +".light.isSpot");
				
				tempLight.ambient =  glm::vec3(	m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.ambient.x"),
												m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.ambient.y"),
												m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.ambient.z"));
				
				tempLight.color = glm::vec3(	m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.color.x"),
												m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.color.y"),
												m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.color.z"));
				
				tempLight.position = glm::vec3(	m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.position.x"),
												m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.position.y"),
												m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.position.z")) + position;
				
				tempLight.halfVector =glm::vec3(m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.halfVector.x"),
												m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.halfVector.y"),
												m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.halfVector.z"));
				
				tempLight.coneDirection = glm::vec3(m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.coneDirection.x"),
												m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.coneDirection.y"),
												m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.coneDirection.z"));
				
				tempLight.spotCosCutoff = m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.spotCosCutoff");
				tempLight.spotExponent = m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.spotExponent");
				tempLight.range = m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.range");
				tempLight.falloff =	m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".light.falloff");
				addLight(tempLight);
			}



			glm::vec3 rotation = glm::vec3(	m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".rotation.x"),
											m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".rotation.y"),
											m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".rotation.z"));

			if(found) m_props[index]->addProp(position,rotation); //  if found, add a new instance at the current coordinates
			else //  If not found, create a new tile object
			{
				glm::vec3 halfBounds = glm::vec3(	m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".halfBounds.x"),
													m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".halfBounds.y"),
													m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".halfBounds.z"));



				string modelPath = m_configManager->m_propertyTree.get<string>("levelManager." + m_levelName + ".props." + colorString +".modelPath");
				string texturePath = m_configManager->m_propertyTree.get<string>("levelManager." + m_levelName + ".props." + colorString +".texturePath");
				//int phase = m_configManager->m_propertyTree.get<int>("levelManager." + m_levelName + ".props." + colorString +".phase");

				std::shared_ptr<Prop> temp = std::shared_ptr<Prop>(new Prop(modelPath,texturePath, position,rotation, halfBounds,2));
				temp->addProp(position,rotation);
				m_props.push_back(temp); // push it into the vector
			}
		}

		else	// Door, Trap or Trigger
		{
			int numberOfPhases = 3;
			int units = blue % 10;	// Extracts the units
			int tens = ( blue / 10 ) % 10;	// Extract the tens
			int phase;
			bool toggleable = true, continuous = true, active = true, solid = true;
			// All dynamic props need this information
			string modelPath = m_configManager->m_propertyTree.get<string>("levelManager." + m_levelName + ".props." + colorString +".modelPath");
			string texturePath = m_configManager->m_propertyTree.get<string>("levelManager." + m_levelName + ".props." + colorString +".texturePath");

			glm::vec3 position = glm::vec3(x,0.0f,y); // position in texture determines world-space position

			glm::vec3 rotation = glm::vec3(	m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".rotation.x"),
				m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".rotation.y"),
				m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".rotation.z"));

			glm::vec3 halfBounds = glm::vec3(	m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".halfBounds.x"),
				m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".halfBounds.y"),
				m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".props." + colorString +".halfBounds.z"));

			if(red == 000)	// Door or Trap
			{
				if( blue >= 150 )	// Door
				{
					// Get the phase and toggleable from the blue channel
					phase = units;
					if( units >= numberOfPhases )
					{
						phase = units - numberOfPhases;
						toggleable = false;
					}

					std::shared_ptr<Door> temp = createDoor(modelPath, texturePath, position, rotation, halfBounds, green, phase, toggleable);
					m_doors[green].push_back(temp);
				}
				else	// Trap
				{
					active = true;
					continuous = true;
					// Get the phase and active from the blue channel
					phase = units;
					if( units >= numberOfPhases )
					{
						phase = units - numberOfPhases;
						active = false;
					}
					// Get continuous from blue channel 
					if( tens % 2 == 0)	// if tens are even continuous is false
						continuous = false;

					std::shared_ptr<Trap> temp = createTrap(modelPath, texturePath, position, rotation, halfBounds, green, phase, continuous, active);
					m_traps[green].push_back(temp);
				}
			}
			else	// Trigger
			{
				// Get the phase and toggleable from the blue channel
				phase = units;
				if( units >= numberOfPhases )
				{
					phase = units - numberOfPhases;
					toggleable = false;
				}
				// Get continuous from blue channel
				// need a special case for triggers
				if( tens % 2 == 0  && tens != 0)	// if tens are even continuous is false
					solid = false;

				std::shared_ptr<Trigger> temp = createTrigger(modelPath, texturePath, position, rotation, halfBounds, green, phase, red, toggleable, solid);
				
				m_triggers[(size_t)green].push_back(temp);
			}
		}
	}
}


void Level::comparePixelToActor(Uint32 pixel, SDL_PixelFormat* fmt, GLuint x, GLuint y) // unfinished function (need to update for new config.info layout)
{
	Uint8 red, blue, green;
	SDL_GetRGB(pixel,fmt, &red, &green, &blue); // split the pixel into it's components
	char colorChar[10];
	int n = sprintf_s(colorChar,10,"%03u%03u%03u",red,green,blue); // converts the RGB Uint8 values to a single decimal string representation aka 255000255 for magenta
	if (n != 9) std::cout << "pixelRGB to string failed: comparePixelToProp() line 4" << std::endl;
	string colorString(colorChar); // convert the char array to a string for ease of comparison with values taken from config.info
	auto result = std::find(m_actorList.begin(),m_actorList.end(),colorString);

	if (result != m_actorList.end()) // Does this color exist in the tile list
	{
		const glm::vec3 rotation = glm::vec3(	m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".actors." + colorString +".rotation.x"),
			m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".actors." + colorString +".rotation.y"),
			m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".actors." + colorString +".rotation.z"));

		const glm::vec3 halfBounds = glm::vec3(	m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".actors." + colorString +".halfBounds.x"),
			m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".actors." + colorString +".halfBounds.y"),
			m_configManager->m_propertyTree.get<float>("levelManager." + m_levelName + ".actors." + colorString +".halfBounds.z"));

		const string modelPath = m_configManager->m_propertyTree.get<string>("levelManager." + m_levelName + ".actors." + colorString +".modelPath");
		const string texturePath = m_configManager->m_propertyTree.get<string>("levelManager." + m_levelName + ".actors." + colorString +".texturePath");

		const GLuint tickrate = m_configManager->m_propertyTree.get<GLuint>("gameTimer.ticksPerSecond");

		const string namechar = m_configManager->m_propertyTree.get<string>("levelManager." + m_levelName + ".actors." + colorString +".name");
		
		if(namechar == "MeleeNPC")
		{
			GLuint phase = m_configManager->m_propertyTree.get<int>("levelManager." + m_levelName + ".actors." + colorString +".phase");
			m_actorManager->createNPCActor(modelPath, texturePath, glm::vec3(x, 0.0f, y), rotation, halfBounds, tickrate, phase);
			std::shared_ptr<NPCActor> temp = m_actorManager->getActor(m_actorManager->getActorVector()->size()-1);
			temp->setMapNodeGridPointer(std::make_shared<vector<vector<std::shared_ptr<MapNode>>>>(m_mapNodeGrid));
		}
		else 
		{
			if(namechar == "Player1")
			{
				std::shared_ptr<PlayerActor> temp = std::shared_ptr<PlayerActor>(new PlayerActor(modelPath, texturePath, glm::vec3(x, 0.0f, y), rotation, halfBounds, 0, true));
				m_actorManager->setPlayer(temp,true);
				temp->setMapNodeGridPointer(std::make_shared<vector<vector<std::shared_ptr<MapNode>>>>(m_mapNodeGrid));
			}
			else
			{
				std::shared_ptr<PlayerActor> temp = std::shared_ptr<PlayerActor>(new PlayerActor(modelPath, texturePath, glm::vec3(x, 0.0f, y), rotation, halfBounds, 1, false));
				m_actorManager->setPlayer(temp,false);
				temp->setMapNodeGridPointer(std::make_shared<vector<vector<std::shared_ptr<MapNode>>>>(m_mapNodeGrid));
			}
		}
	}	
}


void Level::generateTileMap()
{
	char colorChar[10];
	int n;
	for(short i = 0; i <= 255; ++i) 
	{
		n = sprintf_s(colorChar, 10, "000000%03u", i); // create string for each color
		bool found = true;
		string temp;
		try
		{	
			string path = "levelManager." + m_levelName + ".tiles." + colorChar + ".color";		
			temp = m_configManager->m_propertyTree.get<string>(path); // check to see if it exists in the info file
		}
		catch (boost::property_tree::ptree_bad_path)
		{
			found  = false; // if it's not set found to false
		}
		if(found)
		{
			m_tileList.push_back(colorChar); // if the color is found, add it to the vector of valid tiles.
		}
	}
	std::cout << "TileMap generated: " << m_tileList.size() << " tiles found in config.info" << std::endl;
}


void Level::generatePropMap()
{
	char colorChar[10];
	int n;
	for(short i = 0; i <= 255; ++i) 
	{
		n = sprintf_s(colorChar,10,"000000%03u",i); // create string for each color
		bool found = true;
		string temp;
		try
		{	
			string path = "levelManager." + m_levelName + ".props." + colorChar + ".color";		
			temp = m_configManager->m_propertyTree.get<string>(path); // check to see if it exists in the info file
		}
		catch (boost::property_tree::ptree_bad_path)
		{
			found  = false; // if it's not set found to false
		}
		if(found)
		{
			m_propList.push_back(colorChar); // if the color is found, add it to the vector of valid tiles.
		}
	}
	std::cout << "PropMap generated: " << m_propList.size() << " props found in config.info" << std::endl;
}


void Level::generateActorMap()
{
	char colorChar[10];
	int n;
	for(short i = 0; i <= 255; ++i) 
	{
		n = sprintf_s(colorChar,10,"000000%03u",i); // create string for each color
		bool found = true;
		string temp;
		try
		{	
			string path = "levelManager." + m_levelName + ".actors." + colorChar + ".color";		
			temp = m_configManager->m_propertyTree.get<string>(path); // check to see if it exists in the info file
		}
		catch (boost::property_tree::ptree_bad_path)
		{
			found  = false; // if it's not set found to false
		}
		if(found)
		{
			m_actorList.push_back(colorChar); // if the color is found, add it to the vector of valid tiles.
		}
	}
	std::cout << "ActorMap generated: " << m_actorList.size() << " Actors found in config.info" << std::endl;
}


void Level::printMapNodeGrid()
{
	ofstream mapASCII;
	mapASCII.open("assets/levels/" + m_levelName + "/" + m_levelName + "MapNodeGrid.txt");

	mapASCII << "Map Name: " << m_levelName << endl;
	mapASCII << "Map Size: " << m_mapNodeGrid.size() << ", " << m_mapNodeGrid[0].size() << endl << endl;
	
	for(size_t row = 0; row < m_mapNodeGrid[0].size(); ++row)
	{
		for(size_t column = 0; column < m_mapNodeGrid.size() ; ++column)
		{
			shared_ptr<MapNode> temp = m_mapNodeGrid[column][row];
			if(temp != nullptr) mapASCII << temp->getInfo();
			else mapASCII << " ";
		}
		mapASCII << endl;
	}

	mapASCII.close();
}


// Lifted from sdl.beyc.net/sdl.wiki/Pixel_Access
Uint32 Level::getPixel(SDL_Surface *surface, int x, int y)
{
	int bpp = surface->format->BytesPerPixel;
	/* Here p is the address to the pixel we want to retrieve */
	Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

	switch(bpp) {
	case 1:
		return *p;
		break;

	case 2:
		return *(Uint16 *)p;
		break;

	case 3:
		if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
			return p[0] << 16 | p[1] << 8 | p[2];
		else
			return p[0] | p[1] << 8 | p[2] << 16;
		break;

	case 4:
		return *(Uint32 *)p;
		break;

	default:
		return 0;       /* shouldn't happen, but avoids warnings */
	}
}


// function from B00216209's AGP project.
void Level::updateLightVectors(glm::mat4 viewMatrix)
{
	Uint32 time = SDL_GetTicks();
	GLfloat seconds = (GLfloat)(time);
	GLfloat quarterSeconds = seconds * 0.00125f;
	GLfloat intensity = glm::sin(quarterSeconds) * 0.125f + 0.75f;
	
	GLfloat rand = 0.0f + static_cast <float> (std::rand()) /( static_cast <float> (RAND_MAX/(0.10f-0.0f)));
	GLfloat rand1 = 0.0f + static_cast <float> (std::rand()) /( static_cast <float> (RAND_MAX/(0.10f-0.0f)));
	GLfloat rand2 = 0.0f + static_cast <float> (std::rand()) /( static_cast <float> (RAND_MAX/(0.10f-0.0f)));

	glm::vec3 offset = glm::vec3(rand * intensity, rand1 * intensity , rand2 * intensity );

	for(GLuint i = 0; i < m_lightsViewSpace.size(); ++i)
	{
		m_lightsViewSpace[i].isEnabled = m_lightsWorldSpace[i].isEnabled;
		m_lightsViewSpace[i].color = m_lightsWorldSpace[i].color * intensity;
		
		if (m_lightsViewSpace[i].isLocal == 0)
		{
			m_lightsViewSpace[i].position = glm::mat3(viewMatrix) * m_lightsWorldSpace[i].position;
			m_lightsViewSpace[i].halfVector = glm::mat3(viewMatrix) * glm::normalize(glm::normalize(m_lightsViewSpace[i].position) + glm::vec3(0.0f,0.0f,1.0f));
		}
		else
		{
			m_lightsViewSpace[i].position = glm::vec3(viewMatrix * glm::vec4(m_lightsWorldSpace[i].position + offset,1.0f));
			if (m_lightsViewSpace[i].isSpot == 1)
			{
				m_lightsViewSpace[i].spotCosCutoff  = m_lightsWorldSpace[i].spotCosCutoff;
				m_lightsViewSpace[i].spotExponent  = m_lightsWorldSpace[i].spotExponent;
				m_lightsViewSpace[i].coneDirection = glm::mat3(viewMatrix) * (m_lightsWorldSpace[i].coneDirection);
			}
		}
	}
}


void Level::addLight(LightProperties light)
{
	m_lightsWorldSpace.push_back(light);
	m_lightsViewSpace.push_back(light);
}


void Level::setLightArray(const GLuint program, LightProperties* LightArray, GLint size)
{
	lightCount = 1;
	// for each light in the array
	for(int i=1; i < size+1; ++i)
	{
		// check if it's enabled, if true, send it to the shader using the lightCount as the uniform index and the i variable as the array index
		if(LightArray[i].isEnabled == true)
		{
			char loc[40];
			sprintf_s(loc,40,"LightArray[%d].isEnabled",lightCount);
			GLint uniformIndex = glGetUniformLocation(program, loc);
			glUniform1i(uniformIndex, LightArray[i].isEnabled);

			sprintf_s(loc,40,"LightArray[%d].isLocal", lightCount);
			uniformIndex = glGetUniformLocation(program, loc);
			glUniform1i(uniformIndex, LightArray[i].isLocal);

			sprintf_s(loc,40,"LightArray[%d].isSpot",lightCount);
			uniformIndex = glGetUniformLocation(program, loc);
			glUniform1i(uniformIndex, LightArray[i].isSpot);

			sprintf_s(loc,40,"LightArray[%d].ambient",lightCount);
			uniformIndex = glGetUniformLocation(program, loc);
			glUniform3fv(uniformIndex,1, glm::value_ptr(LightArray[i].ambient));

			sprintf_s(loc,40,"LightArray[%d].color",lightCount);
			uniformIndex = glGetUniformLocation(program, loc);
			glUniform3fv(uniformIndex,1, glm::value_ptr(LightArray[i].color));

			sprintf_s(loc,40,"LightArray[%d].position",lightCount);
			uniformIndex = glGetUniformLocation(program, loc);
			glUniform3fv(uniformIndex,1, glm::value_ptr(LightArray[i].position));

			sprintf_s(loc,40,"LightArray[%d].halfVector",lightCount);
			uniformIndex = glGetUniformLocation(program, loc);
			glUniform3fv(uniformIndex,1, glm::value_ptr(LightArray[i].halfVector));

			sprintf_s(loc,40,"LightArray[%d].coneDirection",lightCount);
			uniformIndex = glGetUniformLocation(program, loc);
			glUniform3fv(uniformIndex,1, glm::value_ptr(LightArray[i].coneDirection));

			sprintf_s(loc,40,"LightArray[%d].spotCosCutoff",lightCount);
			uniformIndex = glGetUniformLocation(program, loc);
			glUniform1f(uniformIndex, LightArray[i].spotCosCutoff);

			sprintf_s(loc,40,"LightArray[%d].spotExponent",lightCount);
			uniformIndex = glGetUniformLocation(program, loc);
			glUniform1f(uniformIndex, LightArray[i].spotExponent);

			sprintf_s(loc,40,"LightArray[%d].range",lightCount);
			uniformIndex = glGetUniformLocation(program, loc);
			glUniform1f(uniformIndex, LightArray[i].range);

			sprintf_s(loc,40,"LightArray[%d].falloff",lightCount);
			uniformIndex = glGetUniformLocation(program, loc);
			glUniform1f(uniformIndex, LightArray[lightCount].falloff);
			
			lightCount++;
		}
	}
}


void Level::update()
{
	m_actorManager->update();
	for(std::shared_ptr<DynamicProp> dProp : m_dynamicProps)
	{
		dProp->update();
	}
}


std::string Level::getLevelName()
{
	return m_levelName;
}

std::shared_ptr<Door> Level::createDoor(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const bool toggleable)
{
	bool found = false;

	GLuint tex0;
	GLuint tex1;
	GLuint tex2;
	GLuint tex3;
	GLuint tex4;
	GLuint tex5;
		
	for(int i = 0; i < m_doors.size(); ++i)
	{
		for(int j = 0; j < m_doors[i].size(); ++j)
		{
			if (m_doors[i][j]->getTexturePath() == texturePath)
			{
				found = true;
				tex0 = m_doors[i][j]->getTexture();
				tex1 = m_doors[i][j]->getNormalTexture();
				tex2 = m_doors[i][j]->getSpecularTexture();
				tex3 = m_doors[i][j]->getTexture2();
				tex4 = m_doors[i][j]->getNormalTexture2();
				tex5 = m_doors[i][j]->getSpecularTexture2();
			}
		}
	}

	std::shared_ptr<Door> temp;

	if (found== true)
	{
		 temp = std::shared_ptr<Door>(new Door(modelPath, texturePath, position, orientation, halfBounds, id, phase, toggleable, tex0, tex1, tex2, tex3, tex4, tex5));
	}
	else
	{
		temp = std::shared_ptr<Door>(new Door(modelPath, texturePath, position, orientation, halfBounds, id, phase, toggleable));
	}
	return temp;
}

std::shared_ptr<Trap> Level::createTrap(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const bool continuous, const bool active)
{
	bool found = false;

	GLuint tex0;
	GLuint tex1;
	GLuint tex2;
	GLuint tex3;
	GLuint tex4;
	GLuint tex5;
		
	for(int i = 0; i < m_traps.size(); ++i)
	{
		for(int j = 0; j < m_traps[i].size(); ++j)
		{
			if (m_traps[i][j]->getTexturePath() == texturePath)
			{
				found = true;
				tex0 = m_traps[i][j]->getTexture();
				tex1 = m_traps[i][j]->getNormalTexture();
				tex2 = m_traps[i][j]->getSpecularTexture();
				tex3 = m_traps[i][j]->getTexture2();
				tex4 = m_traps[i][j]->getNormalTexture2();
				tex5 = m_traps[i][j]->getSpecularTexture2();
			}
		}
	}

	std::shared_ptr<Trap> temp;

	if (found== true)
	{
		 temp = std::shared_ptr<Trap>(new Trap(modelPath, texturePath, position, orientation, halfBounds, id, phase, continuous, active, tex0, tex1, tex2, tex3, tex4, tex5));
	}
	else
	{
		temp = std::shared_ptr<Trap>(new Trap(modelPath, texturePath, position, orientation, halfBounds, id, phase, continuous, active));
	}
	return temp;
}

std::shared_ptr<Trigger> Level::createTrigger(const string&  modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const int targetID, const bool toggleable, const bool solid)
{
	bool found = false;

	GLuint tex0;
	GLuint tex1;
	GLuint tex2;
	GLuint tex3;
	GLuint tex4;
	GLuint tex5;
		
	for(int i = 0; i < m_triggers.size(); ++i)
	{
		for(int j = 0; j < m_triggers[i].size(); ++j)
		{
			if (m_triggers[i][j]->getTexturePath() == texturePath)
			{
				found = true;
				tex0 = m_triggers[i][j]->getTexture();
				tex1 = m_triggers[i][j]->getNormalTexture();
				tex2 = m_triggers[i][j]->getSpecularTexture();
				tex3 = m_triggers[i][j]->getTexture2();
				tex4 = m_triggers[i][j]->getNormalTexture2();
				tex5 = m_triggers[i][j]->getSpecularTexture2();
			}
		}
	}

	std::shared_ptr<Trigger> temp;

	if (found== true)
	{
		 temp = std::shared_ptr<Trigger>(new Trigger(modelPath, texturePath, position, orientation, halfBounds, id, phase, targetID, toggleable, solid, tex0, tex1, tex2, tex3, tex4, tex5));
	}
	else
	{
		temp = std::shared_ptr<Trigger>(new Trigger(modelPath, texturePath, position, orientation, halfBounds, id, phase, targetID, toggleable, solid));
	}
	return temp;
}