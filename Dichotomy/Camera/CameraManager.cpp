#include "CameraManager.h"

CameraManager::CameraManager(glm::vec3 position, glm::vec3 forward, glm::vec3 up, GLfloat fov, GLuint width, GLuint height, GLfloat nearclip, GLfloat farclip)
{
	m_position = position; // set position of camera
	m_forward = forward;
	m_up = up; // set up for camera
	m_fov = fov; // field of view
	m_width = (GLfloat)width; // width of screen
	m_height = (GLfloat)height; // height of screen
	m_aspect = m_width/m_height; // aspect ratio
	m_near = nearclip; // near clip
	m_far = farclip; // far clip
	m_projectionMatrix = glm::perspective(fov,m_aspect,nearclip,farclip); // projection matrix
	updateViewMatrix();
}

CameraManager::~CameraManager()
{
}

void CameraManager::Update()
{
	// currently empty but could be used to have finer camera controls such as moving towards it's target or applying dampening to movements.
}
	
void CameraManager::setViewMembers(glm::vec3 position, glm::vec3 target, glm::vec3 up)
{
	m_position = position; // set position of camera
	m_forward = glm::normalize(target - m_position);
	m_up = up; // set up for camera
	updateViewMatrix();
}
	
void CameraManager::setProjectionMembers(GLfloat fov, GLfloat width, GLfloat height, GLfloat nearclip, GLfloat farclip)
{
	m_fov = fov; // field of view
	m_width = width; // width of screen
	m_height = height; // height of screen
	m_aspect = m_width/m_height; // aspect ratio
	m_near = nearclip; // near clip
	m_far = farclip; // far clip
	m_projectionMatrix = glm::perspective(fov,m_aspect,nearclip,farclip); // projection matrix
}

bool CameraManager::setForward(glm::vec3 target)
{
	//credit http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/
	glm::vec3 newForward = glm::normalize(target - m_position);
	glm::fquat rotation1 = RotationBetweenVectors(m_forward,newForward);

	m_forward =  rotation1 * m_forward;
	m_up = rotation1 * m_up;
	m_viewMatrix = glm::lookAt(m_position,m_position + m_forward, m_up);
	m_orientation = glm::quat_cast(m_viewMatrix);
	updateViewMatrix();
	return true;
}

bool CameraManager::setPosition(glm::vec3 position)
{
	m_position = position;
	updateViewMatrix();
	return true;
}

void CameraManager::updateViewMatrix()
{
	m_viewMatrix = glm::lookAt(m_position,m_position+m_forward,glm::vec3(0.0f,1.0f,0.0f));
}

glm::mat4 CameraManager::getViewMatrix()
{
	return m_viewMatrix; // returns the viewMatrix
}

glm::mat4 CameraManager::getProjectionMatrix()
{
	return m_projectionMatrix;
}

glm::mat3 CameraManager::getNormalMatrix(glm::mat4 modelMatrix)
{
	 glm::mat3 normalMatrix = glm::transpose(glm::inverse(glm::mat3(getViewMatrix()*modelMatrix))); // transpose of the inverse of the mat3 version of the modelview matrix
	 return normalMatrix;
}

glm::mat3 CameraManager::getRotationViewMatrix()
{
	glm::mat3 viewMatrix = glm::mat3(getViewMatrix()); // takes out the scale and translate so it just has rotations
	return viewMatrix;
}

GLfloat CameraManager::getFov()
{
	return m_fov; // return field of view
}

GLfloat CameraManager::getWidth()
{
	return m_width; // return width of window
}

GLfloat CameraManager::getHeight()
{
	return m_height; // return height of window
}

GLfloat CameraManager::getAspectRatio()
{
	return m_aspect; //return aspect ratio
}

GLfloat CameraManager::getNear()
{
	return m_near; // return near clip
}
	
GLfloat CameraManager::getFar()
{
	return m_far; // return far clip
}

glm::vec3 CameraManager::getPosition()
{
	return m_position; // return position
}

glm::vec3 CameraManager::getUp()
{
	return m_up; // return up
}

glm::vec3 CameraManager::getForward()
{
	return m_forward; // return forward
}

glm::vec3 CameraManager::getRight()
{
	return -glm::cross(m_forward,m_up);
}

void CameraManager::rotateCamera(glm::vec3 axis, GLfloat angle)
{
	glm::fquat rotation = glm::angleAxis(angle,axis);

	m_forward = glm::conjugate(rotation) *  m_forward;
	m_up = glm::conjugate(rotation) * m_up;

	m_orientation = rotation * m_orientation;
	
	updateViewMatrix();
}

void CameraManager::resetRotation()
{
	m_forward = glm::vec3(0.0f,0.0f,-1.0f);
	m_up = glm::vec3(0.0f,1.0f,0.0f);
	m_orientation = glm::fquat();
	updateViewMatrix();
}

glm::fquat CameraManager::RotationBetweenVectors(glm::vec3 start, glm::vec3 dest)
{
	// credit http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/
	using namespace glm;

    start = normalize(start);
    dest = normalize(dest);
 
    float cosTheta = dot(start, dest);
    vec3 rotationAxis;
 
    if (cosTheta < -1 + 0.001f){
        // special case when vectors in opposite directions:
        // there is no "ideal" rotation axis
        // So guess one; any will do as long as it's perpendicular to start
        rotationAxis = cross(vec3(0.0f, 0.0f, 1.0f), start);
        if (length2(rotationAxis) < 0.01 ) // bad luck, they were parallel, try again!
            rotationAxis = cross(vec3(1.0f, 0.0f, 0.0f), start);
 
        rotationAxis = normalize(rotationAxis);
        return angleAxis(180.0f, rotationAxis);
    }
 
    rotationAxis = cross(start, dest);
 
    float s = sqrt( (1+cosTheta)*2 );
    float invs = 1 / s;
 
    return fquat(
        s * 0.5f, 
        rotationAxis.x * invs,
        rotationAxis.y * invs,
        rotationAxis.z * invs
    );
}