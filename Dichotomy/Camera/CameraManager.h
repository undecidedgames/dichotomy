#pragma once

#include <iostream>
#include <math.h>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

/**
*	A manager class that deals with the view and projection matrices
*	@author B00216209
*/
class CameraManager
{
public:
	/**
	*	Constructor.
	*	Sets the starting values for the member variables needed to create the view and project matrices.
	*	@param position the camera's starting position
	*	@param target	the camera's starting target
	*	@param up		the camera's starting up vecto
	*	@param fov		the projection matrix's starting field of view
	*	@param width	width of the window, used for project matrix aspect ratio calculation
	*	@param height	width of the window, used for project matrix aspect ratio calculation
	*	@param nearclip		the projection matrix's starting near clip
	*	@param farclip		the projection matrix's starting far clip
	*/
	CameraManager(glm::vec3 position, glm::vec3 target, glm::vec3 up, GLfloat fov, GLuint width, GLuint height, GLfloat nearclip, GLfloat farclip);

	/**
	*	Default deconstructor
	*/
	~CameraManager();

	/**
	*	Update function, currently empty.
	*/
	void Update();

	/**
	*	Sets new values to the variables used for the view matrix and upates the view matrix.
	*	@param position	Camera position
	*	@param forward	Unit Vector camera is facing in.
	*	@param up	Unit Vector "up" from the camera.
	*/
	void setViewMembers(glm::vec3 position, glm::vec3 forward, glm::vec3 up);

	/**
	*	Sets new values to the variables used for the projection matrix and updates the project matrix.
	*	@param	fov	Vertical dield of view in degrees
	*	@param width	width of the window, used for project matrix aspect ratio calculation
	*	@param height	width of the window, used for project matrix aspect ratio calculation
	*	@param nearclip	the projection matrix's starting near clip
	*	@param farclip	the projection matrix's starting far clip
	*/
	void setProjectionMembers(GLfloat fov, GLfloat width, GLfloat height, GLfloat nearclip, GLfloat farclip);

	/**
	*	Sets the forward vector of the camera (what it's pointing at)
	*	@param target The camera's target
	*	@author http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/
	*/
	bool setForward(glm::vec3 target);

	/**
	*	Set's the camera position
	*	@param position The camera's position
	*/
	bool setPosition(glm::vec3 position);

	/**
	*	Rotates the camera on an axis by an angle
	*	@param axis		the axis of rotation
	*	@param angle	the rotation in degrees
	*/
	void rotateCamera(glm::vec3 axis, GLfloat angle);

	/**
	*	Resets camera rotation back to default of -z forward, +y up and no roll.
	*/
	void resetRotation();
	
	/**
	*	Gets the view matrix
	*	@return the view matrix
	*/
	glm::mat4 getViewMatrix();
	
	/**
	*	Gets the projection matrix
	*	@return the projection matrix
	*/
	glm::mat4 getProjectionMatrix();

	/**
	*	Gets the Normal matrix based on a modelMatrix
	*	@return the normal matrix, transpose inverse of the modelView matrix(casted to a 3x3 matrix to remove translation)
	*/
	glm::mat3 getNormalMatrix(glm::mat4 modelMatrix);

	/**
	*	Gets the rotation only view matrix
	*	@return the rotation only view matrix
	*/
	glm::mat3 getRotationViewMatrix();

	/**
	*	Get the field of view
	*	@return the field of view
	*/
	GLfloat getFov();
	
	/**
	*	Get the camera plane width
	*	@return the camera plane width
	*/
	GLfloat getWidth();

	/**
	*	Get the camera plane height
	*	@return the camera plane height
	*/
	GLfloat getHeight();

	/**
	*	Get the near clip
	*	@return the near clip
	*/
	GLfloat getNear();

	/**
	*	Get the far clip
	*	@return the far clip
	*/
	GLfloat getFar();

	/**
	*	Get the camera aspect ratio
	*	@return the camera aspect ratio
	*/
	GLfloat getAspectRatio();

	/**
	*	Get the camera position
	*	@return the camera position
	*/
	glm::vec3 getPosition();

	/**
	*	Get the camera up vector
	*	@return the camera up vector
	*/
	glm::vec3 getUp();

	/**
	*	Get the camera forward vector
	*	@return the camera forward vector
	*/
	glm::vec3 getForward();

	/**
	*	Get the camera right vector, cross product of forward and up.
	*	@return the camera right vector
	*/
	glm::vec3 getRight();
	
private:

	/**
	*	Updates the view matrix
	*/
	void updateViewMatrix();

	/**
	*	Calculate a quaternion which will rotate one vector to another.
	*	@author http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/
	*	@param	start the start vector
	*	@param	dest the destination vector
	*	@return a quaternion which rotates start to dest
	*/
	glm::fquat RotationBetweenVectors(glm::vec3 start, glm::vec3 dest);

	GLfloat m_fov;	/**< The field of view  */
	GLfloat m_width; /**<  Camera plane width, used to calculate aspect ratio */
	GLfloat m_height; /**< Camera plane height, used to calculate aspect ratio */
	GLfloat m_aspect; /**< Camera aspect ratio */
	GLfloat m_near; /**< Camera Near clip */
	GLfloat m_far; /**< Camera far clip */
	glm::mat4 m_projectionMatrix; /**< The Projection Matrix */
	glm::vec3 m_position; /**< Camera position */
	glm::vec3 m_forward; /**< Camera forward vector (unit length) */
	glm::vec3 m_up; /**< Camera up vector (unit length) */
	glm::mat4 m_viewMatrix; /**< The View matrix */
	glm::fquat m_orientation; /**< Camera orientation quaternion*/	
};

