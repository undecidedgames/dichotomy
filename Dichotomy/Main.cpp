#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "Application/Application.h"

int main(int argc, char *argv[]) {

	Application Dichotomy;

	Dichotomy.init();

	while (Dichotomy.gameLoop())
		continue;

	return 0;
}