#include "CollisionDetector.h"

CollisionDetector::CollisionDetector()
{
	m_distance = 0.0f;
	m_deltaX = 0.0f;
	m_deltaZ = 0.0f;
	m_radius = 0.0f;
}


CollisionDetector::~CollisionDetector()
{
}

// Circle vs Circle
bool CollisionDetector::detectCirclevsCircle( std::shared_ptr<AbstractRenderable> object1, std::shared_ptr<AbstractRenderable> object2)
{
	// detection for two moving objects (players / npcs)
	float distanceSqr = glm::length(object1->getPosition() - object2->getPosition());
	distanceSqr = distanceSqr * distanceSqr;

	float radiusSqr = object1->getHalfBounds().x + object2->getHalfBounds().x;
	radiusSqr = radiusSqr * radiusSqr;

	if(distanceSqr <= radiusSqr)
		return true;
	else
		return false;	
}

// AABB vs AABB
bool CollisionDetector::detectAABBvsAABB( std::shared_ptr<AbstractRenderable> object1, std::shared_ptr<AbstractRenderable> object2)
{
	float circleDistanceX = std::abs(object1->getPosition().x - object2->getPosition().x);
	float circleDistanceZ = std::abs(object1->getPosition().z - object2->getPosition().z);

	if(circleDistanceX > (object2->getHalfBounds().x + object1->getHalfBounds().x)) return false; // too far to be colliding on x axis
	if(circleDistanceZ > (object2->getHalfBounds().z + object1->getHalfBounds().z)) return false; // too far to be colliding on z axis

	if(circleDistanceX <= object2->getHalfBounds().x) return true; // close enough on x axis to be colliding no matter what
	if(circleDistanceZ <= object2->getHalfBounds().z) return true; // close enough on z axis to be colliding no matter what

	float cornerDistanceSqr =	(circleDistanceX - object2->getHalfBounds().x)*(circleDistanceX - object2->getHalfBounds().x) +
								(circleDistanceZ - object2->getHalfBounds().z)*(circleDistanceZ - object2->getHalfBounds().z);

	return (cornerDistanceSqr <= (object1->getHalfBounds().x * object1->getHalfBounds().x));
}

// Player against NPC or other Player
bool CollisionDetector::detectWeaponStrike(std::shared_ptr<AbstractActor> object1, std::shared_ptr<AbstractRenderable> object2)
{
	glm::vec3 distance = object2->getPosition() - object1->getPosition(); // get distance between objects
	
	float length = glm::length(distance);

    if(length > object1->getWeapon().getRange()) return false; // if weapon is not in range of player 2 return false
	
    glm::vec3 normalizedDistance = glm::normalize(distance); // normalise distance

	float dotprod = glm::dot(object1->getForward(), normalizedDistance); // dot product of forward and distance vector

	if (dotprod < object1->getWeapon().getArcLimit()) return false;

	return true;
}
