#include"CollisionManager.h"


// reference : http://www.gamasutra.com/view/feature/131424/pool_hall_lessons_fast_accurate_.php?page=2
CollisionManager::CollisionManager()
{
	m_detector = CollisionDetector();
	m_resolver = CollisionResolver();

	m_dummyTile = std::shared_ptr<Tile>(new Tile());
	m_buttonDown[0] = false;
	m_buttonDown[1] = false;
}

CollisionManager::~CollisionManager()
{
}


// actor vs actor detection
bool CollisionManager::handleCollision(std::shared_ptr<AbstractActor> object1, std::shared_ptr<AbstractActor> object2)
{
	if(object1->getPhase() == object2->getPhase() || object1->getPhase() == 2 || object2->getPhase() == 2)
	{
		// resolve 1 moveable, 1 immovable object
		if (object2->getMass() >= 50000) // if object 2 is an immovable prop (stationary AABB)
		{
			if(m_detector.detectCirclevsCircle(object1, object2))
				return m_resolver.resolveStationary(object1, object2); // object 1 is always the dynamic object in this case
		}
		// resolve 2 moveable objects
		if(object1->getMass() < 50000 && object2->getMass() < 50000)
		{
			if(m_detector.detectCirclevsCircle(object1, object2))
				return m_resolver.resolveDynamic(object1, object2); 
		}
	}
	return false;
}

// npc against dynamic prop detection
bool CollisionManager::handleCollision(std::shared_ptr<NPCActor> object1, std::shared_ptr<DynamicProp> object2,  std::shared_ptr<AudioManager> audioManager)
{
	if(object1->getPhase() == object2->getPhase() || object1->getPhase() == 2 || object2->getPhase() == 2)
	{
		if(m_detector.detectAABBvsAABB(object1, object2))
		{
			object2->onCollision(object1, audioManager); // on collide do this
	
			if(object2->getSolid()) // if solid object resolve
			{
				return m_resolver.resolveAABBvsAABB(object1, object2); 
			}
		}
	}
	return false;
}

// actor against dynamic prop
bool CollisionManager::handleCollision(std::shared_ptr<PlayerActor> object1, std::shared_ptr<DynamicProp> object2,  std::shared_ptr<AudioManager> audioManager, std::shared_ptr<ServiceManager> serviceManager)
{
	std::shared_ptr<Input> inputService = serviceManager->getInput();

	bool playerID;
	//if in correct phase
	if(object1->getPhase() == object2->getPhase() || object1->getPhase() == 2 || object2->getPhase() == 2)
	{
		
		// if detects collision
		if(m_detector.detectAABBvsAABB(object1, object2))
		{
			//if player 1 set id to 0 (for controller)
			if(object1->isPlayer1()==true)
			{
				playerID = 0;
		
			}
			else
			{
				//if player 2 set id to 1 (for controller)
				playerID = 1;
			}

			// if solid resolve
			if(object2->getSolid()) 
			{
				m_resolver.resolveAABBvsAABB(object1, object2); 

				// only call on collide if solid and button pressed
				if(inputService->onControllerButtonDown(playerID, A) && m_buttonDown[playerID] == false) 
				{
					object2->onCollision(object1, audioManager); // on collide do this
					setButtonPress(true, playerID);
				}
			
				
			}
			else
			{
				object2->onCollision(object1, audioManager); // on collide do this
			}
		}
	}
	return false;
}


//npc vs player attack
bool CollisionManager::handleWeaponStrike(std::shared_ptr<AbstractActor> object1, std::shared_ptr<AbstractActor> object2, std::shared_ptr<AudioManager> audioManager)
{
	if(object1->getPhase() == object2->getPhase() || object1->getPhase() == 2 || object2->getPhase() == 2)
	{
	
		if(m_detector.detectWeaponStrike(object1,object2)) // detect strike arc
		{
			m_resolver.resolveStrike(object1, object2); // resolve any pushing or changes
			object2->onHit(object1->getWeapon().getDamage(), audioManager); // on hit take off 15 health and play groan
			return true;
		}
	}
	return false;
}

bool CollisionManager::handleCollision(std::shared_ptr<AbstractActor> object1, const glm::vec2& position)
{
	// update dummy tile to use new position
	m_dummyTile->setPosition(glm::vec3(position.x,0.0f,position.y));
	
	// if detected resolve
	if(m_detector.detectAABBvsAABB(object1, m_dummyTile))
	{
		return m_resolver.resolveAABBvsAABB(object1, m_dummyTile); 
	}
	return false;
}


void CollisionManager::setButtonPress(bool isDown,int playerID)
{
	m_buttonDown[playerID] = isDown;
}