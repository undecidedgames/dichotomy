#pragma once

#include "../Renderables/AbstractRenderable.h"
#include "../Renderables/Props/Prop.h"
#include "../Renderables/Props/Tile.h"
#include "../Renderables/Actors/PlayerActor.h"
#include "../Renderables//Actors/NPCActor.h"

class CollisionDetector
{

public:
	CollisionDetector();
	~CollisionDetector();

public:
	bool detectCirclevsCircle( std::shared_ptr<AbstractRenderable> object1, std::shared_ptr<AbstractRenderable> object2); // moving object against another moving object
	bool detectAABBvsAABB( std::shared_ptr<AbstractRenderable> object1, std::shared_ptr<AbstractRenderable> object2); // collision check for objects that change on trigger event
	bool detectWeaponStrike(std::shared_ptr<AbstractActor> object1, std::shared_ptr<AbstractRenderable> object2); // arc detect weapon strike

private:
	double m_distance;
	double m_deltaX;
	double m_deltaZ;
	double m_radius;
};