#include "CollisionResolver.h"

CollisionResolver::CollisionResolver()
{
}

CollisionResolver::~CollisionResolver()
{
}


// actor vs actor resolver
bool CollisionResolver::resolveDynamic(std::shared_ptr<AbstractRenderable> object1, std::shared_ptr<AbstractRenderable> object2)
{
	//get distance between the two objects
	glm::vec3 collisionVector = object1->getPosition() - object2->getPosition();
	glm::vec3 normalizedCollisionVector = glm::normalize(collisionVector);

	//get the length of the velocity vectors that are in the direction of the collison normal
	float a1 = glm::dot(object1->getVelocity(), normalizedCollisionVector);
	float a2 = glm::dot(object2->getVelocity(), normalizedCollisionVector);

	//momentum = 2(velocity1 - velocity2)/mass1 + mass 2
	float momentum = (2*(a1 - a2))/(object1->getMass() + object2->getMass());

	//calculate new velocity
	glm::vec3 newVelocity1 = object1->getVelocity() - (momentum * object2->getMass() * normalizedCollisionVector);
	
	glm::vec3 newVelocity2 = object2->getVelocity() + (momentum * object1->getMass() * normalizedCollisionVector);

	float penetration = (object1->getHalfBounds().x + object2->getHalfBounds().x) - glm::length(collisionVector);

	// remove penetration by moving objects apart along the collision vector with the penetration split based on mass ratio
	object1->setPosition( object1->getPosition() + (penetration * normalizedCollisionVector * (object1->getMass()  / (object2->getMass()  +  object1->getMass()))));
	object2->setPosition( object2->getPosition() - (penetration * normalizedCollisionVector * (object2->getMass()  / (object2->getMass()  +  object1->getMass()))));

	object1->setVelocity(newVelocity1);
	return true;
}


// moving actor vs staionary actor that should not move resolver
bool CollisionResolver::resolveStationary(std::shared_ptr<AbstractRenderable> object1, std::shared_ptr<AbstractRenderable> object2)
{
	// for a collision with an actor that we may not want to be able to be moved

	glm::vec3 collisionVector = object1->getPosition() - object2->getPosition();
	glm::vec3 normalizedCollisionVector = glm::normalize(collisionVector);

	float penetration = (object1->getHalfBounds().x + object2->getHalfBounds().x) - glm::length(collisionVector);
	object1->setPosition( object1->getPosition() + (penetration * normalizedCollisionVector));

	return true;
}

// moving actor vs node ( B00218562's AABB vs AABB)
bool CollisionResolver::resolveAABBvsAABB(std::shared_ptr<AbstractRenderable> object1, std::shared_ptr<AbstractRenderable> object2)
{
	// distance between center positions
	glm::vec3 distance = object2->getPosition() - object1->getPosition();

	// minimum distance for there to be a collision
	float minimumX = object1->getHalfBounds().x + object2->getHalfBounds().x;
	float minimumZ = object1->getHalfBounds().z + object2->getHalfBounds().z;

	// how much it intersects in each axis
	float intersectionX = minimumX - abs(distance.x) + 0.01f;
	float intersectionZ =  minimumZ - abs(distance.z) + 0.01f;


	// check which intersection is smallest, resolve penetration on that axis 
	if (intersectionX < intersectionZ)
	{
		if(distance.x < 0)
		{
			object1->setPosition( glm::vec3(object1->getPosition().x + intersectionX, object1->getPosition().y, object1->getPosition().z));
		}
		else
		{
			object1->setPosition( glm::vec3(object1->getPosition().x - intersectionX, object1->getPosition().y, object1->getPosition().z));
		}

	}
	else
	{
		if(distance.z < 0)
		{
			object1->setPosition( glm::vec3(object1->getPosition().x, object1->getPosition().y, object1->getPosition().z + intersectionZ));
		}
		else
		{
			object1->setPosition( glm::vec3(object1->getPosition().x , object1->getPosition().y, object1->getPosition().z - intersectionZ));
		}
	}
	return true;
}

// attack resolver
bool CollisionResolver::resolveStrike(std::shared_ptr<AbstractRenderable> object1, std::shared_ptr<AbstractRenderable> object2)
{	
	// resolve push back
	// for a weapon arc hitting a player
	glm::vec3 collisionVector = object2->getPosition() - object1->getPosition();
	glm::vec3 normalizedCollisionVector = glm::normalize(collisionVector);

	glm::vec3 penetration = normalizedCollisionVector;
	object2->setPosition( object2->getPosition() + ( penetration*glm::vec3(0.25,0.25,0.25)) );
	
	return true;
}
