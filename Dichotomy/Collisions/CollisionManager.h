#pragma once

#include <memory>
#include <sstream> 

#include "CollisionDetector.h"
#include "CollisionResolver.h"
#include "../Renderables/AbstractRenderable.h"
#include "../Renderables/Actors/AbstractActor.h"
#include "../Renderables/Actors/NPCActor.h"
#include "../Renderables/Actors/PlayerActor.h"
#include "../Renderables/Props/Prop.h"
#include "../Renderables/Props/Tile.h"
#include "../Renderables/Props/DynamicProp.h"
#include "../Service/ServiceManager.h"
#include "../Input/Input.h"


class CollisionManager
{
public:
	CollisionManager();
	~CollisionManager();

public:
	bool handleCollision(std::shared_ptr<AbstractActor> object1, std::shared_ptr<AbstractActor> object2);
	bool handleCollision(std::shared_ptr<NPCActor> object1, std::shared_ptr<DynamicProp> object2,  std::shared_ptr<AudioManager> audioManager);
	bool handleCollision(std::shared_ptr<PlayerActor> object1, std::shared_ptr<DynamicProp> object2,  std::shared_ptr<AudioManager> audioManager, std::shared_ptr<ServiceManager> serviceManager);
	bool handleWeaponStrike(std::shared_ptr<AbstractActor> object1, std::shared_ptr<AbstractActor> object2, std::shared_ptr<AudioManager> audioManager);
	bool handleCollision(std::shared_ptr<AbstractActor> object1, const glm::vec2& position);
	void setButtonPress(bool isDown, int playerID);

private:
	CollisionDetector m_detector;
	CollisionResolver m_resolver;
	std::shared_ptr<AbstractRenderable> m_dummyTile;
	bool m_buttonDown[2];
};