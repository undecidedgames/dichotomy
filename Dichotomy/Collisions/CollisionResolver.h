#pragma once

#include "../Renderables/AbstractRenderable.h"
#include "../Renderables/Props/Prop.h"
#include "../Renderables/Props/Tile.h"
#include "../Renderables/Props/DynamicProp.h"

class CollisionResolver
{
public:
	CollisionResolver();
	~CollisionResolver();

public:
	bool resolveDynamic(std::shared_ptr<AbstractRenderable> object1, std::shared_ptr<AbstractRenderable> object2); // dynamic actor collisions
	bool resolveStationary(std::shared_ptr<AbstractRenderable> object1, std::shared_ptr<AbstractRenderable> object2); // actor collision when you don't want 1 actor to move
	bool resolveAABBvsAABB(std::shared_ptr<AbstractRenderable> object1, std::shared_ptr<AbstractRenderable> object2); // collision with dummyTile
	bool resolveStrike(std::shared_ptr<AbstractRenderable> object1, std::shared_ptr<AbstractRenderable> object2); // weapon resolve
};