#include "Controller.h"


#pragma region Class Constructor & Destructor

Controller::Controller()
{
}


Controller::Controller(int controllerID)
{
	m_controllerID = controllerID;
	m_controllerStatus = false;

}


Controller::~Controller(void)
{
	//closeController();

	m_controller = nullptr;
	m_haptic = nullptr;
}

#pragma endregion


#pragma region Manage Controller Device

bool Controller::openController()
{
	bool status = false;

	// Check if index is a supported device
	//if (SDL_IsGameController(m_controllerID))
	//{
		// Test to open controller, if true open it's haptic if supported
		if(m_controller = SDL_GameControllerOpen(m_controllerID)) 
		{
			// Print opened controller information
			std::cout << "Controller opened on index: " << m_controllerID << ". Device name: " << SDL_GameControllerName(m_controller) << std::endl << std::endl;;
			status = true;
			m_controllerStatus = true;

			//openHaptic();
		}
		else 
		{
			// Print information if controller failed to open
			std::cout << "Controller on index: " << m_controllerID << " failed to open. Device name:  " << SDL_GameControllerName(m_controller) << std::endl << std::endl;
			status = false;
		}
//	}
	/*else 
	{
		std::cout << "Device unsupported on index: " << m_controllerID << " failed to open." << std::endl << std::endl;
		status = false;
	}*/

	return status;
}


bool Controller::closeController()
{
	bool status = false;

	// Check if controller is connected and opened
	if(SDL_GameControllerGetAttached(m_controller)) 
	{
		// Close haptic and then controller
		closeHaptic();
		SDL_GameControllerClose(m_controller);
		
		status = true;
		m_controllerStatus = false;

		// Print controller information
		std::cout << "Controller closed on index: " << m_controllerID << ". Device name: " << SDL_GameControllerName(m_controller) << std::endl << std::endl;
	}
	else 
	{
		// If controller not found, print information about controller index
		std::cout << "Close controller failed. Device not found on index: " << m_controllerID << ". Device name: " << SDL_GameControllerName(m_controller) << std::endl << std::endl;

		//delete m_controller;
		//delete m_haptic;

		//m_controller = nullptr;
		//m_haptic = nullptr;

		status = false;
	}

	return status;
}


bool Controller::getControllerStatus()
{
	if (!SDL_GameControllerGetAttached(m_controller))
	{
		m_controllerStatus = false;
	} else m_controllerStatus = true;

	return m_controllerStatus;
}


int Controller::detectHaptic()
{
	int numHaptics = SDL_NumHaptics();

	std::cout << numHaptics << " Haptic device(s) detected." << std::endl;

	// Print number of haptics and their type if any are detected
	if (numHaptics > 0)
		for (int i = 0; i < numHaptics; ++i)
			std::cout << "Haptic detected on device on index: " << i << ". Device name: " << SDL_HapticName(i) << std::endl;

	return numHaptics;
}


bool Controller::openHaptic()
{
	// Exit if there are no controller haptics detected
	if(detectHaptic() < 1) return false;

	bool status = false;

	// Create temp joystick pointer
	SDL_Joystick* tempJoystick = nullptr;

	// Exit if unable to access joystick device
	if (!SDL_JoystickOpen(m_controllerID))
	{
		std::cout << "Unable to access device on index: " << m_controllerID << ". Device name: " << SDL_JoystickNameForIndex(m_controllerID) << std::endl << std::endl;
		return false;
	} else tempJoystick = SDL_JoystickOpen(m_controllerID);
	
	// Check if index is a supported device
	if (SDL_JoystickIsHaptic(tempJoystick))
	{
		// Point temp pointer to device index and open/init haptic
		if(m_haptic = SDL_HapticOpen(m_controllerID)) 
		{
			// Print opened haptic information
			std::cout << "Haptic opened on device on index: " << m_controllerID << std::endl << "Haptic: " 
			<< SDL_HapticName(m_controllerID) << " on device: " << SDL_GameControllerNameForIndex(m_controllerID) << std::endl << std::endl;
			status = true;

			// Init rumble on haptic
			if(SDL_HapticRumbleSupported(m_haptic)) 
			{
				SDL_HapticRumbleInit(m_haptic);
				std::cout << "Rumble initialized for haptic on device index: " << m_controllerID << std::endl << "Haptic: " <<
				SDL_HapticName(m_controllerID) << " on device: " << SDL_GameControllerNameForIndex(m_controllerID) << std::endl << std::endl;
			}
			// Rumble not supported
			else 
			{
				std::cout << "Rumble not supported for haptic on device index: " << m_controllerID << std::endl << "Haptic: " <<
				SDL_HapticName(m_controllerID) << " on device: " << SDL_GameControllerNameForIndex(m_controllerID) << std::endl << std::endl;
			}
		}
		// Couldn't open haptic
		else 
		{
			std::cout << "Haptic for index on index: " << m_controllerID << " failed to open. Device name:  " << SDL_JoystickNameForIndex(m_controllerID) << std::endl << std::endl;
			status = false;
		}
	}
	// Device ain't got no haptic feedback
	else 
	{
		std::cout << "Haptic not supported for device on index: " << m_controllerID << " failed to open. Device name:  " << SDL_JoystickNameForIndex(m_controllerID) << std::endl << std::endl;
		status = false;
	}

	// Close joystick device (only needed for this method)
	SDL_JoystickClose(tempJoystick);
	tempJoystick = nullptr;

	return status;
}


bool Controller::closeHaptic()
{
	bool status = false;

	// Check if index is a supported device
	if (SDL_HapticOpened(m_controllerID) == 1)
	{
		SDL_HapticStopAll(m_haptic);
		SDL_HapticClose(m_haptic);

		// Print haptic information
		std::cout << "Haptic closed for device on index: " << m_controllerID << std::endl << "Haptic: " 
		<< SDL_HapticName(m_controllerID) << " on device: " << SDL_GameControllerNameForIndex(m_controllerID) << std::endl << std::endl;
		status = true;
	}
	else 
	{
		std::cout << "Haptic for device on index: " << m_controllerID << " failed to close. Device name:  " << SDL_JoystickNameForIndex(m_controllerID) << std::endl << std::endl;
		status = false;
	}

	return status;
}

#pragma endregion


#pragma region Controller Data Methods 

int Controller::getControllerAxis(controllerAxes_t axis)
{	
	int axisValue = 0;

	// Check if controller is connected and opened
	if(m_controllerStatus == true) 
	{
		if(SDL_GameControllerGetAxis(m_controller, (SDL_GameControllerAxis) axis)) 
		{
			axisValue = SDL_GameControllerGetAxis(m_controller, (SDL_GameControllerAxis) axis);

			//std::cout << "Axis pushed on controller: " << controllerID << " (" << SDL_GameControllerNameForIndex(controllerID) << "): '"
			//<< SDL_GameControllerGetStringForAxis((SDL_GameControllerAxis) axis) << " ( " 
			//<< SDL_GameControllerGetAxis(tempController, (SDL_GameControllerAxis) axis) << " )'" << std::endl;
		}
	} //else std::cout << "Controller on index: " << m_controllerID << " not found or connected" << std::endl;

	return axisValue;
}


std::pair<float, float> Controller::getControllerAxisUnitVector(controllerStick_t stick)
{
	std::pair<float, float> unitDirection = std::make_pair<float, float>(0, 0);
	glm::vec2 GLMUnitDirection = glm::vec2(0, 0);

	if(m_controllerStatus == true) 
	{
		float xComponent = 0;
		float yComponent = 0;
	
		float x,y;
	
		switch(stick)
		{
			case LEFT: xComponent = getControllerAxis(LEFTSTICKX); yComponent = getControllerAxis(LEFTSTICKY); break;
			case RIGHT: xComponent = getControllerAxis(RIGHTSTICKX); yComponent = getControllerAxis(RIGHTSTICKY); break;
			default: break;
		}
	
		if (xComponent > 0)
		{
			x = (float)(xComponent) / 32767;
		}

		else if (xComponent < 0)
		{
			x = (float)(xComponent) / 32768;
		}
		
		if (yComponent > 0)
		{
			y = (float)(yComponent) / 32767;
		}

		else if (yComponent < 0)
		{
			y = (float)(yComponent) / 32768;
		}
		if (yComponent == 0) y = 0;
		if (xComponent == 0) x = 0;

		
		GLMUnitDirection = glm::vec2(x, y);

		if (glm::length(GLMUnitDirection) < 0.25f) 
		{
			GLMUnitDirection = glm::vec2(0.0f);
		}

		unitDirection.first = GLMUnitDirection.x; unitDirection.second = GLMUnitDirection.y;
	}
	return unitDirection;
}


bool Controller::getControllerButtonState(controllerButtons_t button)
{
	bool result = false;

	// Check if controller is connected and opened
	if(m_controllerStatus == true) 
	{
		if(SDL_GameControllerGetButton(m_controller, (SDL_GameControllerButton) button)) 
		{
			result = true;	

			//std::cout << "Button pressed on controller: " << m_controllerID << " (" << SDL_GameControllerNameForIndex(m_controllerID) 
			//<< "): " << "'" << SDL_GameControllerGetStringForButton((SDL_GameControllerButton) button) << "'"<< std::endl;
		}
	} //else std::cout << "Controller on index: " << m_controllerID << " not found or connected" << std::endl;

	return result;
}

#pragma endregion

