#pragma once

#include <utility>
#include <iostream>
#include <unordered_map>

#include <SDL.h>

#include "Input.h"
#include "Controller.h"

class SDLInput : public Input
{
public:
	/* Class Constructor & Destructor */
	SDLInput(void);
	virtual ~SDLInput(void);

public:
	/* General Public Methods */
	virtual void update();

public:
	/* Keyboard Methods */
	virtual bool getKeyState(SDL_Scancode scanCode);

public:
	/* Mouse Methods */
	virtual std::pair<int, int> getMousePosition();
	virtual std::pair<int, int> getMouseRelativePosition();
	virtual bool mouseButtonState(unsigned int buttonID);
	virtual bool onMouseButtonDown(unsigned int buttonID);
	virtual int getMouseWheel();

public:
	/* Controller Methods */
	virtual int getControllerAxis(unsigned int controllerID, controllerAxes_t axis);
	virtual std::pair<float, float> getControllerAxisUnitVector(unsigned int controllerID, controllerStick_t stick);
	virtual bool getControllerButtonState(unsigned int controllerID, controllerButtons_t button);
	virtual bool onControllerButtonUp(unsigned int controllerID, controllerButtons_t button);
	virtual bool onControllerButtonDown(unsigned int controllerID, controllerButtons_t button);
	virtual void rumbleController(unsigned int controllerID,  float strength, int time);

private:
	/* Controller Methods */
	int detectControllers();
	bool openController(unsigned int controllerID);
	bool closeController(unsigned int controllerID);
	void pollControllers();

private:
	SDL_Event m_SDLEvent;
	int m_connectedControllers;
	std::unordered_map<int, Controller> m_controllerList;
};

