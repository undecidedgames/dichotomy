#pragma once

#include <utility>

#include "../Service/Service.h"
#include "Controller.h"

class Input : public Service
{
public:
	/* Virtual Destructor */
	virtual ~Input(void) {}

public:
	/* General Public Methods */
	virtual void update() = 0;

public:
	/* Keyboard Methods */
	virtual bool getKeyState(SDL_Scancode scanCode) = 0;

public:
	/* Mouse Methods */
	virtual std::pair<int, int> getMousePosition() = 0;
	virtual std::pair<int, int> getMouseRelativePosition() = 0;
	virtual bool mouseButtonState(unsigned int buttonID) = 0;
	virtual bool onMouseButtonDown(unsigned int buttonID) = 0;
	virtual int getMouseWheel() = 0;

public:
	/* Controller Methods*/
	virtual int getControllerAxis(unsigned int controllerID, controllerAxes_t axis) = 0;
	virtual std::pair<float, float> getControllerAxisUnitVector(unsigned int controllerID, controllerStick_t stick) = 0;
	virtual bool getControllerButtonState(unsigned int controllerID, controllerButtons_t button) = 0;
	virtual bool onControllerButtonUp(unsigned int controllerID, controllerButtons_t button) = 0;
	virtual bool onControllerButtonDown(unsigned int controllerID, controllerButtons_t button) = 0;
	virtual void rumbleController(unsigned int controllerID, float strength, int time) = 0;
};

