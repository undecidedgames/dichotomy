#include "SDLInput.h"


#pragma region Class Constructor & Destructor

SDLInput::SDLInput(void)
{
	// Class constructor, init SDL game controller and haptic modules	

	if (SDL_InitSubSystem(SDL_INIT_GAMECONTROLLER | SDL_INIT_HAPTIC) != 0)
	{
		std::cout << "SDL_InitSubSystem: " << SDL_GetError() << std::endl;
	} else std::cout << "SDLInput constructed. " << std::endl;

	// Call detect controllers to find out how many controlers are currently connected	
	m_connectedControllers = detectControllers();

	// Add a new controller to the controller map and open it for how many controllers have been detected
	for (int index = 0; index < m_connectedControllers; ++index)
	{
		m_controllerList[index] = Controller(index);
		openController(index);
	}
}


SDLInput::~SDLInput(void)
{
	std::cout << "SDLInput destructed." << std::endl;
}

#pragma endregion


#pragma region General Public Methods

void SDLInput::update()
{
	SDL_PollEvent(&m_SDLEvent);
}

#pragma endregion


#pragma region Keyboard Methods

bool SDLInput::getKeyState(SDL_Scancode scanCode)
{
	// Return boolean based on state of SDL_Scancode input parameter

	bool status = false;

	const Uint8* keyboard = SDL_GetKeyboardState( NULL );

	if( keyboard[ scanCode ] ) {
		status = true;
		//std::cout << "Key pressed: " << SDL_GetScancodeName(scanCode) << " (Scancode: " << scanCode << ")" << std::endl; 
	}
	
	return status;
}

#pragma endregion


#pragma region Mouse Methods 

std::pair<int, int> SDLInput::getMousePosition()
{
	// Return a pair containing exact mouse X and Y coordinates
	int x, y;
	SDL_GetMouseState(&x, &y);

	return std::pair<int, int> (x, y);
}


std::pair<int, int> SDLInput::getMouseRelativePosition()
{
	// Return a pair containing relative mouse X and Y coordinates
	int x, y;
	SDL_GetRelativeMouseState(&x, &y);

	return std::pair<int, int> (x, y);
}


bool SDLInput::mouseButtonState(unsigned int buttonID)
{
	// Return state of mouse button specified by input buttonID parameter
	
	if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(buttonID))
		return true;
	else
		return false;
}


bool SDLInput::onMouseButtonDown(unsigned int buttonID)
{
	// Return boolean if mouse button is held down

	bool returnVal = false;
	
	//if (m_SDLEvent.type == SDL_MOUSEBUTTONUP)
	//{
	//	returnVal = true;
	//}

	return returnVal;
}


int SDLInput::getMouseWheel()
{
	// Return value of mouse wheel position

	int wheelValue = 0;

	SDL_Event mouse;
	while (SDL_PollEvent(&mouse)) {
		if(mouse.type == SDL_MOUSEWHEEL) {
			std::cout << "Mouse wheel scrolled: " << mouse.wheel.y << std::endl;
			wheelValue = mouse.wheel.x;
		}
	}

	return wheelValue;
}

#pragma endregion


#pragma region Controller Methods

int SDLInput::getControllerAxis(unsigned int controllerID, controllerAxes_t axis)
{
	// Return value of axis on controller specified by input parameters
	int axisValue = 0;

	if (controllerID >= 0 && controllerID < m_connectedControllers)
		axisValue = m_controllerList[controllerID].getControllerAxis(axis);

	return axisValue;
}


std::pair<float, float> SDLInput::getControllerAxisUnitVector(unsigned int controllerID, controllerStick_t stick)
{
	// Return unit vector based on axis position of specified axis and controllers
	std::pair<float, float> axisValue = std::make_pair<float, float>(0, 0);

	if (controllerID >= 0 && controllerID < m_connectedControllers)
		axisValue = m_controllerList[controllerID].getControllerAxisUnitVector(stick);

	return axisValue;
}


bool SDLInput::getControllerButtonState(unsigned int controllerID, controllerButtons_t button)
{
	// Return boolean based on state for specified button on specified controller
	bool buttonState = false;

	if (controllerID >= 0 && controllerID < m_connectedControllers)
		buttonState = m_controllerList[controllerID].getControllerButtonState(button);

	return buttonState;
}


bool SDLInput::onControllerButtonUp(unsigned int controllerID, controllerButtons_t button)
{
	// Return boolean based if button is 'Up' on specified controller

	bool buttonState = false;

	if (controllerID >= 0 && controllerID < m_connectedControllers)
	{
		if (m_SDLEvent.cbutton.type == SDL_CONTROLLERBUTTONUP)
		{
			if (m_SDLEvent.cbutton.button == button)
			{
				buttonState = true;
			}
		}
	}

	return buttonState;
}


bool SDLInput::onControllerButtonDown(unsigned int controllerID, controllerButtons_t button)
{
	// Return boolean if specified button on specified controller is held down

	bool buttonState = true;

	if (controllerID >= 0 && controllerID < m_connectedControllers)
	{
		if (m_controllerList[controllerID].getControllerButtonState(button) == false)
		{
			buttonState = false;
		} else buttonState = true;
	}

	return buttonState;
}

void SDLInput::rumbleController(unsigned int controllerID, float strength, int time)
{
}


int SDLInput::detectControllers()
{
	// Return number of controllers detected on system
	int connectedControllers = SDL_NumJoysticks();

	//std::cout << std::endl << m_connectedControllers << " controller(s) detected." << std::endl;

	// Print number of controllers and their type if any are detected
	//if (m_connectedControllers > 0)
	//	for (int index = 0; index < m_connectedControllers; index++)
		//	std::cout << "Controller detected on index: " << index << ". Device name: " << SDL_GameControllerNameForIndex(index) << std::endl;

	return connectedControllers;
}	


bool SDLInput::openController(unsigned int controllerID)
{
	// Open controller specified by input parameter controllerID
	return m_controllerList[controllerID].openController();
}


bool SDLInput::closeController(unsigned int controllerID)
{
	// Close controller specified by input parameter controllerID
	return m_controllerList[controllerID].closeController();
}


void SDLInput::pollControllers()
{
	// Poll to detect when controllers have been removed or connect and deal with event appropriately

	// Test if controllers are removed, try to close them
	if (m_SDLEvent.cdevice.type == SDL_CONTROLLERDEVICEREMOVED) 
	{	
		for(std::unordered_map<int, Controller>::iterator iterator = m_controllerList.begin(); iterator != m_controllerList.end(); ++iterator)
		{
			if (/*!iterator->second &&*/ !iterator->second.getControllerStatus())
			{
				std::cout << "Controller " << iterator->second.getControllerID() << " disconneted." << std::endl;
				iterator->second.closeController();
			}
		}
	}

	// Check if controllers are connected, try to open them
	if (m_SDLEvent.cdevice.type == SDL_CONTROLLERDEVICEADDED)
	{
		for(std::unordered_map<int, Controller>::iterator iterator = m_controllerList.begin(); iterator != m_controllerList.end(); ++iterator)
		{
			if (/*!iterator->second && */!iterator->second.getControllerStatus())
			{
				std::cout << "Controller " << iterator->second.getControllerID() << " connected." << std::endl;
				iterator->second.openController();
			}
		}
	}
}

#pragma endregion