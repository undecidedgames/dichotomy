#pragma once

#include <iostream>
#include <utility>

#include <SDL.h>
#include <glm\glm.hpp>

/* Enumerator for controller buttons */
enum controllerButtons_t {
	A = SDL_CONTROLLER_BUTTON_A,
	B = SDL_CONTROLLER_BUTTON_B,
	X = SDL_CONTROLLER_BUTTON_X,
	Y = SDL_CONTROLLER_BUTTON_Y,
	LEFTSHOULDER = SDL_CONTROLLER_BUTTON_LEFTSHOULDER,
	RIGHTSHOULDER = SDL_CONTROLLER_BUTTON_RIGHTSHOULDER,
	LEFTSTICK = SDL_CONTROLLER_BUTTON_LEFTSTICK,
	RIGHTSTICK = SDL_CONTROLLER_BUTTON_RIGHTSTICK,
	DPADLEFT = SDL_CONTROLLER_BUTTON_DPAD_LEFT,
	DPADRIGHT = SDL_CONTROLLER_BUTTON_DPAD_RIGHT,
	DPADUP = SDL_CONTROLLER_BUTTON_DPAD_UP,
	DPADDOWN = SDL_CONTROLLER_BUTTON_DPAD_DOWN,
	START = SDL_CONTROLLER_BUTTON_START,
	BACK = SDL_CONTROLLER_BUTTON_BACK,
	GUIDE = SDL_CONTROLLER_BUTTON_GUIDE,
};

/* Enumerator for controller axes */
enum controllerAxes_t {
	LEFTSTICKX = SDL_CONTROLLER_AXIS_LEFTX,
	LEFTSTICKY = SDL_CONTROLLER_AXIS_LEFTY,
	RIGHTSTICKX = SDL_CONTROLLER_AXIS_RIGHTX,
	RIGHTSTICKY = SDL_CONTROLLER_AXIS_RIGHTY,
	LEFTTRIGGER = SDL_CONTROLLER_AXIS_TRIGGERLEFT,
	RIGHTTRIGGER = SDL_CONTROLLER_AXIS_TRIGGERRIGHT,
};

/* Enumerator for controller sticks */
enum controllerStick_t {
	LEFT = false,
	RIGHT = true,
};

class Controller
{
public:
	/* Class Constructor & Destructor */
	Controller();
	Controller(int controllerID);
	~Controller(void);

public:
	/* Manage Controller Device */
	bool openController();
	bool closeController();
	bool getControllerStatus();
	int getControllerID() { return m_controllerID; }

	/* Controller data methods */
	int getControllerAxis(controllerAxes_t axis);
	std::pair<float, float> getControllerAxisUnitVector(controllerStick_t stick);
	bool getControllerButtonState(controllerButtons_t button);
	void rumbleController(float strength, int time);

private:
	int m_controllerID;
	SDL_GameController* m_controller;
	SDL_Haptic* m_haptic;

	bool m_controllerStatus;

	int detectHaptic();
	bool openHaptic();
	bool closeHaptic();
};

