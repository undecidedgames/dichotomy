#pragma once

#include <glm\glm.hpp>
#include <memory>
#include <array>
#include <queue>
#include <SDL.h>

#include "Levels\MapNode.h"

class MapNode;

class Pathfinding
{
public:
	// Default Constructor and Destructor
	Pathfinding();
	~Pathfinding();


public:	
	// Calculates the path form start to target with the lowest cost and returns true if a path is found
	bool findPath(std::shared_ptr<MapNode> startNode, std::shared_ptr<MapNode> targetNode, std::shared_ptr<std::vector<std::vector<std::shared_ptr<MapNode>>>> map);

	// Return the path for the object using pathfinding
	std::vector< glm::vec3 > getPath();


private:
	// Calculates the cost from start by incrementing current cost with this nodes weight	
	void calculateCostFromStart(std::shared_ptr<MapNode> currentNode);

	// Calculates the cost to the target by calculating the distance from current node to target	
	void calculateHeuristicCost(std::shared_ptr<MapNode> currentNode, std::shared_ptr<MapNode> targetNode);

	// Adds cost from start(G) and heuristic cost(H) together to get the total cost(F) -> F = G + H
	void calculateTotalCost(std::shared_ptr<MapNode> currentNode);

	// Checks is the neighbour node valid (ie can it be added to the open list)
	void isValidNeighbour(std::shared_ptr<MapNode> currentNode, std::shared_ptr<MapNode> neighbourNode, std::shared_ptr<MapNode> targetNode, bool isDiagonal);


private:
	// Possible node the path can progress
	std::vector< std::shared_ptr<MapNode> > m_openNodes;	// < operator is overloaded in mapNode therefore no compare function

	// Lowest total cost nodes (the path in incorrect order)
	std::vector< std::shared_ptr<MapNode> > m_closedNodes;

	// The resulting path
	std::vector< glm::vec3 > m_path;			// < operator is overloaded in mapNode therefore no compare function

	Uint16 m_currentCost;	// cost from start tile to current tile
	glm::vec3 m_nextWayPoint;	// The next position an object should move to

	bool m_foundGoal;	// Has a path been found
};

