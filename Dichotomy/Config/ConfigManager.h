#pragma once

#include <string>

#include <boost\property_tree\ptree.hpp>
#include <boost\property_tree\info_parser.hpp>

using boost::property_tree::ptree;

/**
*	A manager class that deals with the boost::propery_tree::ptree that is loaded in from the config.info file.
*	@author B00216209
*/
class ConfigManager
{
public:
	
	/**
	*	Constructor.
	*	Cout's a confirmation of launch message and calls loadInfoFile()
	*	@see loadInfoFile()
	*/
	ConfigManager();

	/**
	*	Destructor.
	*	Cout's a confirmation of close message and calls saveInfoFile()
	*	@see saveInfoFile()
	*/
	~ConfigManager();

	ptree m_propertyTree; /**< The propertry tree object which holds all configuration data */

private:
	
	/**
	*	Loads the config.info file into the m_propertyTree variable.
	*	@param filename A path to the config.info file.
	*/
	void loadInfoFile(const std::string& filename);

	/**
	*	Saves the m_propertyTree variable into the config.info file.
	*	@param filename A path to the config.info file.
	*/
	void saveInfoFile(const std::string& filename);
};
