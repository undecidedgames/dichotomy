#include "ConfigManager.h"


ConfigManager::ConfigManager()
{
	std::cout << "ConfigManager constructor called" << std::endl;
	loadInfoFile("config/config.info");
}


ConfigManager::~ConfigManager()
{
	std::cout << "ConfigManager destructor called" << std::endl;
	saveInfoFile("config/config.info");
}


void ConfigManager::loadInfoFile(const std::string &filename)
{
	boost::property_tree::read_info(filename,m_propertyTree);
}


void ConfigManager::saveInfoFile(const std::string &filename)
{
	boost::property_tree::write_info(filename,m_propertyTree);
}
