#include "RenderManager.h"


RenderManager::RenderManager(string renderer, std::shared_ptr<ConfigManager> configManager, std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<CameraManager> cameraManager)
{
	std::cout << "RenderManager constructor called." << std::endl;

	m_configManager = configManager;
	m_shaderManager = shaderManager;
	m_cameraManager = cameraManager;
	m_currentRenderer = renderer;
	m_exposureAdjustSpeed = 0.1f;
	m_exposure = 1.0f;
	m_targetExposure= 1.0f;
	m_currentPhase = HUMAN_PHASE;
	m_superSampling = 1; // 1 is default sized, increase to a power of 2 for SSAA (beware GPU memory limits)

	// Generate texture ID's once on startup.
	m_diffuseRT = 0;
	m_normalRT = 0;
	m_positionRT = 0;
	m_specularRT = 0;
	m_phongRT = 0;
	m_gBuffer = 0;
	m_phongBuffer = 0;
	m_depthRT = 0;
	GLuint* textures[] = {&m_diffuseRT, &m_normalRT, &m_positionRT, &m_specularRT, &m_phongRT};
	GLuint* framebuffers[] = {&m_gBuffer, &m_phongBuffer};
	glGenTextures(5, *textures);
	glGenRenderbuffers(1, &m_depthRT);
	glGenFramebuffers(2, *framebuffers);
	
	// Initialise framebuffers
	initGeometryBuffer();
	initPhongBuffer();
	//initShadowMapBuffer(); currently NYI so commented out to speed up load times and reduce memory usage
	
	// Initialise the quad VAO used for drawing UI elements and offscreen textures
	initScreenQuad();
	
	// Initialise Skybox, this has 3 cubemaps: normal skybox, an irradiance environment map and a radiance environmentmap.
	m_skybox = std::shared_ptr<Skybox>(new Skybox("skyCube.obj","river"));
	
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBlendEquation(GL_FUNC_ADD);
}


RenderManager::~RenderManager(void)
{
	// Deleteing openGL ID's created by glGen*
	GLuint textures[5] = {m_diffuseRT, m_normalRT, m_positionRT, m_specularRT, m_phongRT};
	GLuint depthTextures[2] = {m_depthRT, m_phongDepthRT};
	GLuint framebuffers[2] = {m_gBuffer, m_phongBuffer};
	
	glDeleteTextures(5,textures);
	glDeleteRenderbuffers(2, depthTextures);
	glDeleteFramebuffers(2, framebuffers);
}


void RenderManager::calculateLuminance()
{
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D,m_phongRT);
	GLdouble summedLuminance = 0.0f;
	
	size_t arraySize = m_configManager->m_propertyTree.get<int>("window.width") *  
					m_configManager->m_propertyTree.get<int>("window.height") * 4 * m_superSampling;

	GLfloat* imageStore = new GLfloat[arraySize];
	
	glGetTexImage(GL_TEXTURE_2D,0, GL_RGBA,GL_FLOAT,imageStore);
	
	for(size_t i = 0; i < arraySize*0.25; ++i)
	{
		summedLuminance += imageStore[3 + i*4];
	}

	if(_isnan(summedLuminance))
	{
		summedLuminance = arraySize/4 * 0.5f;
	}
	
	GLdouble avarageLuminance = (summedLuminance) / (arraySize/4);
	
	avarageLuminance = glm::clamp(avarageLuminance,0.3,0.7);
	
	m_targetExposure = (GLfloat)(0.5 / avarageLuminance);

	GLdouble difference = m_targetExposure - m_exposure;
	difference = 0.01 * difference;
	difference = glm::clamp(difference, -0.05, 0.05);
	
	m_exposure += (GLfloat)difference;
	
	delete[] imageStore;
}


void RenderManager::renderSkyBox(string shader)
{
	GLuint program = m_shaderManager->getShader(shader);
	GLuint vao = m_skybox->getVAO();
	GLuint indiceCount = m_skybox->getIndexCount();
	GLuint textureID = m_skybox->getTexture();
	
	glUseProgram(program);

	glUniformMatrix3fv(glGetUniformLocation(program,"viewMatrix"), 1, GL_FALSE, glm::value_ptr(glm::mat3(m_cameraManager->getViewMatrix())));
	glUniformMatrix4fv(glGetUniformLocation(program,"projectionMatrix"), 1, GL_FALSE, glm::value_ptr(m_cameraManager->getProjectionMatrix()));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_diffuseRT);
	
	glCullFace(GL_FRONT);
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, indiceCount, GL_UNSIGNED_INT,0);
	glCullFace(GL_BACK);
}

void RenderManager::render(std::shared_ptr<AbstractRenderable> renderable, string shader)
{
		GLuint program = m_shaderManager->getShader(shader);
		GLuint vao;
		GLuint indiceCount;
		glm::mat4 modelMatrix = renderable->getModelMatrix();
		GLuint textureID;
		GLuint normalTextureID;
		GLuint specularID;
		GLuint phase = renderable->getPhase();

		switch(phase)
		{
			case 0:
			{
				if(m_currentPhase == HUMAN_PHASE)
				{
					textureID = renderable->getTexture();
					normalTextureID = renderable->getNormalTexture();
					specularID = renderable->getSpecularTexture();
					vao = renderable->getVAO();
					indiceCount = renderable->getIndexCount();
				}
				else
				{
					textureID = renderable->getTexture2();
					normalTextureID = renderable->getNormalTexture2();
					specularID = renderable->getSpecularTexture2();
					vao = renderable->getVAO2();
					indiceCount = renderable->getIndexCount2();
				}
			} break;
			case 1:
			{
				if(m_currentPhase == DEMON_PHASE)
				{
					textureID = renderable->getTexture();
					normalTextureID = renderable->getNormalTexture();
					specularID = renderable->getSpecularTexture();
					vao = renderable->getVAO();
					indiceCount = renderable->getIndexCount();
				}
				else
				{
					textureID = renderable->getTexture2();
					normalTextureID = renderable->getNormalTexture2();
					specularID = renderable->getSpecularTexture2();
					vao = renderable->getVAO2();
					indiceCount = renderable->getIndexCount2();
				}
			}break;
			case 2:
			{
				textureID = renderable->getTexture();
				normalTextureID = renderable->getNormalTexture();
				specularID = renderable->getSpecularTexture();
				vao = renderable->getVAO();
				indiceCount = renderable->getIndexCount();
			}break;
		}
				
		glUseProgram(program);
		
		GLuint viewMatrixLoc = glGetUniformLocation(program,"viewMatrix");
		GLuint projMatrixLoc = glGetUniformLocation(program,"projectionMatrix");
		GLuint modelMatrixLoc = glGetUniformLocation(program,"modelMatrix");
		glUniformMatrix4fv(viewMatrixLoc, 1, GL_FALSE, glm::value_ptr(m_cameraManager->getViewMatrix()));
		glUniformMatrix4fv(projMatrixLoc, 1, GL_FALSE, glm::value_ptr(m_cameraManager->getProjectionMatrix()));
		glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, glm::value_ptr(modelMatrix));

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, textureID);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, normalTextureID);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, specularID);

		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, indiceCount,  GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
}


void RenderManager::render(Weapon& weapon, string shader)
{
	GLuint program = m_shaderManager->getShader(shader);
	GLuint vao;
	GLuint indiceCount;
	glm::mat4 modelMatrix = weapon.getModelMatrix();
	GLuint textureID;
	GLuint normalTextureID;
	GLuint specularID;
	GLuint phase = weapon.getPhase();

	switch(phase)
	{
		case 0:
		{
			if(m_currentPhase == HUMAN_PHASE)
			{
				textureID = weapon.getTexture();
				normalTextureID = weapon.getNormalTexture();
				specularID = weapon.getSpecularTexture();
				vao = weapon.getVAO();
				indiceCount = weapon.getIndexCount();
			}
			else
			{
				textureID = weapon.getTexture2();
				normalTextureID = weapon.getNormalTexture2();
				specularID = weapon.getSpecularTexture2();
				vao = weapon.getVAO2();
				indiceCount = weapon.getIndexCount2();
			}
		} break;
		case 1:
		{
			if(m_currentPhase == DEMON_PHASE)
			{
				textureID = weapon.getTexture();
				normalTextureID = weapon.getNormalTexture();
				specularID = weapon.getSpecularTexture();
				vao = weapon.getVAO();
				indiceCount = weapon.getIndexCount();
			}
			else
			{
				textureID = weapon.getTexture2();
				normalTextureID = weapon.getNormalTexture2();
				specularID = weapon.getSpecularTexture2();
				vao = weapon.getVAO2();
				indiceCount = weapon.getIndexCount2();
			}
		}break;
		case 2:
		{
			textureID = weapon.getTexture();
			normalTextureID = weapon.getNormalTexture();
			specularID = weapon.getSpecularTexture();
			vao = weapon.getVAO();
			indiceCount = weapon.getIndexCount();
		}break;
	}
				
	glUseProgram(program);
		
	GLuint viewMatrixLoc = glGetUniformLocation(program, "viewMatrix");
	GLuint projMatrixLoc = glGetUniformLocation(program, "projectionMatrix");
	GLuint modelMatrixLoc = glGetUniformLocation(program, "modelMatrix");
	glUniformMatrix4fv(viewMatrixLoc, 1, GL_FALSE, glm::value_ptr(m_cameraManager->getViewMatrix()));
	glUniformMatrix4fv(projMatrixLoc, 1, GL_FALSE, glm::value_ptr(m_cameraManager->getProjectionMatrix()));
	glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, glm::value_ptr(modelMatrix));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, normalTextureID);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, specularID);

	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, indiceCount,  GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}


void RenderManager::instancedRender(vector<std::shared_ptr<Tile>> tiles, string shader)
{
	GLuint program = m_shaderManager->getShader(shader);
	for(GLuint i = 0; i < tiles.size(); ++i)
	{
		GLuint textureID;
		GLuint normalTextureID;
		GLuint specularID;
		GLuint vao;
		GLuint indiceCount;
		GLuint TBOID = tiles[i]->m_modelMatrixTBO;
		GLuint tileCount = tiles[i]->m_drawCount;;

		if(m_currentPhase == HUMAN_PHASE)
		{
			textureID = tiles[i]->getTexture();
			normalTextureID = tiles[i]->getNormalTexture();
			specularID = tiles[i]->getSpecularTexture();

			vao = tiles[i]->getVAO();
			indiceCount = tiles[i]->getIndexCount();
		}
		if(m_currentPhase == DEMON_PHASE)
		{
			textureID = tiles[i]->getTexture2();
			normalTextureID = tiles[i]->getNormalTexture2();
			specularID = tiles[i]->getSpecularTexture2();

			vao = tiles[i]->getVAO2();
			indiceCount = tiles[i]->getIndexCount2();
		}

		glUseProgram(program);

		GLuint viewMatrixLoc = glGetUniformLocation(program,"viewMatrix");
		GLuint projMatrixLoc = glGetUniformLocation(program,"projectionMatrix");
		glUniformMatrix4fv(viewMatrixLoc, 1, GL_FALSE, glm::value_ptr(m_cameraManager->getViewMatrix()));
		glUniformMatrix4fv(projMatrixLoc, 1, GL_FALSE, glm::value_ptr(m_cameraManager->getProjectionMatrix()));
				
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, textureID);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, normalTextureID);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, specularID);
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_BUFFER, TBOID);

		glBindVertexArray(vao);
		glDrawElementsInstanced(GL_TRIANGLES, indiceCount,  GL_UNSIGNED_INT, 0, tileCount);
		glBindVertexArray(0);
	}
}


void RenderManager::instancedRender(vector<std::shared_ptr<Prop>> props, string shader)
{
	GLuint program = m_shaderManager->getShader(shader);
	for(GLuint i = 0; i < props.size(); ++i)
	{
		GLuint textureID;
		GLuint normalTextureID;
		GLuint specularID;
		GLuint vao;
		GLuint indiceCount;
		GLuint TBOID = props[i]->m_modelMatrixTBO;
		GLuint propCount = props[i]->m_drawCount;

		if(m_currentPhase == HUMAN_PHASE)
		{
			textureID = props[i]->getTexture();
			normalTextureID = props[i]->getNormalTexture();
			specularID = props[i]->getSpecularTexture();

			vao = props[i]->getVAO();
			indiceCount = props[i]->getIndexCount();
		}
		if(m_currentPhase == DEMON_PHASE)
		{
			textureID = props[i]->getTexture2();
			normalTextureID = props[i]->getNormalTexture2();
			specularID = props[i]->getSpecularTexture2();

			vao = props[i]->getVAO2();
			indiceCount = props[i]->getIndexCount2();
		}

		glUseProgram(program);

		GLuint viewMatrixLoc = glGetUniformLocation(program,"viewMatrix");
		GLuint projMatrixLoc = glGetUniformLocation(program,"projectionMatrix");
		glUniformMatrix4fv(viewMatrixLoc, 1, GL_FALSE, glm::value_ptr(m_cameraManager->getViewMatrix()));
		glUniformMatrix4fv(projMatrixLoc, 1, GL_FALSE, glm::value_ptr(m_cameraManager->getProjectionMatrix()));

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, textureID);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, normalTextureID);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, specularID);
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_BUFFER, TBOID);
		
		glBindVertexArray(vao);
		glDrawElementsInstanced(GL_TRIANGLES, indiceCount,  GL_UNSIGNED_INT, 0, propCount);
		glBindVertexArray(0);
	}
}

bool RenderManager::setRenderTarget(GLuint renderTarget)
{
	GLenum error = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (error == GL_FRAMEBUFFER_COMPLETE)
	{
		glBindFramebuffer(GL_FRAMEBUFFER,renderTarget);
		return true;
	}
	else
	{
		std::cout << "Incomplete or invalid framebuffer" << std::endl;
		return false;
	}
}

void RenderManager::initGeometryBuffer()
{
	glBindTexture(GL_TEXTURE_2D, m_diffuseRT);
	glTexImage2D(	GL_TEXTURE_2D, 0, GL_RGBA8, 
					m_configManager->m_propertyTree.get<int>("window.width")*m_superSampling, 
					m_configManager->m_propertyTree.get<int>("window.height")*m_superSampling, 
					0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	glBindTexture(GL_TEXTURE_2D, m_normalRT);
	glTexImage2D(	GL_TEXTURE_2D, 0, GL_RGBA16F, 
					m_configManager->m_propertyTree.get<int>("window.width")*m_superSampling, 
					m_configManager->m_propertyTree.get<int>("window.height")*m_superSampling,
					0, GL_RGBA, GL_HALF_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	glBindTexture(GL_TEXTURE_2D, m_specularRT);
	glTexImage2D(	GL_TEXTURE_2D, 0, GL_RGBA8, 
					m_configManager->m_propertyTree.get<int>("window.width")*m_superSampling, 
					m_configManager->m_propertyTree.get<int>("window.height")*m_superSampling, 
					0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	glBindTexture(GL_TEXTURE_2D, m_positionRT);
	glTexImage2D(	GL_TEXTURE_2D, 0, GL_RGB16F, 
					m_configManager->m_propertyTree.get<int>("window.width")*m_superSampling, 
					m_configManager->m_propertyTree.get<int>("window.height")*m_superSampling,
					0, GL_RGB, GL_HALF_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	glBindRenderbuffer(GL_RENDERBUFFER,m_depthRT);
	glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT32,
							m_configManager->m_propertyTree.get<int>("window.width")*m_superSampling, 
							m_configManager->m_propertyTree.get<int>("window.height")*m_superSampling);
	
	glBindFramebuffer(GL_FRAMEBUFFER, m_gBuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_diffuseRT, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_normalRT, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D ,m_specularRT, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D ,m_positionRT, 0);
	glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,m_depthRT);
	
	GLenum error = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (error == GL_FRAMEBUFFER_COMPLETE) std::cout << "G-buffer framebuffer complete." << std::endl;
	else 
	{
		std::cout << "G-buffer framebuffer incomplete!" << std::endl;
	
		switch(error)
		{
			case GL_FRAMEBUFFER_UNDEFINED : std::cout << " GL_FRAMEBUFFER_UNDEFINED." << std::endl;
			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT : std::cout  << " GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT." << std::endl;
			case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT : std::cout  << " GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT." << std::endl;
			case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER : std::cout << " GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER." << std::endl;
			case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER : std::cout  << " GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER." << std::endl;
			case GL_FRAMEBUFFER_UNSUPPORTED : std::cout  << " GL_FRAMEBUFFER_UNSUPPORTED." << std::endl;
			case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE : std::cout  << " GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE." << std::endl;
			case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS : std::cout  << " GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS." << std::endl;
			case GL_INVALID_ENUM : std::cout<< " GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS." << std::endl;
			default : std::cout << "Error not recognised: " << error << std::endl;
		}
	}
}

void RenderManager::initPhongBuffer()
{
	glBindTexture(GL_TEXTURE_2D, m_phongRT);
	glTexImage2D(	GL_TEXTURE_2D, 0, GL_RGBA16F, 
					m_configManager->m_propertyTree.get<int>("window.width")*m_superSampling, 
					m_configManager->m_propertyTree.get<int>("window.height")*m_superSampling, 
					0, GL_RGBA, GL_HALF_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindFramebuffer(GL_FRAMEBUFFER, m_phongBuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_phongRT, 0);
	
	GLenum error = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (error != 0) std::cout << "Phong framebuffer complete." << std::endl;
	else std::cout << "Phong framebuffer incomplete!" << std::endl;
}

void RenderManager::initShadowMapBuffer()
{
	glGenTextures(1,&m_shadowMapDepthRT);
	glBindTexture(GL_TEXTURE_2D,m_shadowMapDepthRT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
	glTexImage2D(	GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, 
					512, 
					512, 
					0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

	glGenFramebuffers(1, &m_shadowMapBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, m_shadowMapBuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_shadowMapDepthRT, 0);
	
	GLenum error = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (error != 0) std::cout << "ShadowMap framebuffer complete." << std::endl;
	else std::cout << "ShadowMap framebuffer incomplete!" << std::endl;
}

void RenderManager::renderToBackBuffer(string shader, GLint type, GLfloat exposure)
{
	GLfloat inverseGamma = 1.0f/1.2f;
	GLuint program = m_shaderManager->getShader(shader);
	m_exposure = 1.0f;
	
	glUseProgram(program);
	glUniform1i(glGetUniformLocation(program, "type"),type);
	glUniform1f(glGetUniformLocation(program, "inverseGamma"),inverseGamma);
	glUniform1f(glGetUniformLocation(program, "exposure"),m_exposure);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_diffuseRT);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_normalRT);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_specularRT);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_positionRT);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, m_phongRT);
	
	glBindVertexArray(quad_VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void RenderManager::renderUIElement(std::shared_ptr<UIElement> element, string shader)
{
	GLuint program = m_shaderManager->getShader(shader);
	GLuint texture = element->getTexture();
	glm::mat4 modelMatrix = element->getModelMatrix();

	glUseProgram(program);
	GLuint modelMatrixLoc = glGetUniformLocation(program, "modelMatrix");
	glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, glm::value_ptr(modelMatrix));
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	glBindVertexArray(quad_VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void RenderManager::renderPhongBuffer(string shader, std::shared_ptr<Level> level)
{
	GLuint program = m_shaderManager->getShader(shader);
	GLuint envMap = m_skybox->getTexture();
	GLuint iradMap = m_skybox->getSpecularTexture();
	GLuint lightArraySize = level->m_lightsViewSpace.size();
	
	glUseProgram(program);
	level->setLightArray(program, level->m_lightsViewSpace.data(), level->m_lightsViewSpace.size());
	glUniform1i(glGetUniformLocation(program, "LightArraySize"), level->lightCount);
	glm::vec2 screenSize(	m_configManager->m_propertyTree.get<int>("window.width")*m_superSampling, 
							m_configManager->m_propertyTree.get<int>("window.height")*m_superSampling);
	glUniform2fv(glGetUniformLocation(program, "screenSize"),1,glm::value_ptr(screenSize));
	glUniformMatrix3fv(glGetUniformLocation(program,"inverseViewMatrix"), 1, GL_FALSE, glm::value_ptr(glm::inverse((glm::mat3)m_cameraManager->getViewMatrix())));
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_diffuseRT);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_normalRT);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_specularRT);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_positionRT);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_CUBE_MAP,envMap);
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_CUBE_MAP,iradMap);
		
	glBindVertexArray(quad_VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void RenderManager::setPhase(phase_enum phase)
{
	m_currentPhase = phase;
}

void RenderManager::initScreenQuad()
{
	GLfloat vertices[] = {	-1, -1,
							-1,  1,
							 1,  1,
							 1, -1};

	GLuint indices[] = { 0,3,1, 
						 3,2,1 }; 

	glGenVertexArrays(1, &quad_VAO);
	glBindVertexArray(quad_VAO);

	GLuint VBOlist[2];
	glGenBuffers(2, VBOlist);

	glBindBuffer(GL_ARRAY_BUFFER, VBOlist[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOlist[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void RenderManager::setSuperSampling(GLuint samples)
{
	if(samples != m_superSampling)
	{
		if ( samples == 1 || samples == 2 || samples == 4 || samples == 8)
		{
			m_superSampling = samples;
			initGeometryBuffer();
			initPhongBuffer();
		}
		else
			cout << "Samples given is either negative or not a power of 2, no change to sampling." << endl;
	}

}

GLuint RenderManager::getSuperSampling()
{
	return m_superSampling;
}


void RenderManager::viewFrustumCull(LightProperties* LightArray, GLint size)
{
	// Get up-to-date cullingSphere from camera
	cullingSphere temp = calculateCullingSphere();
	// Setup Variables needed for calculation
	GLfloat lightRadius;
	glm::vec3 lightCenter;
	// for each light in the array
	for(int i=0; i < size; ++i)
	{ 
		// get it's radius(range) and position
		lightRadius = LightArray[i].range;
		lightCenter = LightArray[i].position;

		// calculate the distance between the light and the center point of the culling sphere
		GLfloat distance = glm::length(glm::vec3(temp.center - lightCenter));

		// if the distance squared is greater than the sum of the radius squared, set the light to disabled else enable it.
		if((distance * distance) > (lightRadius + temp.radius) * (lightRadius + temp.radius))
		{
			LightArray[i].isEnabled = false;
		}
		else
		{
			LightArray[i].isEnabled = true;
		}
	}
}

void RenderManager::viewFrustumCull(vector<std::shared_ptr<Tile>> tiles)
{
	// Get up-to-date cullingSphere from camera
	cullingSphere temp = calculateCullingSphere();
	// Setup Variables needed for calculation
	GLfloat tileRadius;
	glm::vec3 tileCenter;
	// for each light in the array
	GLuint count = 0;
	for(std::shared_ptr<Tile> tempTile : tiles)
	{ 
		for(glm::vec3 position : tempTile->m_positions)
		{
			// get it's radius(range) and position
			tileRadius = glm::length(glm::vec3(tempTile->getHalfBounds().x,tempTile->getHalfBounds().y,tempTile->getHalfBounds().z));
			tileCenter = position;

			// calculate the distance between the light and the center point of the culling sphere
			GLfloat distance = glm::length(glm::vec3(temp.center - tileCenter));

			// if the distance squared is greater than the sum of the radius squared, set the light to disabled else enable it.
			if((distance * distance) > (tileRadius + temp.radius) * (tileRadius + temp.radius))
			{
				tempTile->m_culledList[count] = true;
			}
			else
			{
				tempTile->m_culledList[count] = false;
			}
			count++;
		}
	}
}

void RenderManager::viewFrustumCull(vector<std::shared_ptr<Prop>> props)
{
	// Get up-to-date cullingSphere from camera
	cullingSphere temp = calculateCullingSphere();
	// Setup Variables needed for calculation
	GLfloat propRadius;
	glm::vec3 propCenter;
	// for each light in the array
	GLuint count = 0;
	for(std::shared_ptr<Prop> tempProp : props)
	{ 
		for(glm::vec3 position : tempProp->m_positions)
		{
			// get it's radius(range) and position
			propRadius = glm::length(glm::vec3(tempProp->getHalfBounds().x, tempProp->getHalfBounds().y, tempProp->getHalfBounds().z));
			propCenter = position;

			// calculate the distance between the light and the center point of the culling sphere
			GLfloat distance = glm::length(glm::vec3(temp.center - propCenter));

			// if the distance squared is greater than the sum of the radius squared, set the light to disabled else enable it.
			if((distance * distance) > (propRadius + temp.radius) * (propRadius + temp.radius))
			{
				tempProp->m_culledList[count] = true;
			}
			else
			{
				tempProp->m_culledList[count] = false;
			}
			count++;
		}
	}
}

cullingSphere RenderManager::calculateCullingSphere()
{
	// calculate the radius of the frustum sphere
	float fViewLen = m_cameraManager->getFar() - m_cameraManager->getNear();

	// use some trig to find the height of the frustum at the far plane
	float fHeight = fViewLen * tan(glm::radians(m_cameraManager->getFov()) * 0.5f);

	// with an aspect ratio of 1, the width will be the same
	float fWidth = m_cameraManager->getAspectRatio()*fHeight;

	// halfway point between near/far planes starting at the origin and extending along the z axis
	glm::vec3 P = glm::vec3(0.0f, 0.0f, m_cameraManager->getNear() + fViewLen * 0.5f);

	// the calculate far corner of the frustum
	glm::vec3 Q = glm::vec3(fWidth, fHeight, m_cameraManager->getNear() +  fViewLen);

	// the vector between P and Q
	glm::vec3 vDiff = glm::vec3(P - Q);

	cullingSphere temp;

	// the radius becomes the length of this vector
	temp.radius = glm::length(vDiff);

	// calculate the center of the sphere
	temp.center = m_cameraManager->getPosition() + (m_cameraManager->getForward() * (fViewLen * 0.5f + m_cameraManager->getNear()));

	// return cullingSphere
	return temp;
}



