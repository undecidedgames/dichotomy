#pragma once

#include <memory>
#include <string>
#include <vector>
#include <math.h>

#include "../Renderables/AbstractRenderable.h"
#include "../Config/ConfigManager.h"
#include "../Shaders/ShaderManager.h"
#include "../Camera/CameraManager.h"
#include "../Renderables/Props/Tile.h"
#include "../Renderables/Props/Prop.h"
#include "../Levels/Level.h"
#include "../Renderables/Skybox/Skybox.h"
#include "../UI/UIElement.h"

using namespace std;

/**
*	An enum for the various phases
*/
enum phase_enum {HUMAN_PHASE, DEMON_PHASE, DUAL_PHASE};

/**
* A struct for the sphere used in camera based culling
*/
struct cullingSphere {glm::vec3 center; GLfloat radius;};

/**
*	A manager class that deals with configuring the the framebuffers and drawing objects to them.
*	@author B00216209
*/
class RenderManager
{
public:

	/**
	*	Default contructor
	*	@param rederer A string used to identify the current rendering API
	*	@param configManager A shared_ptr to the config manager
	*	@param shaderManager A shared_ptr to the shader manager
	*	@param cameraManager A shared_ptr to the camera manager
	*/
	RenderManager(string renderer, std::shared_ptr<ConfigManager> configManager, std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<CameraManager> cameraManager);
	
	/**
	*	Default destructor
	*/
	~RenderManager();

	/**
	*	Renders an abtract renderable object with the shader provided
	*	@param renderable A shared_ptr to an abstract renderable
	*	@param shader A string used as a lookup into ShaderManager's map to get the shader program ID
	*/
	void render(std::shared_ptr<AbstractRenderable> renderable, string shader);

	/**
	*	Renders a weapon object with the shader provided
	*	@param renderable A reference to a weapon object
	*	@param shader A string used as a lookup into ShaderManager's map to get the shader program ID
	*/
	void render(Weapon& weapon, string shader);

	/**
	*	Renders a UI Element with the shader provided
	*	@param element A shared_ptr to a UI element
	*	@param shader A string used as a lookup into ShaderManager's map to get the shader program ID
	*/
	void renderUIElement(std::shared_ptr<UIElement> element, string shader);

	/**
	*	Renders multile tile object using instanced rendering with the shader provided
	*	@param tiles A vector of shared_ptrs of tile objects
	*	@param shader A string used as a lookup into ShaderManager's map to get the shader program ID
	*/
	void instancedRender(vector<std::shared_ptr<Tile>> tiles, string shader);

	/**
	*	Renders multile tile object using instanced rendering with the shader provided
	*	@param props A vector of shared_ptrs of prop objects
	*	@param shader A string used as a lookup into ShaderManager's map to get the shader program ID
	*/
	void instancedRender(vector<std::shared_ptr<Prop>> props, string shader);

	/**
	*	Renders the current skybox member variables with the shader provided
	*	@param shader A string used as a lookup into ShaderManager's map to get the shader program ID
	*/
	void renderSkyBox(string shader);

	/**
	*	Takes the data from the G-buffer and renders the shaded scene to an offscreen texture with the shader provided 
	*	@param shader A string used as a lookup into ShaderManager's map to get the shader program ID
	*	@param level A shared_ptr to a level object, used to get that level's lightarray
	*/
	void renderPhongBuffer(string shader, std::shared_ptr<Level> level);

	/**
	*	Renders the off-screen textures to the back buffer.
	*	@param shader A string used as a lookup into ShaderManager's map to get the shader program ID
	*	@param type An integer which is sent to the shader as a uniform which identifies which of the attached textures to output
	*	@param exposure A float sent to the shader which controls the tone mapping algorithm
	*/
	void renderToBackBuffer(string shader, GLint type, GLfloat exposure);

	/**
	*	Computes an exposure value based on the luminance texture using the CPU (Very taxing on perforance)
	*/
	void calculateLuminance();

	/**
	*	Sets the currently bound openGL framebuffer object
	*	@param renderTarget An unsigned int used as an openGL identifier for a framebuffer object
	*	@returns Returns a boolean, false if the framebuffer bound is incomplete, else true
	*/
	bool setRenderTarget(GLuint renderTarget);

	/**
	*	Changes the current renderManager phase, this is used to decide what textures and models are rendered for any renderable.
	*	@param phase a phase_enum used to set the phase
	*/
	void setPhase(phase_enum phase);

	/**
	*	Sets the current super sampling amount (1x, 2x, 4x or 8x)
	*	@param samples An unsigned int determining the number of samples per pixel (2^samples)
	*/
	void setSuperSampling(GLuint samples);

	/**
	*	Gets the current super sampling amount
	*	@return an unsigned int representing the number of samples per pixel (2^samples)
	*/
	GLuint getSuperSampling();

	/**
	*	Culls world-space Lights using a sphere-sphere colision check
	*	@param LightArray an array of LightProperties structs
	*	@param size the number of structs in the array
	*/
	void viewFrustumCull(LightProperties* LightArray, GLint size);

	/**
	*	Culls world-space tiles using a sphere-sphere colision check
	*	@param tiles a vector of shared_ptr to tile objects
	*/
	void viewFrustumCull(vector<std::shared_ptr<Tile>> tiles);

	/**
	*	Culls world-space props using a sphere-sphere colision check
	*	@param tiles a vector of shared_ptr to tile objects
	*/
	void viewFrustumCull(vector<std::shared_ptr<Prop>> props);

	GLuint m_gBuffer; /**< OpenGl indentifier for the geometry framebuffer (g-buffer)*/
	GLuint m_phongBuffer; /**< OpenGL indentifier for the phong framebuffer */
	GLuint m_shadowMapBuffer; /**< OpenGL indentifier for the shadowmap framebuffer */

private:

	/**
	*	Creates the g-buffer and attaches it's textures
	*/
	void initGeometryBuffer();

	/**
	*	Creates the phong framebuffer and attaches it's textures
	*/
	void initPhongBuffer();

	/**
	*	Creates the shadowmap framebuffer and attaches it's textures
	*/
	void initShadowMapBuffer();


	/**
	*	Creates the quad VAO used whend drawing UI and attaches it's textures
	*/
	void initScreenQuad();

	/**
	*	Creates and returns a cullingSphere struct using data from the camera Manager
	*	@return	Returns a cullingSphere struct used in camera based culling
	*	@see viewFrustumCull()
	*/
	cullingSphere calculateCullingSphere();
		
	GLuint m_diffuseRT; /**< OpenGL indentifier for the diffuse(albedo) g-buffer layer */
	GLuint m_specularRT; /**< OpenGL indentifier for the specular color g-buffer layer */
	GLuint m_normalRT; /**< OpenGL indentifier for the normal g-buffer layer */
	GLuint m_positionRT; /**< OpenGL indentifier for the view-space positions g-buffer layer */
	GLuint m_phongRT; /**< OpenGL indentifier for the shaded texture used in the phong framebuffer */
	GLuint m_depthRT; /**< OpenGL indentifier for the depth texture used in the g-buffer */
	GLuint m_phongDepthRT; /**< OpenGL indentifier for the depth texture used in the phong framebuffer */
	GLuint m_shadowMapDepthRT; /**< OpenGL indentifier for the depth texture used in the shadowmap framebuffer*/
	GLuint quad_VAO; /**< OpenGL indentifier for the VAO used when drawing a full-screen quad, also used for drawing UI elements */
	std::shared_ptr<Skybox> m_skybox; /**< Shared pointer to a skybox object used with renderSkybox() */
	std::shared_ptr<ShaderManager> m_shaderManager; /**< Shared pointer to the shader manager */
	std::shared_ptr<ConfigManager> m_configManager; /**< shared pointer to the config manager */
	std::shared_ptr<CameraManager> m_cameraManager; /**< Shared pointer to the camera manager */
	string m_currentRenderer; /**< Shared pointer to the camera manager */
	GLfloat m_exposure; /**< Float used to control current exposure in eye-adaptation */
	GLfloat m_targetExposure; /**< Float used to set target exposure in eye-adaptation */
	GLfloat m_exposureAdjustSpeed; /**< Float used to cap exposure change per tick in eye-adaptation* */
	phase_enum m_currentPhase; /**< enum used to control current phase of rendering */
	GLuint m_superSampling; /**< GLuint used to control super sampling */
};