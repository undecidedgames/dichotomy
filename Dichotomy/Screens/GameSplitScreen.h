#pragma once

#include "AbstractScreen.h"

class GameSplitScreen : public AbstractScreen 
{
public:
	GameSplitScreen(std::shared_ptr<ServiceManager> serviceManager, std::shared_ptr<ConfigManager> configManager, std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<RenderManager> renderManager, std::shared_ptr<LevelManager> levelManager, std::shared_ptr<AudioManager> audioManager, std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<CollisionManager> collisionManager, std::shared_ptr<UIManager> UIManager, std::shared_ptr<GameTimer> timer);
	~GameSplitScreen();

	void init();
	void render();

	void update();

private:
	GLuint m_visibleBuffer;

	bool m_freeCam;

	GLfloat m_exposure;
};


