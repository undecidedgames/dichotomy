#include "ScreenManager.h"

ScreenManager::ScreenManager(std::shared_ptr<ServiceManager> serviceManager, std::shared_ptr<ConfigManager> configManager, std::shared_ptr<RenderManager> renderManager, std::shared_ptr<AudioManager> audioManager, std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<LevelManager> levelManager, std::shared_ptr<CollisionManager> collisionManager, std::shared_ptr<UIManager> UIManager, std::shared_ptr<NetworkManager> networkManager, std::shared_ptr<GameTimer> timer)
{
	std::cout << "ScreenManager constructor called" << std::endl;
	m_serviceManager = serviceManager;
	m_configManager = configManager;
	m_renderManager = renderManager;
	m_audioManager = audioManager;
	m_cameraManager = cameraManager;
	m_levelManager = levelManager;
	m_collisionManager = collisionManager;
	m_UIManager = UIManager;
	m_networkManager = networkManager;
	m_timer = timer;
	m_VSync = 1;

	window = setupRC(glContext);
	
	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;

	m_screenMap["GameSplitScreen"] = std::shared_ptr<GameSplitScreen>(new GameSplitScreen(m_serviceManager, m_configManager, m_shaderManager, m_renderManager, m_levelManager, m_audioManager, m_cameraManager, m_collisionManager, m_UIManager, m_timer));
	m_screenMap["StartScreen"] = std::shared_ptr<StartScreen>(new StartScreen(m_serviceManager, m_configManager, m_renderManager, m_UIManager));
	m_screenMap["OptionsScreen"] = std::shared_ptr<OptionsScreen>(new OptionsScreen(m_serviceManager, m_configManager, m_renderManager, m_UIManager));
	m_screenMap["ArtistTestScreen"] = std::shared_ptr<ArtistTestScreen>(new ArtistTestScreen(m_serviceManager, m_configManager, m_shaderManager, m_renderManager, m_levelManager, m_audioManager, m_cameraManager, m_collisionManager, m_UIManager));
	m_screenMap["NetworkingTestScreen"] = std::shared_ptr<NetworkingTestScreen>(new NetworkingTestScreen(m_serviceManager, m_configManager, m_shaderManager, m_renderManager, m_levelManager, m_audioManager, m_cameraManager, m_collisionManager, m_UIManager, m_networkManager));
	
	m_screenMap["CurrentScreen"] = m_screenMap["StartScreen"];
	
	m_screenMap["StartScreen"]->init();
	m_screenMap["OptionsScreen"]->init();
	m_screenMap["NetworkingTestScreen"]->init();
	m_screenMap["ArtistTestScreen"]->init();
	m_screenMap["GameSplitScreen"]->init();
	

	m_screenMap["StartScreen"]->setVSync(m_VSync);
	m_screenMap["OptionsScreen"]->setVSync(m_VSync);
	m_screenMap["NetworkingTestScreen"]->setVSync(m_VSync);
	m_screenMap["ArtistTestScreen"]->setVSync(m_VSync);
	m_screenMap["GameSplitScreen"]->setVSync(m_VSync);
	
}


ScreenManager::~ScreenManager()
{
	std::cout << "ScreenManager destructor called" << std::endl;

	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(window);
	SDL_Quit();
}


void ScreenManager::init()
{
}


// credit - http://www.altdevblogaday.com/2011/06/23/improving-opengl-error-messages/
void FormatDebugOutputARB(char outStr[], size_t outStrSize, GLenum source, GLenum type, GLuint id, GLenum severity, const char *msg)
{
    char sourceStr[32];
    const char *sourceFmt = "UNDEFINED(0x%04X)";
    switch(source)

    {
    case GL_DEBUG_SOURCE_API_ARB:             sourceFmt = "API"; break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB:   sourceFmt = "WINDOW_SYSTEM"; break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER_ARB: sourceFmt = "SHADER_COMPILER"; break;
    case GL_DEBUG_SOURCE_THIRD_PARTY_ARB:     sourceFmt = "THIRD_PARTY"; break;
    case GL_DEBUG_SOURCE_APPLICATION_ARB:     sourceFmt = "APPLICATION"; break;
    case GL_DEBUG_SOURCE_OTHER_ARB:           sourceFmt = "OTHER"; break;
    }

    _snprintf(sourceStr, 32, sourceFmt, source);
 
    char typeStr[32];
    const char *typeFmt = "UNDEFINED(0x%04X)";
    switch(type)
    {

    case GL_DEBUG_TYPE_ERROR_ARB:               typeFmt = "ERROR"; break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB: typeFmt = "DEPRECATED_BEHAVIOR"; break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB:  typeFmt = "UNDEFINED_BEHAVIOR"; break;
    case GL_DEBUG_TYPE_PORTABILITY_ARB:         typeFmt = "PORTABILITY"; break;
    case GL_DEBUG_TYPE_PERFORMANCE_ARB:         typeFmt = "PERFORMANCE"; break;
    case GL_DEBUG_TYPE_OTHER_ARB:               typeFmt = "OTHER"; break;
    }
    _snprintf(typeStr, 32, typeFmt, type);

 
    char severityStr[32];
    const char *severityFmt = "UNDEFINED";
    switch(severity)
    {
    case GL_DEBUG_SEVERITY_HIGH_ARB:   severityFmt = "HIGH";   break;
    case GL_DEBUG_SEVERITY_MEDIUM_ARB: severityFmt = "MEDIUM"; break;
    case GL_DEBUG_SEVERITY_LOW_ARB:    severityFmt = "LOW"; break;
    }

    _snprintf(severityStr, 32, severityFmt, severity);
 
    _snprintf(outStr, outStrSize, "OpenGL: %s [source=%s type=%s severity=%s id=%d]",
        msg, sourceStr, typeStr, severityStr, id);
}


static void CALLBACK DebugCallbackARB(GLenum source, GLenum type, GLuint id, GLenum severity,
                     GLsizei length, const GLchar *message, GLvoid *userParam)
{
    (void)length;
    FILE *outFile = (FILE*)userParam;
    char finalMessage[256];
    FormatDebugOutputARB(finalMessage, 256, source, type, id, severity, message);
    fprintf(outFile, "%s\n", finalMessage);
}


SDL_Window* ScreenManager::setupRC(SDL_GLContext &context)
{
	SDL_Window* window;
	
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	// Request an OpenGL 3.0 context.
	#if DEBUG
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS,SDL_GL_CONTEXT_DEBUG_FLAG);
	#endif
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 0);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

	// Create window based on configManager variables
	if (SDL_GetNumVideoDisplays() > 1)
	{
		m_displayDevice = 1;
	} else m_displayDevice = 0;

    window = SDL_CreateWindow(	m_configManager->m_propertyTree.get<string>("window.name").c_str(), 
								SDL_WINDOWPOS_CENTERED_DISPLAY(m_displayDevice), 
								SDL_WINDOWPOS_CENTERED_DISPLAY(m_displayDevice), 
								m_configManager->m_propertyTree.get<int>("window.width"), 
								m_configManager->m_propertyTree.get<int>("window.height"), 
								SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (!window) // Check window was created OK
        std::cout << "Unable to create window" << std::endl;
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
	
	//setVSync(1);
	SDL_GL_SetSwapInterval(1);
	
	#if DEBUG
	PFNGLDEBUGMESSAGECALLBACKARBPROC glDebugMessageCallbackARB = NULL;
	glDebugMessageCallbackARB  = (PFNGLDEBUGMESSAGECALLBACKARBPROC) SDL_GL_GetProcAddress("glDebugMessageCallbackARB");

	if(glDebugMessageCallbackARB != NULL)
	{
		std::cout << "DEBUG OUTPUT SUPPORTED" << std::endl;
		
		glDebugMessageCallbackARB(DebugCallbackARB,stderr);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
	}
	else std::cout << "DEBUG OUTPUT NOT SUPPORTED!" << std::endl;
		
	#endif
	
	return window;
}


void ScreenManager::update()
{
	if (m_screenMap["CurrentScreen"]->screenChangeReady() == true)
	{
		setCurrentScreen(m_screenMap[m_screenMap["CurrentScreen"]->screenChange()]);
	}	

	if (m_screenMap["CurrentScreen"]->getVSyncChange() != m_VSync)
	{
		setVSync(m_screenMap["CurrentScreen"]->getVSyncChange());
	}

	m_screenMap["CurrentScreen"]->update();
}


void ScreenManager::render()
{
	m_screenMap["CurrentScreen"]->render();
	SDL_GL_SwapWindow(window);
}


void ScreenManager::setCurrentScreen(std::shared_ptr<AbstractScreen> newScreen)
{
	m_screenMap["CurrentScreen"]->setScreenChangeReady(false);
	m_screenMap["CurrentScreen"]->setScreenChange("null");	

	m_screenMap["CurrentScreen"] = newScreen;
	m_screenMap["CurrentScreen"]->setLevelManager(m_levelManager);
	m_screenMap["CurrentScreen"]->setRenderManager(m_renderManager);
	m_screenMap["CurrentScreen"]->setShaderManager(m_shaderManager);
	m_screenMap["CurrentScreen"]->setUIManager(m_UIManager);
}


void ScreenManager::setShaderManager(std::shared_ptr<ShaderManager> shaderManager)
{
	m_shaderManager = shaderManager;
	m_screenMap["CurrentScreen"]->setShaderManager(m_shaderManager);
}


void ScreenManager::setLevelManager(std::shared_ptr<LevelManager> levelManager)
{
	m_levelManager = levelManager;
	m_screenMap["CurrentScreen"]->setLevelManager(m_levelManager);
}


void ScreenManager::setRenderManager(std::shared_ptr<RenderManager> renderManager)
{
	m_renderManager = renderManager;
	m_screenMap["CurrentScreen"]->setRenderManager(m_renderManager);
}

void ScreenManager::setUIManager(std::shared_ptr<UIManager> UImanager)
{
	m_UIManager = UImanager;
	m_screenMap["CurrentScreen"]->setUIManager(m_UIManager);
}


void ScreenManager::setVSync()
{
	m_VSync++;
	if (m_VSync > 1) m_VSync = -1;

	if (SDL_GL_SetSwapInterval(m_VSync) == -1)
	{
		std::cout << std::endl << "SDL_GL_SetSwapInterval: " << SDL_GetError() << std::endl << std::endl;
	} else std::cout << "VSync set to: " << (int)m_VSync << std::endl;
}


void ScreenManager::setVSync(int toggle)
{
	m_VSync = toggle;

	if (SDL_GL_SetSwapInterval(m_VSync) == -1)
	{
		//std::cout << std::endl << "SDL_GL_SetSwapInterval: " << SDL_GetError() << std::endl << std::endl;
	} else std::cout << "VSync set to: " << (int)m_VSync << std::endl;
}