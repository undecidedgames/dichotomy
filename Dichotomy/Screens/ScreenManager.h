#pragma once

#include <iostream>
#include <memory>
#include <unordered_map>

#include <SDL.h>
#include <GL/glew.h>

#include "AbstractScreen.h"
#include "GameSplitScreen.h"
#include "StartScreen.h"
#include "ArtistTestScreen.h"
#include "NetworkingTestScreen.h"
#include "OptionsScreen.h"
#include "../Service/ServiceManager.h"
#include "../Config/ConfigManager.h"
#include "../Renderer/RenderManager.h"
#include "../Shaders/ShaderManager.h"
#include "../Levels/LevelManager.h"
#include "../Audio/AudioManager.h"
#include "../Collisions/CollisionManager.h"
#include "../UI/UIManager.h"
#include "../Networking/NetworkManager.h"
#include "../Timer/GameTimer.h"

using namespace std;

class ScreenManager
{
public:
	ScreenManager(std::shared_ptr<ServiceManager> serviceManager, std::shared_ptr<ConfigManager> configManager, std::shared_ptr<RenderManager> renderManager, std::shared_ptr<AudioManager> audioManager, std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<LevelManager> levelManager, std::shared_ptr<CollisionManager> collisionManager, std::shared_ptr<UIManager> UIManager, std::shared_ptr<NetworkManager> networkManager, std::shared_ptr<GameTimer> gameTimer);
	~ScreenManager();

	void init();
	void update();
	void render();

	void setShaderManager(std::shared_ptr<ShaderManager> shaderManager);
	void setLevelManager(std::shared_ptr<LevelManager> levelManager);
	void setRenderManager(std::shared_ptr<RenderManager> renderManager);
	void setUIManager(std::shared_ptr<UIManager> UImanager);

	void setVSync();
	void setVSync(int toggle);
	
private:
	SDL_GLContext glContext;
	SDL_Window* window;

	SDL_Window* setupRC(SDL_GLContext &context);

private:
	void setCurrentScreen(std::shared_ptr<AbstractScreen> newScreen);

	std::unordered_map<std::string, std::shared_ptr<AbstractScreen>> m_screenMap;	

	std::shared_ptr<GameTimer> m_timer;

private:
	std::shared_ptr<ServiceManager> m_serviceManager;
	std::shared_ptr<ConfigManager> m_configManager;
	std::shared_ptr<ShaderManager> m_shaderManager;
	std::shared_ptr<RenderManager> m_renderManager;
	std::shared_ptr<LevelManager> m_levelManager;
	std::shared_ptr<AudioManager> m_audioManager;
    std::shared_ptr<CollisionManager> m_collisionManager;
	std::shared_ptr<CameraManager> m_cameraManager;
	std::shared_ptr<UIManager> m_UIManager;
	std::shared_ptr<NetworkManager> m_networkManager;

private:
	void checkError(char* func);

private:
	int m_VSync;
	int m_displayDevice;
};