#pragma once

#include "AbstractScreen.h"

class NetworkingTestScreen : public AbstractScreen 
{
public:
	NetworkingTestScreen(std::shared_ptr<ServiceManager> serviceManager, std::shared_ptr<ConfigManager> configManager, std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<RenderManager> renderManager, std::shared_ptr<LevelManager> levelManager, std::shared_ptr<AudioManager> audioManager, std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<CollisionManager> collisionManager, std::shared_ptr<UIManager> UIManager, std::shared_ptr<NetworkManager> networkManager);
	~NetworkingTestScreen();

	void init();
	void render();

	void update();

private:
	GLuint m_visibleBuffer;

	bool quality;
	bool m_wireframe;
	bool freeCam;

	GLfloat m_exposure;

	unsigned int m_player;
};


