#pragma once

#include <memory>
#include <string>

#include "AbstractScreen.h"
#include "../Service/ServiceManager.h"
#include "../Input/SDLInput.h"
#include "../Audio/AudioManager.h"
#include "../Config/ConfigManager.h"
#include "../Shaders/ShaderManager.h"
#include "../Renderer/RenderManager.h"
#include "../Levels/LevelManager.h"
#include "../Collisions/CollisionManager.h"
#include "../UI/UIManager.h"
#include "../Networking/NetworkManager.h"
#include "../Timer/GameTimer.h"


class AbstractScreen {
public:
	virtual ~AbstractScreen(){};
	virtual void init() = 0;
	virtual void render() = 0;
	virtual void update() = 0;
	virtual void setShaderManager(std::shared_ptr<ShaderManager> shaderManager) { m_shaderManager = shaderManager; };
	virtual void setLevelManager(std::shared_ptr<LevelManager> levelManager) { m_levelManager = levelManager; } ;
	virtual void setRenderManager(std::shared_ptr<RenderManager> renderManager) { m_renderManager = renderManager; };
	void setUIManager(std::shared_ptr<UIManager> UImanager) { m_UIManager = UImanager; };

	std::string screenChange()
	{
		return m_screenChange;
	}

	void setScreenChange(std::string screen)
	{
		m_screenChange = screen;
	}

	void setScreenChangeReady(bool changeReady)
	{
		m_screenChangeReady = changeReady;
	}

	bool screenChangeReady()
	{
		return m_screenChangeReady;
	}
	
	void setVSync()
	{
		m_VSync++;
		if (m_VSync > 1) m_VSync = -1;
	}

	void setVSync(int toggle)
	{
		m_VSync = toggle;
	}

	int getVSyncChange()
	{
		return m_VSync;
	}

protected:
	std::shared_ptr<ServiceManager> m_serviceManager;
	std::shared_ptr<ConfigManager> m_configManager;
	std::shared_ptr<ShaderManager> m_shaderManager;
	std::shared_ptr<RenderManager> m_renderManager;
	std::shared_ptr<LevelManager> m_levelManager;
	std::shared_ptr<AudioManager> m_audioManager;
	std::shared_ptr<CameraManager> m_cameraManager;
	std::shared_ptr<CollisionManager> m_collisionManager;
	std::shared_ptr<UIManager> m_UIManager;
	std::shared_ptr<NetworkManager> m_networkManager;
	std::shared_ptr<GameTimer> m_timer;
	std::string m_screenChange;
	bool m_screenChangeReady;
	int m_VSync;
};


