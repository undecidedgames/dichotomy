#include "ArtistTestScreen.h"


ArtistTestScreen::ArtistTestScreen(std::shared_ptr<ServiceManager> serviceManager, std::shared_ptr<ConfigManager> configManager, std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<RenderManager> renderManager, std::shared_ptr<LevelManager> levelManager, std::shared_ptr<AudioManager> audioManager, std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<CollisionManager> collisionManager, std::shared_ptr<UIManager> UIManager)
{
	m_serviceManager = serviceManager;
	m_configManager = configManager;
	m_shaderManager = shaderManager;
	m_renderManager = renderManager;
	m_levelManager = levelManager;
	m_audioManager = audioManager;
	m_cameraManager = cameraManager;
	m_collisionManager = collisionManager;
	m_UIManager = UIManager;

	freeCam = false;
}


ArtistTestScreen::~ArtistTestScreen(void)
{
}


void ArtistTestScreen::init()
{
	m_levelManager->loadLevel("ArtistTest");
	m_levelManager->setCurrentLevel("ArtistTest");
	
	m_audioManager->init(m_configManager);
	visibleBuffer = 0;
	quality = 1;
	exposure = 1;
	m_wireframe = false;
}


void ArtistTestScreen::render()
{
	// draw left viewport
	m_cameraManager->setPosition(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1)->getPosition() + glm::vec3(0.0,0.9f,1.7f));
	m_renderManager->setPhase(HUMAN_PHASE);
	glViewport(0,0,
				m_configManager->m_propertyTree.get<int>("window.width")/2, 
				m_configManager->m_propertyTree.get<int>("window.height"));

	m_renderManager->setRenderTarget(m_renderManager->m_gBuffer);
	GLenum buffers4[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3};
	glDrawBuffers(4,buffers4);
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_renderManager->instancedRender(m_levelManager->getCurrentLevel()->m_tiles,"PBRBlinn-tex-instanced");
	m_renderManager->instancedRender(m_levelManager->getCurrentLevel()->m_props,"PBRBlinn-tex-instanced");
	m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1),"PBRBlinn-tex");
	m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2),"PBRBlinn-tex");

	m_renderManager->setRenderTarget(m_renderManager->m_phongBuffer);
	GLenum buffers5[] = {GL_COLOR_ATTACHMENT0};
	glDrawBuffers(1,buffers5);
	glClearColor(0.0f,1.0f,0.0f,0.5f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	m_levelManager->getCurrentLevel()->updateLightVectors(m_cameraManager->getViewMatrix());
	m_renderManager->renderPhongBuffer("multiLightPBRBlinn", m_levelManager->getCurrentLevel());
	
	
	// draw right viewport
	m_cameraManager->setPosition(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2)->getPosition() + glm::vec3(0.0,0.9f,1.7f));
	m_renderManager->setPhase(DEMON_PHASE);
	glViewport(m_configManager->m_propertyTree.get<int>("window.width")/2,0,
				m_configManager->m_propertyTree.get<int>("window.width")/2, 
				m_configManager->m_propertyTree.get<int>("window.height"));

	m_renderManager->setRenderTarget(m_renderManager->m_gBuffer);
	glDrawBuffers(4,buffers4);
	
	m_renderManager->instancedRender(m_levelManager->getCurrentLevel()->m_tiles,"PBRBlinn-tex-instanced");
	m_renderManager->instancedRender(m_levelManager->getCurrentLevel()->m_props,"PBRBlinn-tex-instanced");
	m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1),"PBRBlinn-tex");
	m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2),"PBRBlinn-tex");

	
	// shade screen
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	m_renderManager->setRenderTarget(m_renderManager->m_phongBuffer);
	glDrawBuffers(1,buffers5);
		
	m_levelManager->getCurrentLevel()->updateLightVectors(m_cameraManager->getViewMatrix());
	m_renderManager->renderPhongBuffer("multiLightPBRBlinn", m_levelManager->getCurrentLevel());
	
	// draw final image to screen
	glViewport(0,0,
				m_configManager->m_propertyTree.get<int>("window.width"), 
				m_configManager->m_propertyTree.get<int>("window.height"));
	glBindFramebuffer(GL_FRAMEBUFFER,0);
	glClearColor(1.0f,0.0f,0.0f,0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	m_renderManager->renderToBackBuffer("passThroughColor",visibleBuffer,exposure);
	glViewport(0,0,
				m_configManager->m_propertyTree.get<int>("window.width")/2, 
				m_configManager->m_propertyTree.get<int>("window.height"));
	m_renderManager->renderSkyBox("cubemap");
	glViewport(m_configManager->m_propertyTree.get<int>("window.width")/2,0,
				m_configManager->m_propertyTree.get<int>("window.width")/2, 
				m_configManager->m_propertyTree.get<int>("window.height"));
	m_renderManager->renderSkyBox("cubemap");
}

void ArtistTestScreen::update()
{
	std::shared_ptr<Input> inputService = m_serviceManager->getInput();

	if(inputService->getKeyState(SDL_SCANCODE_ESCAPE)) exit(0);

	if(inputService->getKeyState(SDL_SCANCODE_F1)) m_levelManager->getCurrentLevel()->reloadLevel(true);
	if(inputService->getKeyState(SDL_SCANCODE_F2)) m_levelManager->getCurrentLevel()->reloadLevel(false);
		
	if(inputService->getKeyState(SDL_SCANCODE_1)) visibleBuffer = 0;
	if(inputService->getKeyState(SDL_SCANCODE_2)) visibleBuffer = 1;
	if(inputService->getKeyState(SDL_SCANCODE_3)) visibleBuffer = 2;
	if(inputService->getKeyState(SDL_SCANCODE_4)) visibleBuffer = 3;
	if(inputService->getKeyState(SDL_SCANCODE_5)) visibleBuffer = 4;
	if(inputService->getKeyState(SDL_SCANCODE_6)) visibleBuffer = 5;
	if(inputService->getKeyState(SDL_SCANCODE_7)) visibleBuffer = 6;
	if(inputService->getKeyState(SDL_SCANCODE_C)) freeCam = false;
	if(inputService->getKeyState(SDL_SCANCODE_V)) freeCam = true;
	if(inputService->getKeyState(SDL_SCANCODE_Z)) m_renderManager->setPhase(DEMON_PHASE);
	if(inputService->getKeyState(SDL_SCANCODE_X)) m_renderManager->setPhase(HUMAN_PHASE);

	// Controller Input
	if(inputService->getControllerButtonState(0, A)) m_audioManager->playSample("background", 0.8f,"1");
	if(inputService->getControllerButtonState(0, X)) m_audioManager->stopSample("background");
	
	// checks collison with player 2
	m_collisionManager->handleCollision(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1), m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2));

	/*// checks collisions with props
	for(size_t i = 0; i < m_levelManager->getCurrentLevel()->m_props.size(); ++i)
	{
		for(size_t j = 0; j < m_levelManager->getCurrentLevel()->m_props[i]->m_propCount; ++j)	
		{
			m_collisionManager->handleCollision(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1), m_levelManager->getCurrentLevel()->m_props[i], j);
			m_collisionManager->handleCollision(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2), m_levelManager->getCurrentLevel()->m_props[i], j);
		}
	}*/
	
	// checks collisions with tiles
	/*for(size_t i = 0; i < m_levelManager->getCurrentLevel()->m_tiles.size(); ++i)
	{
		for(size_t j = 0; j < m_levelManager->getCurrentLevel()->m_tiles[i]->m_tileCount; ++j)	
		{
			m_collisionManager->handleCollision(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1), m_levelManager->getCurrentLevel()->m_tiles[i], j);
			m_collisionManager->handleCollision(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2), m_levelManager->getCurrentLevel()->m_tiles[i], j);
		}
	}*/
	
	m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1)->move(inputService->getControllerAxisUnitVector(0,LEFT));
	m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2)->move(inputService->getControllerAxisUnitVector(1,LEFT));

	// KeyBoard controls player 1
	if(inputService->getKeyState(SDL_SCANCODE_W))	m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1)->move(std::make_pair<int, int>(0, -1));
	if(inputService->getKeyState(SDL_SCANCODE_S))	m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1)->move(std::make_pair<int, int>(0, 1));
	if(inputService->getKeyState(SDL_SCANCODE_D))	m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1)->move(std::make_pair<int, int>(1, 0));
	if(inputService->getKeyState(SDL_SCANCODE_A))	m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1)->move(std::make_pair<int, int>(-1, 0));

	// Keyboard controls player 2
	if(inputService->getKeyState(SDL_SCANCODE_UP))	m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2)->move(std::make_pair<int, int>(0, -1));
	if(inputService->getKeyState(SDL_SCANCODE_DOWN))	m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2)->move(std::make_pair<int, int>(0, 1));
	if(inputService->getKeyState(SDL_SCANCODE_RIGHT))	m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2)->move(std::make_pair<int, int>(1, 0));
	if(inputService->getKeyState(SDL_SCANCODE_LEFT))	m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2)->move(std::make_pair<int, int>(-1, 0));

	
	m_levelManager->getCurrentLevel()->update();
	
}
