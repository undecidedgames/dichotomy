#include "GameSplitScreen.h"


GameSplitScreen::GameSplitScreen(std::shared_ptr<ServiceManager> serviceManager, std::shared_ptr<ConfigManager> configManager, std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<RenderManager> renderManager, std::shared_ptr<LevelManager> levelManager, std::shared_ptr<AudioManager> audioManager, std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<CollisionManager> collisionManager, std::shared_ptr<UIManager> UIManager, std::shared_ptr<GameTimer> timer)
{
	m_serviceManager = serviceManager;
	m_configManager = configManager;
	m_shaderManager = shaderManager;
	m_renderManager = renderManager;
	m_levelManager = levelManager;
	m_audioManager = audioManager;
	m_cameraManager = cameraManager;
	m_collisionManager = collisionManager;
	m_UIManager = UIManager;
	m_timer = timer;

	m_freeCam = true;;
}


GameSplitScreen::~GameSplitScreen(void)
{
}


void GameSplitScreen::init()
{
	std::string startScreen = m_configManager->m_propertyTree.get<string>("levelManager.startingScreen");
	m_levelManager->loadLevel(startScreen);
	m_levelManager->setCurrentLevel(startScreen);

	
	m_audioManager->init(m_configManager);
	m_audioManager->playSample("ambient",1.0f,"1");
	m_visibleBuffer = 0;
	m_exposure = 1;
}


void GameSplitScreen::render()
{
	glDisable(GL_BLEND);
	// draw left viewport
	if(m_freeCam == true)
		m_cameraManager->setPosition(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1)->getPosition() + glm::vec3(0.0,15.0f,10.0f));
	else
		m_cameraManager->setPosition(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1)->getPosition() + glm::vec3(0.0,2.5f,2.5f));

	m_renderManager->setPhase(HUMAN_PHASE);
	glViewport(0,0,
				(m_configManager->m_propertyTree.get<int>("window.width")*m_renderManager->getSuperSampling())/2, 
				m_configManager->m_propertyTree.get<int>("window.height")*m_renderManager->getSuperSampling());

	m_renderManager->setRenderTarget(m_renderManager->m_gBuffer);
	GLenum buffers4[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3};
	glDrawBuffers(4,buffers4);
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_renderManager->instancedRender(m_levelManager->getCurrentLevel()->m_tiles,"PBRBlinn-tex-instanced");
	m_renderManager->instancedRender(m_levelManager->getCurrentLevel()->m_props,"PBRBlinn-tex-instanced");
	for(size_t i = 0; i < m_levelManager->getCurrentLevel()->m_dynamicProps.size(); ++i)
		m_renderManager->render(m_levelManager->getCurrentLevel()->m_dynamicProps[i] ,"PBRBlinn-tex");
	m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1),"PBRBlinn-tex");
	m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2),"PBRBlinn-tex");
	m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1)->getWeapon(),"PBRBlinn-tex");
	m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2)->getWeapon(),"PBRBlinn-tex");
	for(size_t i = 0; i < m_levelManager->getCurrentLevel()->m_actorManager->getActorVector()->size(); ++i)
	{
		m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getActor(i) ,"PBRBlinn-tex");
		m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getActor(i)->getWeapon() ,"PBRBlinn-tex");
	}



	m_renderManager->setRenderTarget(m_renderManager->m_phongBuffer);
	GLenum buffers5[] = {GL_COLOR_ATTACHMENT0};
	glDrawBuffers(1,buffers5);
	glClearColor(0.0f,1.0f,0.0f,0.5f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	std::shared_ptr<Level> temp = m_levelManager->getCurrentLevel();

	// Cull lights in world space
	m_renderManager->viewFrustumCull(temp->m_lightsWorldSpace.data(), temp->m_lightsWorldSpace.size());
	// Update the view space light array with the updated world space light array
	m_levelManager->getCurrentLevel()->updateLightVectors(m_cameraManager->getViewMatrix());
	m_renderManager->renderPhongBuffer("multiLightPBRBlinn", m_levelManager->getCurrentLevel());
	
	
	// draw right viewport
	if(m_freeCam == true)
	{
		m_cameraManager->setPosition(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2)->getPosition() + glm::vec3(0.0,15.0f,10.0f));
	}
	else
	{
		m_cameraManager->setPosition(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2)->getPosition() + glm::vec3(0.0,2.5f,2.5f));
	}
	m_renderManager->setPhase(DEMON_PHASE);
	glViewport((m_configManager->m_propertyTree.get<int>("window.width")*m_renderManager->getSuperSampling())/2,0,
				(m_configManager->m_propertyTree.get<int>("window.width")*m_renderManager->getSuperSampling())/2, 
				(m_configManager->m_propertyTree.get<int>("window.height")*m_renderManager->getSuperSampling()));

	m_renderManager->setRenderTarget(m_renderManager->m_gBuffer);
	glDrawBuffers(4,buffers4);
	
	m_renderManager->instancedRender(m_levelManager->getCurrentLevel()->m_tiles,"PBRBlinn-tex-instanced");
	m_renderManager->instancedRender(m_levelManager->getCurrentLevel()->m_props,"PBRBlinn-tex-instanced");
	for(size_t i = 0; i < m_levelManager->getCurrentLevel()->m_dynamicProps.size(); ++i)
		m_renderManager->render(m_levelManager->getCurrentLevel()->m_dynamicProps[i] ,"PBRBlinn-tex");
	m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1),"PBRBlinn-tex");
	m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2),"PBRBlinn-tex");
	m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1)->getWeapon(),"PBRBlinn-tex");
	m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(2)->getWeapon(),"PBRBlinn-tex");
	for(size_t i = 0; i < m_levelManager->getCurrentLevel()->m_actorManager->getActorVector()->size(); ++i)
	{
		m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getActor(i), "PBRBlinn-tex");
		m_renderManager->render(m_levelManager->getCurrentLevel()->m_actorManager->getActor(i)->getWeapon(), "PBRBlinn-tex");
	}
	
	// shade screen
	m_renderManager->setRenderTarget(m_renderManager->m_phongBuffer);
	glDrawBuffers(1,buffers5);
	

	// Cull lights in world space
	m_renderManager->viewFrustumCull(temp->m_lightsWorldSpace.data(), temp->m_lightsWorldSpace.size());
	// Update the view space light array with the updated world space light array
	m_levelManager->getCurrentLevel()->updateLightVectors(m_cameraManager->getViewMatrix());
	m_renderManager->renderPhongBuffer("multiLightPBRBlinn", m_levelManager->getCurrentLevel());
	
	// draw GUI followed by g-buffer layer and cubemap.
	glViewport(0,0,
				m_configManager->m_propertyTree.get<int>("window.width"), 
				m_configManager->m_propertyTree.get<int>("window.height"));
	glBindFramebuffer(GL_FRAMEBUFFER,0);
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_BLEND);
	glDepthMask(false);
	m_renderManager->renderToBackBuffer("passThroughColor", m_visibleBuffer, m_exposure);
	glViewport(0,0,
				m_configManager->m_propertyTree.get<int>("window.width")/2, 
				m_configManager->m_propertyTree.get<int>("window.height"));
	m_renderManager->renderSkyBox("cubemap");
	glViewport(m_configManager->m_propertyTree.get<int>("window.width")/2,0,
				m_configManager->m_propertyTree.get<int>("window.width")/2, 
				m_configManager->m_propertyTree.get<int>("window.height"));
	m_renderManager->renderSkyBox("cubemap");
	glViewport(0,0,
				m_configManager->m_propertyTree.get<int>("window.width"), 
				m_configManager->m_propertyTree.get<int>("window.height"));

	m_renderManager->renderUIElement(m_UIManager->getForm("testForm")->getElement("testText"), "UIRender");
	//m_renderManager->renderUIElement(m_UIManager->getForm("FPS")->getElement("FPS"), "UIRender");
	//m_renderManager->renderUIElement(m_UIManager->getForm("FPS")->getElement("FrameTime"), "UIRender");
	//m_renderManager->renderUIElement(m_UIManager->getForm("ChatBox")->getElement("ChatBox"), "UIRender");

	glDepthMask(true);
}


void GameSplitScreen::update()
{
	m_audioManager->update();
	
#pragma region keyboardInput
	std::shared_ptr<Input> inputService = m_serviceManager->getInput();

	if(inputService->getKeyState(SDL_SCANCODE_ESCAPE)) 
	{
		setScreenChangeReady(true);
		setScreenChange("StartScreen");
	}

	if(inputService->getKeyState(SDL_SCANCODE_F1)) m_levelManager->getCurrentLevel()->reloadLevel(true);
	if(inputService->getKeyState(SDL_SCANCODE_F2)) m_levelManager->getCurrentLevel()->reloadLevel(false);
	if(inputService->getKeyState(SDL_SCANCODE_F5)) m_levelManager->getCurrentLevel()->printMapNodeGrid();
	if(inputService->getKeyState(SDL_SCANCODE_F6)) m_renderManager->setSuperSampling(1);
	if(inputService->getKeyState(SDL_SCANCODE_F7)) m_renderManager->setSuperSampling(2);
	
	if(inputService->getKeyState(SDL_SCANCODE_T)) m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1)->setPosition(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1)->getPosition() - glm::vec3(0.0,0.0f,0.05f));
	if(inputService->getKeyState(SDL_SCANCODE_G)) m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1)->setPosition(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1)->getPosition() + glm::vec3(0.0,0.0f,0.05f));
	
	if(inputService->getKeyState(SDL_SCANCODE_LEFT)) m_cameraManager->rotateCamera(glm::vec3(0.0f,0.0f,1.0f), -0.75f);
	if(inputService->getKeyState(SDL_SCANCODE_RIGHT)) m_cameraManager->rotateCamera(glm::vec3(0.0f,0.0f,1.0f), 0.75f);
	if(inputService->getKeyState(SDL_SCANCODE_DOWN)) m_cameraManager->rotateCamera(glm::vec3(1.0f,0.0f,0.0f), 0.75f);
	if(inputService->getKeyState(SDL_SCANCODE_UP)) m_cameraManager->rotateCamera(glm::vec3(1.0f,0.0f,0.0f), -0.75f);
	if(inputService->getKeyState(SDL_SCANCODE_Q)) m_cameraManager->rotateCamera(glm::vec3(0.0f,1.0f,0.0f), -0.75f);
	if(inputService->getKeyState(SDL_SCANCODE_E)) m_cameraManager->rotateCamera(glm::vec3(0.0f,1.0f,0.0f), 0.75f);
	if(inputService->getKeyState(SDL_SCANCODE_L)) std::cout << m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(1)->getCooldown() << endl;
	
	if(inputService->getKeyState(SDL_SCANCODE_1)) m_visibleBuffer = 0;
	if(inputService->getKeyState(SDL_SCANCODE_2)) m_visibleBuffer = 1;
	if(inputService->getKeyState(SDL_SCANCODE_3)) m_visibleBuffer = 2;
	if(inputService->getKeyState(SDL_SCANCODE_4)) m_visibleBuffer = 3;
	if(inputService->getKeyState(SDL_SCANCODE_5)) m_visibleBuffer = 4;
	if(inputService->getKeyState(SDL_SCANCODE_6)) m_visibleBuffer = 5;
	if(inputService->getKeyState(SDL_SCANCODE_7)) m_visibleBuffer = 6;
	if(inputService->getKeyState(SDL_SCANCODE_8)) m_visibleBuffer = 7;
	if(inputService->getKeyState(SDL_SCANCODE_C)) m_freeCam = false;
	if(inputService->getKeyState(SDL_SCANCODE_V)) m_freeCam = true;
	if(inputService->getKeyState(SDL_SCANCODE_Z)) m_renderManager->setPhase(DEMON_PHASE);
#pragma endregion

#pragma region controllerInput

	for (unsigned int player = 0; player < 2; ++player)
	{
		if (m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(player+1)->move(inputService->getControllerAxisUnitVector(player, LEFT)))
		{
			m_audioManager->playSample("footsteps", 0.1f, "2");
		}

		if (inputService->onControllerButtonDown(player, LEFTSHOULDER) == false)
		{
			m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(player+1)->setParry(false);
		}
		else
		{
			if (inputService->onControllerButtonDown(player, LEFTSHOULDER)) 
			{
				m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(player+1)->setParry(true);
			}
		}

		if (inputService->onControllerButtonDown(player, A)==false)
		{
			m_collisionManager->setButtonPress(false,player);
		}


		if (inputService->onControllerButtonDown(player, B))
		{
			m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(player+1)->setSpeed(6.4f);
		}

		if (inputService->onControllerButtonDown(player, B) == false) 
		{
			m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(player+1)->setSpeed(3.2f);
		}

		if(inputService->getControllerButtonState(player, RIGHTSHOULDER)) 
		{
			m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(player+1)->setParry(false);
			if(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(player+1)->getCooldown()<=0)
			{
				
				m_audioManager->playSample("weaponSwing", 0.2f, "2");
				for (size_t index = 0; index < m_levelManager->getCurrentLevel()->m_actorManager->getActorVector()->size(); ++index)
				{
				
					m_collisionManager->handleWeaponStrike(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(player+1), m_levelManager->getCurrentLevel()->m_actorManager->getActor(index), m_audioManager);
					m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(player+1)->resetCooldown();
			
				}
				
			}
		
		}
	}

#pragma endregion

	
#pragma region Collisions
	// For each NPCActor
	for(size_t i = 0; i < m_levelManager->getCurrentLevel()->m_actorManager->getActorVector()->size(); ++i)
	{
		// for each MapNode it is on
		for(size_t j = 0; j < m_levelManager->getCurrentLevel()->m_actorManager->getActor(i)->getMapNodes().size(); ++j)
		{
			// If a Node is marked as inaccessable check for a collision
			if (m_levelManager->getCurrentLevel()->m_actorManager->getActor(i)->getMapNodes().at(j)->m_isAccessable == false)
				m_collisionManager->handleCollision(m_levelManager->getCurrentLevel()->m_actorManager->getActor(i), m_levelManager->getCurrentLevel()->m_actorManager->getActor(i)->getMapNodes().at(j)->m_position);
			else
			{
				// If a DynamicProp is on this node check for a collision
				if (m_levelManager->getCurrentLevel()->m_actorManager->getActor(i)->getMapNodes().at(j)->m_dynamicProp != nullptr)
				{
					m_collisionManager->handleCollision(m_levelManager->getCurrentLevel()->m_actorManager->getActor(i), m_levelManager->getCurrentLevel()->m_actorManager->getActor(i)->getMapNodes().at(j)->m_dynamicProp, m_audioManager);
				}
				else
				{
					// for each AbstractActor on this node check for a collision
					for(size_t k = 0; k < m_levelManager->getCurrentLevel()->m_actorManager->getActor(i)->getMapNodes().at(j)->m_actors.size(); ++k)
					{
						if (m_levelManager->getCurrentLevel()->m_actorManager->getActor(i) != m_levelManager->getCurrentLevel()->m_actorManager->getActor(i)->getMapNodes().at(j)->m_actors[k])
						{
							m_collisionManager->handleCollision(m_levelManager->getCurrentLevel()->m_actorManager->getActor(i)->getMapNodes().at(j)->m_actors[k], m_levelManager->getCurrentLevel()->m_actorManager->getActor(i));
						}
					}
				}				
			}
		}

		if( m_levelManager->getCurrentLevel()->m_actorManager->getActor(i)->getStatePointer()->getState() == 3 && m_levelManager->getCurrentLevel()->m_actorManager->getActor(i)->getCooldown() <= 0.0)
		{
			m_collisionManager->handleWeaponStrike(m_levelManager->getCurrentLevel()->m_actorManager->getActor(i), m_levelManager->getCurrentLevel()->m_actorManager->getActor(i)->getStatePointer()->getTarget(), m_audioManager);
			m_levelManager->getCurrentLevel()->m_actorManager->getActor(i)->resetCooldown();	
		}
	}

	// For each player Actor (no need to test against NPC actors because they will test against the player in their own MapNode checks)
	for(size_t i = 0; i < 2; ++i)
	{
		// for each mapNode it is on
		for(size_t j = 0; j < m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(i)->getMapNodes().size(); ++j)
		{
			// If a DynamicProp is on this node check for a collision
			if (m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(i)->getMapNodes().at(j)->m_dynamicProp != nullptr)
			{
				m_collisionManager->handleCollision(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(i), m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(i)->getMapNodes().at(j)->m_dynamicProp, m_audioManager, m_serviceManager);
			}
			else
			{
				// If a Node is marked as inaccessable check for a collision
				if (m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(i)->getMapNodes().at(j)->m_isAccessable == false)
				{
					m_collisionManager->handleCollision(m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(i), m_levelManager->getCurrentLevel()->m_actorManager->getPlayer(i)->getMapNodes().at(j)->m_position);
				}
			}
		}
	}
#pragma endregion

	m_levelManager->getCurrentLevel()->update();
}
