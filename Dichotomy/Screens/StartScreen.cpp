#include "StartScreen.h"


StartScreen::StartScreen(std::shared_ptr<ServiceManager> serviceManager, std::shared_ptr<ConfigManager> configManager, std::shared_ptr<RenderManager> renderManager, std::shared_ptr<UIManager> UIManager)
{
	m_serviceManager = serviceManager;
	m_configManager = configManager;
	m_renderManager = renderManager;
	m_UIManager = UIManager;
}


StartScreen::~StartScreen(void)
{
}


void StartScreen::init()
{
}


void StartScreen::render()
{
		// draw GUI followed by g-buffer layer and cubemap.
	glViewport(0,0,
				m_configManager->m_propertyTree.get<int>("window.width"), 
				m_configManager->m_propertyTree.get<int>("window.height"));
	glBindFramebuffer(GL_FRAMEBUFFER,0);
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_BLEND);
	glDepthMask(false);
	
	m_renderManager->renderUIElement(m_UIManager->getForm("StartScreen")->getElement("GameTitle"), "UIRender");
	m_renderManager->renderUIElement(m_UIManager->getForm("StartScreen")->getElement("StartGame"), "UIRender");
	m_renderManager->renderUIElement(m_UIManager->getForm("StartScreen")->getElement("Options"), "UIRender");
}


void StartScreen::update()
{
	std::shared_ptr<Input> inputService = m_serviceManager->getInput();

	if (m_UIManager->getForm("StartScreen")->getElement("StartGame")->getMouseOver() == true)
	{
		m_UIManager->getForm("StartScreen")->getElement("StartGame")->setTextColour(255, 100, 255);
		if (inputService->mouseButtonState(1) == true)
		{
			setScreenChangeReady(true);
			setScreenChange("GameSplitScreen");
		}
	}
	else 
	{
		m_UIManager->getForm("StartScreen")->getElement("StartGame")->setTextColour(255, 255, 255);
	}

	if (m_UIManager->getForm("StartScreen")->getElement("Options")->getMouseOver() == true)
	{
		m_UIManager->getForm("StartScreen")->getElement("Options")->setTextColour(255, 100, 255);
		if (inputService->mouseButtonState(1) == true)
		{
			setScreenChangeReady(true);
			setScreenChange("optionsScreen");
		}
	}
	else 
	{
		m_UIManager->getForm("StartScreen")->getElement("Options")->setTextColour(255, 255, 255);
	}
}


void StartScreen::setShaderManager(std::shared_ptr<ShaderManager> shaderManager)
{
	m_shaderManager = shaderManager;
}


void StartScreen::setLevelManager(std::shared_ptr<LevelManager> levelManager)
{
	m_levelManager = levelManager;
}


void StartScreen::setRenderManager(std::shared_ptr<RenderManager> renderManager)
{
	m_renderManager = renderManager;
}