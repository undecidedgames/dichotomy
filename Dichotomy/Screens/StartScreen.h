#pragma once

#include <memory>

#include "AbstractScreen.h"
#include "../Shaders/ShaderManager.h"
#include "../Levels/LevelManager.h"
#include "../Renderer/RenderManager.h"

class StartScreen : public AbstractScreen
{
public:
	StartScreen(std::shared_ptr<ServiceManager> serviceManager, std::shared_ptr<ConfigManager> configManager, std::shared_ptr<RenderManager> renderManager, std::shared_ptr<UIManager> UIManager);
	~StartScreen(void);

	void init();
	void render();
	void update();

	void setShaderManager(std::shared_ptr<ShaderManager> shaderManager);
	void setLevelManager(std::shared_ptr<LevelManager> levelManager);
	void setRenderManager(std::shared_ptr<RenderManager> renderManager);
};

