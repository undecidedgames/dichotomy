#pragma once

#include "AbstractScreen.h"

class ArtistTestScreen : public AbstractScreen 
{
public:
	ArtistTestScreen(std::shared_ptr<ServiceManager> serviceManager, std::shared_ptr<ConfigManager> configManager, std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<RenderManager> renderManager, std::shared_ptr<LevelManager> levelManager, std::shared_ptr<AudioManager> audioManager, std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<CollisionManager> collisionManager, std::shared_ptr<UIManager> UIManager);
	~ArtistTestScreen();

	void init();
	void render();

	void update();

private:
	GLuint visibleBuffer;

	bool quality;
	bool m_wireframe;
	bool freeCam;

	GLfloat exposure;
};


