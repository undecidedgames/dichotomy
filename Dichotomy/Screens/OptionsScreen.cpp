#include "OptionsScreen.h"


OptionsScreen::OptionsScreen(std::shared_ptr<ServiceManager> serviceManager, std::shared_ptr<ConfigManager> configManager, std::shared_ptr<RenderManager> renderManager, std::shared_ptr<UIManager> UIManager)
{
	m_serviceManager = serviceManager;
	m_configManager = configManager;
	m_renderManager = renderManager;
	m_UIManager = UIManager;
}


OptionsScreen::~OptionsScreen(void)
{
}


void OptionsScreen::init()
{
}


void OptionsScreen::render()
{
		// draw GUI followed by g-buffer layer and cubemap.
	glViewport(0,0,
				m_configManager->m_propertyTree.get<int>("window.width"), 
				m_configManager->m_propertyTree.get<int>("window.height"));
	glBindFramebuffer(GL_FRAMEBUFFER,0);
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_BLEND);
	glDepthMask(false);

	m_renderManager->renderUIElement(m_UIManager->getForm("OptionsScreen")->getElement("ScreenTitle"), "UIRender");
	m_renderManager->renderUIElement(m_UIManager->getForm("OptionsScreen")->getElement("Back"), "UIRender");
	m_renderManager->renderUIElement(m_UIManager->getForm("GraphicsSettings")->getElement("VSync"), "UIRender");
	m_renderManager->renderUIElement(m_UIManager->getForm("GraphicsSettings")->getElement("VSyncVal"), "UIRender");
}


void OptionsScreen::update()
{
	std::shared_ptr<Input> inputService = m_serviceManager->getInput();

	if (m_UIManager->getForm("OptionsScreen")->getElement("Back")->getMouseOver() == true)
	{
		m_UIManager->getForm("OptionsScreen")->getElement("Back")->setTextColour(255, 100, 255);
		if (inputService->mouseButtonState(1) == true)
		{
			setScreenChangeReady(true);
			setScreenChange("startScreen");
		}
	}
	else 
	{
		m_UIManager->getForm("OptionsScreen")->getElement("Back")->setTextColour(255, 255, 255);
	}

	if (m_UIManager->getForm("GraphicsSettings")->getElement("VSync")->getMouseOver() == true)
	{
		m_UIManager->getForm("GraphicsSettings")->getElement("VSync")->setTextColour(255, 100, 255);
		if (inputService->onMouseButtonDown(1) == true)
		{
			setVSync();
			std::string blah = std::to_string(m_VSync);
			m_UIManager->getForm("GraphicsSettings")->getElement("VSyncVal")->setText(blah.c_str());
		}
	}
	else 
	{
		m_UIManager->getForm("GraphicsSettings")->getElement("VSync")->setTextColour(255, 255, 255);
	}
}


void OptionsScreen::setShaderManager(std::shared_ptr<ShaderManager> shaderManager)
{
	m_shaderManager = shaderManager;
}


void OptionsScreen::setLevelManager(std::shared_ptr<LevelManager> levelManager)
{
	m_levelManager = levelManager;
}


void OptionsScreen::setRenderManager(std::shared_ptr<RenderManager> renderManager)
{
	m_renderManager = renderManager;
}