#pragma once

#include <memory>

#include <SDL.h>

#include "../Service/ServiceManager.h"
#include "../Input/SDLInput.h"
#include "../Config/ConfigManager.h"
#include "../Screens/ScreenManager.h"
#include "../Shaders/ShaderManager.h"
#include "../Renderer/RenderManager.h"
#include "../Levels/LevelManager.h"
#include "../Timer/GameTimer.h"
#include "../Camera/CameraManager.h"
#include "../Audio/AudioManager.h"
#include "../Networking/NetworkManager.h"
#include "../Collisions/CollisionManager.h"
#include "../UI/UIManager.h"

using namespace std;

class Application
{
public:
	/* Class Constructor & Destructor */
	Application();
	~Application();

	/* General public methods */
	void init();
	bool gameLoop();
	
private:
	SDL_Event m_SDLEvent;
	
	/* Manager Classes */
	std::shared_ptr<ServiceManager> m_serviceManager;
	std::shared_ptr<ConfigManager> m_configManager;
	std::shared_ptr<NetworkManager> m_networkManager;
	std::shared_ptr<AudioManager> m_audioManager;
	std::shared_ptr<LevelManager> m_levelManager;
	std::shared_ptr<CollisionManager> m_collisionManager;
	std::shared_ptr<CameraManager> m_cameraManager;
	std::shared_ptr<UIManager> m_UIManager;
	std::unique_ptr<ScreenManager> m_screenManager;
	std::shared_ptr<ShaderManager> m_shaderManager;
	std::shared_ptr<RenderManager> m_renderManager;

	std::shared_ptr<GameTimer> m_timer;

	bool m_running;
};