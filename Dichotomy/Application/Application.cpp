#include "Application.h"


#pragma region Class Constructor & Destructor

Application::Application()
{	
	m_running = true;
}

Application::~Application() 
{
}

#pragma endregion


#pragma region General public methods

void Application::init()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize SDL
		std::cout << "SDL_Init:" << SDL_GetError() << std::endl; 

	/* Create and initialise all manager classes */
	m_serviceManager = std::shared_ptr<ServiceManager>(new ServiceManager());
	std::shared_ptr<SDLInput> inputService = std::shared_ptr<SDLInput>(new SDLInput());
	m_serviceManager->registerService(inputService);


	m_configManager = std::shared_ptr<ConfigManager>(new ConfigManager());

	m_networkManager = std::shared_ptr<NetworkManager>(new NetworkManager(m_configManager->m_propertyTree.get<std::string>("networkManager.hostName"), 
		m_configManager->m_propertyTree.get<unsigned int>("networkManager.port"),
		m_configManager->m_propertyTree.get<unsigned int>("networkManager.bufferSize")));
	m_networkManager->init();
	m_networkManager->connectToServer();

	m_audioManager = std::shared_ptr<AudioManager>(new AudioManager());
	m_levelManager = std::shared_ptr<LevelManager>(new LevelManager(m_configManager, m_audioManager));
	m_collisionManager = std::shared_ptr<CollisionManager>(new CollisionManager());

	glm::vec3 artistCamera = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 gameCamera = glm::vec3(0.0f,-0.6f,-0.3f);

	m_cameraManager = std::shared_ptr<CameraManager>(new CameraManager(glm::vec3(12.0f,15.0f,30.0f), glm::normalize(gameCamera), glm::vec3(0.0f,1.0f,0.0f),
		m_configManager->m_propertyTree.get<float>("CameraManager.fov"),
		m_configManager->m_propertyTree.get<int>("CameraManager.windowWidth"),
		m_configManager->m_propertyTree.get<int>("CameraManager.windowHeight"),
		m_configManager->m_propertyTree.get<float>("CameraManager.nearClip"),
		m_configManager->m_propertyTree.get<float>("CameraManager.farClip")));


	m_timer = std::shared_ptr<GameTimer>(new GameTimer);

	m_screenManager = std::unique_ptr<ScreenManager>(new ScreenManager(m_serviceManager, m_configManager, m_renderManager, m_audioManager, m_cameraManager, m_levelManager, m_collisionManager, m_UIManager, m_networkManager, m_timer));

	m_shaderManager = std::shared_ptr<ShaderManager>(new ShaderManager(m_configManager));

	m_screenManager->setShaderManager(m_shaderManager);

	m_renderManager = std::shared_ptr<RenderManager>(new RenderManager("openGL", m_configManager, m_shaderManager, m_cameraManager));
	m_screenManager->setRenderManager(m_renderManager);

	m_timer->setFramesPerSec(m_configManager->m_propertyTree.get<float>("gameTimer.ticksPerSecond")); // fps

	m_UIManager = std::shared_ptr<UIManager>(new UIManager(m_serviceManager, m_configManager));		
	m_UIManager->init();

	m_screenManager->setUIManager(m_UIManager);
}


bool Application::gameLoop()
{
	m_timer->startTimer();

	// Begin main game loop
	while (m_running) 
	{
		while (SDL_PollEvent(&m_SDLEvent))
		{
			// Poll input manager events
			std::shared_ptr<Input> inputService = m_serviceManager->getInput();
			inputService->update();

			switch (m_SDLEvent.type)
			{
			case SDL_WINDOWEVENT:
				switch (m_SDLEvent.window.event)
				{
				case SDL_WINDOWEVENT_CLOSE:
					m_running = false;
					break;
				}
				break;
			case SDL_QUIT:
				m_running = false;
				break;
			}
		}

		// Update UI manager, game rendering and logic updating
		if (m_timer->pollTimeStep())
		{
			m_UIManager->update();
			m_screenManager->update();
			m_screenManager->render();
		}
		// Update the network manager
		m_networkManager->update();
	}	
	return m_running;
}

#pragma endregion
