#pragma once

#include <iostream>

#include "AbstractRenderable.h"

using namespace std;

class Weapon :	public AbstractRenderable
{
public:
	Weapon();
	Weapon(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const bool is_sword);
	Weapon(const string& modelPath, const string& texturePath, const GLuint vao, const GLuint indexCount,const GLuint tex0,const GLuint tex1,const GLuint tex2, const glm::vec3& position, const glm::vec3& orientation, bool is_sword);
	~Weapon();

public:
	bool setProperties(bool is_Sword);

	GLuint getDamage();
	GLfloat getRange();
	GLfloat getSpeed();
	GLfloat getArcLimit();
	
private:
	GLuint m_damage;
	GLfloat m_range;
	GLfloat m_speed;
	GLfloat m_arcLimit;
};

