#pragma once

#include "../AbstractRenderable.h"
#include "../../rgbe.h"

using namespace std;

/**
*	A class which contains all needed textures and models for drawing a skybox and for doing skybox based lighting
*	@author B00216209
*/
class Skybox : public AbstractRenderable
{
public:
	/**
	*	Constructor
	*	Sets path variiables and loads the skybox model and textures.
	*	@param modelPath A path to the skybox model
	*	@param texturePath A path to the skybox textures
	*/
	Skybox(string modelPath, string texturePath);

	/**
	*	Destructor
	*	Frees openGL IDs used by the skybox textures.
	*/
	~Skybox();

private:

	/**
	*	Sets up the arrays of paths and calls loadCubeMap to assign the openGL texture ID's to the skybox member variables.
	*	@param path the path to the skybox texture, different suffices are added to this to load the cubemap variants.
	*/
	void loadSkybox(std::string path);

	/**
	*	Loads a cubemap based on a string* of paths.
	*	@param filenames The 6 paths to the faces of a cubemap, should be ordered +x -x +y -y +z -z 
	*	@returns a GLuint ID which identifies an openGL GL_TEXTURE_CUBEMAP id.
	*/
	GLuint loadCubeMap(std::string* filenames);
};

