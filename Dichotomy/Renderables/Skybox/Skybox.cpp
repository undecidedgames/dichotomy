#include "Skybox.h"


Skybox::Skybox(string modelPath, string texturePath)
{
	m_modelPath = modelPath;
	m_texturePath = texturePath;
	m_textureID = 0;
	loadSkybox(m_texturePath);
	m_indexCount = 0;
	m_vertexArrayObject = 0;
	importFiletoAiScene("assets/models/" + m_modelPath, &m_vertexArrayObject, &m_indexCount);
}


Skybox::~Skybox(void)
{	
	GLuint textures[] = {m_textureID};
	glDeleteTextures(1,textures);
}

void Skybox::loadSkybox(std::string path)
{
	std::string fname = "assets/textures/" + path;

	std::string filenames[6];
	filenames[0] = fname + "-POSX.bmp";
	filenames[1] = fname + "-NEGX.bmp";
	filenames[2] = fname + "-POSY.bmp";
	filenames[3] = fname + "-NEGY.bmp";
	filenames[4] = fname + "-POSZ.bmp";
	filenames[5] = fname + "-NEGz.bmp";

	std::string filenames2[6];
	filenames2[0] = fname + "-irradiance-POSX.bmp";
	filenames2[1] = fname + "-irradiance-NEGX.bmp";
	filenames2[2] = fname + "-irradiance-POSY.bmp";
	filenames2[3] = fname + "-irradiance-NEGY.bmp";
	filenames2[4] = fname + "-irradiance-POSZ.bmp";
	filenames2[5] = fname + "-irradiance-NEGZ.bmp";

	std::string filenames3[6];
	filenames3[0] = fname + "-cosine-POSX.bmp";
	filenames3[1] = fname + "-cosine-NEGX.bmp";
	filenames3[2] = fname + "-cosine-POSY.bmp";
	filenames3[3] = fname + "-cosine-NEGY.bmp";
	filenames3[4] = fname + "-cosine-POSZ.bmp";
	filenames3[5] = fname + "-cosine-NEGZ.bmp";
	
	m_textureID = loadCubeMap(filenames);
	m_normalTextureID = loadCubeMap(filenames2);
	m_specularTextureID = loadCubeMap(filenames3);
}

GLuint Skybox::loadCubeMap(std::string* filenames)
{
	GLuint texID;
	glGenTextures(1,&texID);
	glBindTexture(GL_TEXTURE_CUBE_MAP,texID);
		
	SDL_Surface* xPos = SDL_LoadBMP(filenames[0].c_str());
	if (!xPos) {
		std::cout << "Error loading xPos bitmap" << std::endl;
	}
	SDL_Surface* xNeg = SDL_LoadBMP(filenames[1].c_str());
	if (!xPos) {
		std::cout << "Error loading xNeg bitmap" << std::endl;
	}
	SDL_Surface* yPos = SDL_LoadBMP(filenames[2].c_str());
	if (!xPos) {
		std::cout << "Error loading yPos bitmap" << std::endl;
	}
	SDL_Surface* yNeg = SDL_LoadBMP(filenames[3].c_str());
	if (!xPos) {
		std::cout << "Error loading yNeg bitmap" << std::endl;
	}
	SDL_Surface* zPos = SDL_LoadBMP(filenames[4].c_str());
	if (!xPos) {
		std::cout << "Error loading zPos bitmap" << std::endl;
	}
	SDL_Surface* zNeg = SDL_LoadBMP(filenames[5].c_str());
	if (!xPos) {
		std::cout << "Error loading zNeg bitmap" << std::endl;
	}

	SDL_PixelFormat *format = xPos->format;

	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}
		
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, internalFormat, xPos->w, xPos->h, 0, externalFormat, GL_UNSIGNED_BYTE, xPos->pixels);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, internalFormat, xNeg->w, xNeg->h, 0, externalFormat, GL_UNSIGNED_BYTE, xNeg->pixels);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, internalFormat, yPos->w, yPos->h, 0, externalFormat, GL_UNSIGNED_BYTE, yPos->pixels);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, internalFormat, yNeg->w, yNeg->h, 0, externalFormat, GL_UNSIGNED_BYTE, yNeg->pixels);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, internalFormat, zPos->w, zPos->h, 0, externalFormat, GL_UNSIGNED_BYTE, zPos->pixels);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, internalFormat, zNeg->w, zNeg->h, 0, externalFormat, GL_UNSIGNED_BYTE, zNeg->pixels);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		
	SDL_FreeSurface(xPos);  
	SDL_FreeSurface(xNeg);
	SDL_FreeSurface(yPos);  
	SDL_FreeSurface(yNeg);
	SDL_FreeSurface(zPos);  
	SDL_FreeSurface(zNeg);

	return texID;
}

// ---- LNK2013 errors when trying to use these functions staticly, not sure why so putting a copy of them here until I figure it out. ----


/* offsets to red, green, and blue components in a data (float) pixel */
#define RGBE_DATA_RED    0
#define RGBE_DATA_GREEN  1
#define RGBE_DATA_BLUE   2
/* number of floats per pixel */
#define RGBE_DATA_SIZE   3

enum rgbe_error_codes {
  rgbe_read_error,
  rgbe_write_error,
  rgbe_format_error,
  rgbe_memory_error,
};

int rgbe_error(int rgbe_error_code, char *msg)
{
  switch (rgbe_error_code) {
  case rgbe_read_error:
    perror("RGBE read error");
    break;
  case rgbe_write_error:
    perror("RGBE write error");
    break;
  case rgbe_format_error:
    fprintf(stderr,"RGBE bad file format: %s\n",msg);
    break;
  default:
  case rgbe_memory_error:
    fprintf(stderr,"RGBE error: %s\n",msg);
  }
  return RGBE_RETURN_FAILURE;
}

int RGBE_ReadHeader(FILE *fp, int *width, int *height, rgbe_header_info *info)
{
  char buf[128];
  int found_format;
  float tempf;
  int i;

  found_format = 0;
  if (info) {
    info->valid = 0;
    info->programtype[0] = 0;
    info->gamma = info->exposure = 1.0;
  }
  if (fgets(buf,sizeof(buf)/sizeof(buf[0]),fp) == NULL)
    return rgbe_error(rgbe_read_error,NULL);
  if ((buf[0] != '#')||(buf[1] != '?')) {
    /* if you want to require the magic token then uncomment the next line */
    /*return rgbe_error(rgbe_format_error,"bad initial token"); */
  }
  else if (info) {
    info->valid |= RGBE_VALID_PROGRAMTYPE;
    for(i=0;i<sizeof(info->programtype)-1;++i) {
      if ((buf[i+2] == 0) || isspace(buf[i+2]))
	break;
      info->programtype[i] = buf[i+2];
    }
    info->programtype[i] = 0;
    if (fgets(buf,sizeof(buf)/sizeof(buf[0]),fp) == 0)
      return rgbe_error(rgbe_read_error,NULL);
  }
  for(;;) {
    if ((buf[0] == 0)||(buf[0] == '\n'))
      return rgbe_error(rgbe_format_error,"no FORMAT specifier found");
    else if (strcmp(buf,"FORMAT=32-bit_rle_rgbe\n") == 0)
      break;       /* format found so break out of loop */
    else if (info && (sscanf_s(buf,"GAMMA=%g",&tempf) == 1)) {
      info->gamma = tempf;
      info->valid |= RGBE_VALID_GAMMA;
    }
    else if (info && (sscanf_s(buf,"EXPOSURE=%g",&tempf) == 1)) {
      info->exposure = tempf;
      info->valid |= RGBE_VALID_EXPOSURE;
    }
    if (fgets(buf,sizeof(buf)/sizeof(buf[0]),fp) == 0)
      return rgbe_error(rgbe_read_error,NULL);
  }
  if (fgets(buf,sizeof(buf)/sizeof(buf[0]),fp) == 0)
    return rgbe_error(rgbe_read_error,NULL);
  if (strcmp(buf,"\n") != 0)
    return rgbe_error(rgbe_format_error,
		      "missing blank line after FORMAT specifier");
  if (fgets(buf,sizeof(buf)/sizeof(buf[0]),fp) == 0)
    return rgbe_error(rgbe_read_error,NULL);
  if (sscanf_s(buf,"-Y %d +X %d",height,width) < 2)
    return rgbe_error(rgbe_format_error,"missing image size specifier");
  return RGBE_RETURN_SUCCESS;
}

void rgbe2float(float *red, float *green, float *blue, unsigned char rgbe[4])
{
  float f;

  if (rgbe[3]) {   /*nonzero pixel*/
    f = (float)ldexp(1.0,rgbe[3]-(int)(128+8));
    *red = rgbe[0] * f;
    *green = rgbe[1] * f;
    *blue = rgbe[2] * f;
  }
  else
    *red = *green = *blue = 0.0;
}

int RGBE_ReadPixels(FILE *fp, float *data, int numpixels)
{
  unsigned char rgbe[4];

  while(numpixels-- > 0) {
    if (fread(rgbe, sizeof(rgbe), 1, fp) < 1)
      return rgbe_error(rgbe_read_error,NULL);
    rgbe2float(&data[RGBE_DATA_RED],&data[RGBE_DATA_GREEN],
	       &data[RGBE_DATA_BLUE],rgbe);
    data += RGBE_DATA_SIZE;
  }
  return RGBE_RETURN_SUCCESS;
}

int RGBE_ReadPixels_RLE(FILE *fp, float *data, int scanline_width, int num_scanlines)
{
  unsigned char rgbe[4], *scanline_buffer, *ptr, *ptr_end;
  int i, count;
  unsigned char buf[2];

  if ((scanline_width < 8)||(scanline_width > 0x7fff))
    /* run length encoding is not allowed so read flat*/
    return RGBE_ReadPixels(fp,data,scanline_width*num_scanlines);
  scanline_buffer = NULL;
  /* read in each successive scanline */
  while(num_scanlines > 0) {
    if (fread(rgbe,sizeof(rgbe),1,fp) < 1) {
      free(scanline_buffer);
      return rgbe_error(rgbe_read_error,NULL);
    }
    if ((rgbe[0] != 2)||(rgbe[1] != 2)||(rgbe[2] & 0x80)) {
      /* this file is not run length encoded */
      rgbe2float(&data[0],&data[1],&data[2],rgbe);
      data += RGBE_DATA_SIZE;
      free(scanline_buffer);
      return RGBE_ReadPixels(fp,data,scanline_width*num_scanlines-1);
    }
    if ((((int)rgbe[2])<<8 | rgbe[3]) != scanline_width) {
      free(scanline_buffer);
      return rgbe_error(rgbe_format_error,"wrong scanline width");
    }
    if (scanline_buffer == NULL)
      scanline_buffer = (unsigned char *)
	malloc(sizeof(unsigned char)*4*scanline_width);
    if (scanline_buffer == NULL) 
      return rgbe_error(rgbe_memory_error,"unable to allocate buffer space");
    
    ptr = &scanline_buffer[0];
    /* read each of the four channels for the scanline into the buffer */
    for(i=0;i<4;++i) {
      ptr_end = &scanline_buffer[(i+1)*scanline_width];
      while(ptr < ptr_end) {
	if (fread(buf,sizeof(buf[0])*2,1,fp) < 1) {
	  free(scanline_buffer);
	  return rgbe_error(rgbe_read_error,NULL);
	}
	if (buf[0] > 128) {
	  /* a run of the same value */
	  count = buf[0]-128;
	  if ((count == 0)||(count > ptr_end - ptr)) {
	    free(scanline_buffer);
	    return rgbe_error(rgbe_format_error,"bad scanline data");
	  }
	  while(count-- > 0)
	    *ptr++ = buf[1];
	}
	else {
	  /* a non-run */
	  count = buf[0];
	  if ((count == 0)||(count > ptr_end - ptr)) {
	    free(scanline_buffer);
	    return rgbe_error(rgbe_format_error,"bad scanline data");
	  }
	  *ptr++ = buf[1];
	  if (--count > 0) {
	    if (fread(ptr,sizeof(*ptr)*count,1,fp) < 1) {
	      free(scanline_buffer);
	      return rgbe_error(rgbe_read_error,NULL);
	    }
	    ptr += count;
	  }
	}
      }
    }
    /* now convert data from buffer into floats */
    for(i=0;i<scanline_width;++i) {
      rgbe[0] = scanline_buffer[i];
      rgbe[1] = scanline_buffer[i+scanline_width];
      rgbe[2] = scanline_buffer[i+2*scanline_width];
      rgbe[3] = scanline_buffer[i+3*scanline_width];
      rgbe2float(&data[RGBE_DATA_RED],&data[RGBE_DATA_GREEN],
		 &data[RGBE_DATA_BLUE],rgbe);
      data += RGBE_DATA_SIZE;
    }
    num_scanlines--;
  }
  free(scanline_buffer);
  return RGBE_RETURN_SUCCESS;
}
