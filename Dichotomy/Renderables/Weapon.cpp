#include "Weapon.h"


Weapon::Weapon()
{
}


Weapon::Weapon(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, bool is_sword)
{
	m_isOccluded = false;
	m_position = position;
	m_orientation = orientation;
	m_velocity = glm::vec3(0.0f);
	m_modelPath = modelPath;
	m_texturePath = texturePath;

	// different attributes for sword and mace
	if (is_sword == true)
	{
		m_damage = 150;
		m_range = 2.0f;
		m_speed = 1.0f; // not sure if speed is time taken or what?!?
		m_arcLimit = 0.1f;
	}
	else
	{
		m_damage = 250;
		m_range = 2.5f;
		m_speed = 1.5;
		m_arcLimit = 0.4f;
	}

	m_textureID = loadImage(m_texturePath + ".bmp");
	m_normalTextureID = loadImage(m_texturePath + "-Normals.bmp");
	m_specularTextureID = loadImage(m_texturePath + "-Specular.bmp");

	m_textureID2 = m_textureID;
	m_normalTextureID2 = m_normalTextureID;
	m_specularTextureID2 = m_specularTextureID;

	m_indexCount = 0;
	m_indexCount2 = 0;
	m_vertexArrayObject = 0;
	m_vertexArrayObject2 = 0;

	importFiletoAiScene("assets/models/" + m_modelPath, &m_vertexArrayObject, &m_indexCount);
	m_vertexArrayObject2 = m_vertexArrayObject;
	m_indexCount2 = m_indexCount;
	updateModelMatrix();
}

Weapon::Weapon(const string& modelPath, const string& texturePath, const GLuint vao, const GLuint indexCount,const GLuint tex0,const GLuint tex1,const GLuint tex2, const glm::vec3& position, const glm::vec3& orientation, bool is_sword)
{
	m_isOccluded = false;
	m_position = position;
	m_orientation = orientation;
	m_velocity = glm::vec3(0.0f);
	m_modelPath = modelPath;
	m_texturePath = texturePath;

	// different attributes for sword and mace
	if (is_sword == true)
	{
		m_damage = 150;
		m_range = 2.0f;
		m_speed = 1.0f; // not sure if speed is time taken or what?!?
		m_arcLimit = 0.1f;
	}
	else
	{
		m_damage = 250;
		m_range = 2.5f;
		m_speed = 1.5;
		m_arcLimit = 0.4f;
	}

	m_textureID = tex0;
	m_normalTextureID = tex1;
	m_specularTextureID = tex2;

	m_textureID2 = m_textureID;
	m_normalTextureID2 = m_normalTextureID;
	m_specularTextureID2 = m_specularTextureID;

	m_indexCount = indexCount;
	m_indexCount2 = indexCount;
	m_vertexArrayObject = vao;
	m_vertexArrayObject2 = vao;

	updateModelMatrix();
}


Weapon::~Weapon()
{
}

bool Weapon::setProperties(bool is_sword)
{
	//change model and texture depending on if
	// Sets properties of selected weapon and return true successful
	if(is_sword == true)
	{
		m_damage = 150;
		m_range = 2.0f;
		m_speed = 1.0f; // not sure if speed is time taken or what?!?
	}
	else
	{
		m_damage = 250;
		m_range = 2.5f;
		m_speed = 1.5;
	}

	if(m_damage >= 0 && m_range >= 0 && m_speed >= 0)
		return true;

	return false;
}


GLuint Weapon::getDamage()
{
	// Returns damage of selected weapon
	return rand()%(60)+m_damage;
}
	

GLfloat Weapon::getRange()
{
	// Returns range of selected weapon
	return m_range;
}
	

GLfloat Weapon::getSpeed()
{
	// Returns speed of selected weapon
	return m_speed;
}

GLfloat Weapon::getArcLimit()
{
	return m_arcLimit;
}