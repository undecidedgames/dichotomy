#pragma once

#include <memory>

#include "AbstractActor.h"
#include "ActorState.h"

class NPCActor : public AbstractActor
{
public:
	// Constuctors and Deconstructor
	NPCActor(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const GLuint tickRate, const GLuint phase);
	// Added inputs are texture ID as texture has already been loaded
	NPCActor(const string& modelPath, const string& texturePath, const GLuint VAO, const GLuint VAO2, const GLuint indexCount, const GLuint indexCount2, const GLuint texture0, const GLuint texture1, const GLuint texture2, const GLuint texture3, const GLuint texture4, const GLuint texture5, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const GLuint tickRate, const GLuint phase, const GLuint wVAO, const GLuint wIC, const GLuint wTex0, const GLuint wTex1,const GLuint wTex2);
	~NPCActor(void);

public:
	// Set up ActorStates object
	void initState(std::shared_ptr<NPCActor> actor, int tickRate);

	// Run ActorState update
	bool update(std::shared_ptr<AbstractActor> player1, std::shared_ptr<AbstractActor> player2);
	
	// Actor has been hit by weapon reduce health by weapon damage
	void onHit(int weaponDamage,  std::shared_ptr<AudioManager> audioManager);

	// Get for m_state
	std::shared_ptr<ActorState> getStatePointer();

private:
	// ActorState object
	std::shared_ptr<ActorState> m_state;
};

