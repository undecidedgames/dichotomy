#pragma once

#include <vector>

#include "AbstractActor.h"
#include "../../Audio/AudioManager.h"
#include "../../Pathfinding.h"

class ActorState
{
public:
	// Constructor and Deconstructor
	ActorState(std::shared_ptr<AbstractActor> actor, GLuint tickRate);
	~ActorState(void);

public:
	// State Functions
	bool idle();	// Depending on distance either stays in idle state or changes to move state or attack state
	bool move();	// Depending on distance either stays in move state or changes to move attack or idle state
	bool attack();	// Depending on distance either stays in attack state or changes to move state or idle state
	bool death();	// Actor is "dead" and moved off map (possibly for object pooling)

	// Gets and Sets for m_target depending on type of NPC
	std::shared_ptr<AbstractActor> getTarget();
	std::shared_ptr<AbstractActor> ActorState::setTarget(std::shared_ptr<AbstractActor> actor);
	std::shared_ptr<AbstractActor> setTarget(std::shared_ptr<AbstractActor> player1, std::shared_ptr<AbstractActor> player2);	
	std::shared_ptr<AbstractActor> setTarget(std::vector <std::shared_ptr<AbstractActor>> actorsVector);

	// Get for m_state
	int getState();

	// Finds nearest target and sets m_target to nearest then runs the state function that m_state is in
	bool update(std::shared_ptr<AbstractActor> player1, std::shared_ptr<AbstractActor> player2);

private:
	std::shared_ptr<AbstractActor> m_target;	// NPC's target

	std::shared_ptr<AbstractActor> m_owner;		// NPC that owns this actor state

	std::shared_ptr<Pathfinding> m_pathFinder;	//  A* Pathfinding

	int m_state;	// Current state
	
	GLuint m_tickrate;

	float distance;	// Distance between Actor and target

	std::vector< glm::vec3 > m_path;

	bool m_isInSight;

};

