#include "ActorState.h"


ActorState::ActorState(std::shared_ptr<AbstractActor> actor, GLuint tickRate)
{
	m_owner = actor;
	m_tickrate = tickRate;
	m_state = 1;
	m_pathFinder = std::shared_ptr<Pathfinding>(new Pathfinding());
	//m_path.resize(1);
}


ActorState::~ActorState(void)
{
	m_owner = NULL;
}


bool ActorState::idle()
{
	// Run idle animation
	// If player is in sight and not in attack range go to move
	// If player is in sight and in attack range go to attack
	if( distance <= m_owner->getSight() )	// If in sight
	{
		if( distance <= m_owner->getWeapon().getRange() )	// If in attack range
		{
			m_state = 3;	// attack
		}
		else
		{
			m_isInSight = m_owner->rayCheck(m_target->getPosition());
			m_state = 2;	// move
		}
	}
	else
		m_state = 1;	// idle

	return true;
}


bool ActorState::move()
{
	bool pathFound = false;
	float dx;	// Difference between owner position and path position
	float dz;	// Difference between owner position and path position

	glm::vec3 movementVector;

	if(m_isInSight == true)
	{
		m_path.clear();

		movementVector = m_target->getPosition() - m_owner->getPosition();	// Calculate the movement vector
		float length = glm::length(movementVector);	// Calculate the length of the vector
		movementVector = movementVector / length;	// Calculat the unit vector
	}
	else
	{
		if(m_path.empty() == true)
		{
			pathFound = false;

			// .at(4) is the center of the 9 surrounding mapNode ie the object position (findPath input params are shared_ptr<MapNode>'s and the entire mapNode array)
			pathFound = m_pathFinder->findPath(m_owner->getMapNodes().at(4), m_target->getMapNodes().at(4), m_owner->getMapNodeGrid());

			if(pathFound == true)
			{
				m_path = m_pathFinder->getPath();
			}
		}
		else
		{
			dx = std::abs( m_owner->getPosition().x - m_path.back().x );
			dz = std::abs( m_owner->getPosition().z - m_path.back().z );

			if(dx < 0.3 && dz < 0.3)	// if owner position is ~ equal to path position
			{
				m_path.pop_back();
			}
		}
		movementVector = m_path.back() - m_owner->getPosition();	// Calculate the movement vector
		float length = glm::length(movementVector);	// Calculate the length of the vector
		movementVector = movementVector / length;	// Calculat the unit vector
	}
	
	m_owner->setVelocity( movementVector * m_owner->getSpeed() * 2.0f / (GLfloat)m_tickrate);

	if( distance <= m_owner->getWeapon().getRange() )	// If in attack range
	{
		m_state = 3;	// attack
	}
	else if( distance <= m_owner->getSight() )	// If in sight
	{
		m_isInSight = m_owner->rayCheck(m_target->getPosition());
		m_state = 2;	// move
	}
	else m_state = 1;	// idle

	return true;
}


bool ActorState::attack()
{
	glm::vec3 movementVector = m_target->getPosition() - m_owner->getPosition();
	movementVector = glm::normalize(movementVector);

	float yRotationRadians = atan2(-movementVector.z,movementVector.x);
	float yRotationDegrees  = glm::degrees(yRotationRadians);

	m_owner->setOrientation(glm::vec3(0.0f, yRotationDegrees+90, 0.0f));

	if( distance <= m_owner->getWeapon().getRange())	// If in attack range
	{
		m_state = 3;	// attack
	}
	else if( distance <= m_owner->getSight() )	// If in sight range
	{
		m_isInSight = m_owner->rayCheck(m_target->getPosition());
		m_state = 2;	// move
	}
	else m_state = 1;	// idle

	return true;
}


bool ActorState::death()
{
	// Play death animation
	// Stop rendering actor
	// Delete actor

	m_owner->setPosition(glm::vec3( -m_owner->getPosition().x, 50, -m_owner->getPosition().z));
	return true;
}


std::shared_ptr<AbstractActor> ActorState::setTarget(std::shared_ptr<AbstractActor> actor)
{
	// Return actor
	return actor;
}


std::shared_ptr<AbstractActor> ActorState::setTarget(std::vector <std::shared_ptr<AbstractActor>> actorsVector)
{
	GLuint i = 0;
	int nearest = 0;
	float nearestActor;
	// Set nearest actor to the first entry in the array
	nearestActor = glm::distance( actorsVector[0]->getPosition(), m_owner->getPosition() );

	// Traverse the array and find the nearest actor and get array entry
	for(i = 1; i < actorsVector.size(); ++i)
	{
		distance = glm::distance( actorsVector[i]->getPosition(), m_owner->getPosition() );
		if(distance < nearestActor)
		{
			nearestActor = distance;
			nearest = i;
		}
	}
	// Return nearest actor
	return actorsVector[nearest];
}


std::shared_ptr<AbstractActor> ActorState::getTarget()
{
	return m_target;
}

std::shared_ptr<AbstractActor> ActorState::setTarget(std::shared_ptr<AbstractActor> player1, std::shared_ptr<AbstractActor> player2)
{
	float player1Distance;
	float player2Distance;
	
	// Calculate the distance between both players
 	player1Distance = glm::distance( player1->getPosition(), m_owner->getPosition() );
	player2Distance = glm::distance( player2->getPosition(), m_owner->getPosition() );

	// Return nearest player
	if(player1Distance <= player2Distance)
	{
		distance = player1Distance;
		return player1;
	}
	else
	{
		distance = player2Distance;
		return player2;
	}
}


int ActorState::getState()
{
	return m_state;
}


bool ActorState::update(std::shared_ptr<AbstractActor> player1, std::shared_ptr<AbstractActor> player2)
{
	bool result = false;
	//m_isInSight = false;

	if(m_owner->getHealth() <= 0)	// If Actor has less than 0 health
	{
		m_state = 0;	// death
	}
	
	m_target = setTarget(player1, player2);	// set target 

	switch(m_state)
	{
	case 0:
		// death
		death();
		result = true;
		break;
	case 1:
		// idle
		idle();
		result = true;
		break;
	case 2:
		// move
		move();
		result = true;
		break;
	case 3:
		// attack
		attack();
		result = true;
		break;
	default:
		// Every other value is an error therefore the function returns false
		result = false;
		cout << "Error:- m_state value out of range: " << m_state << endl;
		break;
	}

	return result;
}