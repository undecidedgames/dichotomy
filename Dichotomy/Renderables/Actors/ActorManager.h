#pragma once

#include <vector>
#include <glm\glm.hpp>
#include <memory>
#include <string>

#include "AbstractActor.h"
#include "PlayerActor.h"
#include "NPCActor.h"
#include "../../Audio/AudioManager.h"
#include "../../Timer/GameTimer.h"

class ActorManager
{
public:
	// Constructor and Deconstructor
	ActorManager(std::shared_ptr<AudioManager> audioManager);
	~ActorManager(void);

public:
	// Get and Set for player
	// playerID determins which player is returned
	std::shared_ptr<PlayerActor> getPlayer(unsigned int playerID);
	void setPlayer(std::shared_ptr<PlayerActor> actor, bool isPlayer1);

	// Checks m_NPCActorList if the actorID matches any Actor IDs
	bool actorExists(unsigned int actorID);

	// Get for Actor at actorID in the vector
	std::shared_ptr<NPCActor> getActor(unsigned int actorID);

	// Calls NPCActors constructors using them depends on if the textures have already been loaded
	void createNPCActor(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& halfBounds, const GLuint tickRate, const GLuint phase);
	
	// Adds an actor to the vector
	void addActor(std::shared_ptr<NPCActor> actor);

	// Get for m_NPCActorList
	std::shared_ptr<std::vector<std::shared_ptr<NPCActor>>> getActorVector();

	// Updates Players healths to match, checks attack cooldowns for Player andN PCActor
	// and plays audio depending on NPCActor states
	bool update();

private:
	std::shared_ptr<PlayerActor> m_player1;
	std::shared_ptr<PlayerActor> m_player2;

	GameTimer m_timer;

	std::shared_ptr<std::vector<std::shared_ptr<NPCActor>>> m_NPCActorList;	// Vector of NPCActors

	std::shared_ptr<AudioManager> m_audioManager;
};