#include "NPCActor.h"


NPCActor::NPCActor(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const GLuint tickRate, const GLuint phase)
{
	// Sets this actor as the owner of this ActorState
	m_phase = phase;
	m_parry = false;
	m_isOccluded = false;
	m_cooldown = 3;
	m_position = position;
	m_orientation = orientation;

	m_velocity = glm::vec3(0.0f);

	m_modelPath = modelPath;
	m_texturePath = texturePath;
	m_halfBounds = halfBounds;

	m_textureID = loadImage(m_texturePath + ".bmp");
	m_normalTextureID = loadImage(m_texturePath + "-Normals.bmp");
	m_specularTextureID = loadImage(m_texturePath + "-Specular.bmp");

	m_textureID2 = loadImage(m_texturePath + "-2.bmp");
	m_normalTextureID2 = loadImage(m_texturePath + "-2-Normals.bmp");
	m_specularTextureID2 = loadImage(m_texturePath + "-2-Specular.bmp");

	m_indexCount = 0;
	m_indexCount2 = 0;
	m_vertexArrayObject = 0;
	m_vertexArrayObject2 = 0;

	importFiletoAiScene("assets/models/" + m_modelPath, &m_vertexArrayObject, &m_indexCount);
	importFiletoAiScene("assets/models/alt-" + m_modelPath, &m_vertexArrayObject2, &m_indexCount2);

    m_walkSpeed = 1.6f;	// 1.65?
	m_rotation = 0.0f;
	m_sight = 8.0f;
	m_mass = 100.0f;
	m_health = 1000;

    updateModelMatrix();
	m_weapon = Weapon("Viking_Sword_LP.obj", "Viking_Sword_LP", glm::vec3(0.0, 0.0f, 0.0f), glm::vec3(0.0, 0.0f, 0.0f), true);
}

NPCActor::NPCActor(const string& modelPath, const string& texturePath, const GLuint VAO, const GLuint VAO2, const GLuint indexCount, const GLuint indexCount2, const GLuint texture0, const GLuint texture1, const GLuint texture2, const GLuint texture3, const GLuint texture4, const GLuint texture5, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const GLuint tickRate, const GLuint phase, const GLuint wVAO, const GLuint wIC, const GLuint wTex0, const GLuint wTex1,const GLuint wTex2)
{
	m_phase = phase;
	m_parry = false;
	m_isOccluded = false;
	m_cooldown = 3;
	m_position = position;
	m_orientation = orientation;

	m_velocity = glm::vec3(0.0f);

	m_modelPath = modelPath;
	m_texturePath = texturePath;
	m_halfBounds = halfBounds;

	m_textureID = texture0;
	m_normalTextureID = texture1;
	m_specularTextureID = texture2;

	m_textureID2 = texture3;
	m_normalTextureID2 = texture4;
	m_specularTextureID2 = texture5;

	m_indexCount = indexCount;
	m_indexCount2 = indexCount2;
	m_vertexArrayObject = VAO;
	m_vertexArrayObject2 = VAO2;
	
    m_walkSpeed = 1.6f;	// 1.65?
	m_rotation = 0.0f;
	m_sight = 8.0f;
	m_mass = 100.0f;
	m_health = 100;

    updateModelMatrix();
	m_weapon = Weapon("Viking_Sword_LP.obj", "Viking_Sword_LP", wVAO, wIC, wTex0, wTex1, wTex2, glm::vec3(0.0, 0.0f, 0.0f), glm::vec3(0.0, 0.0f, 0.0f), true);
}

NPCActor::~NPCActor(void)
{
}


void NPCActor::initState(std::shared_ptr<NPCActor> actor, int tickRate)
{
	m_state = std::shared_ptr<ActorState>( new ActorState(actor, tickRate) );
}


bool NPCActor::update(std::shared_ptr<AbstractActor> player1, std::shared_ptr<AbstractActor> player2)
{
	// Runs ActorState update
	
	m_state->update(player1, player2); // state is allowed to set velocity only

	AbstractActor::update(); // the normal abstractActor update will set the orientation, apply friction, update position and update the modelMatrix.

	return true;
}


std::shared_ptr<ActorState> NPCActor::getStatePointer()
{
	return m_state;
}
	

void NPCActor::onHit(int weaponDamage,  std::shared_ptr<AudioManager> audioManager)
{
	if (m_parry == false)
	{
		m_health -= weaponDamage;
		audioManager->playSample("npcPain",0.1f, "2");
	}
	else
	{
		m_health -= (GLuint)floor(weaponDamage*0.2f);
		audioManager->playSample("attack",0.1f,"2");
	}
}