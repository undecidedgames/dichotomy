#include "ActorManager.h"


ActorManager::ActorManager(std::shared_ptr<AudioManager> audioManager)
{
	m_audioManager = audioManager;
	m_NPCActorList = std::shared_ptr<std::vector<std::shared_ptr<NPCActor>>>(new std::vector<std::shared_ptr<NPCActor>>);
	m_timer = GameTimer();
	m_timer.startCooldown();
}


ActorManager::~ActorManager(void)
{
	m_NPCActorList->clear();
}

void ActorManager::createNPCActor(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& halfBounds, const GLuint tickRate, const GLuint phase)
{
	// For each Actor int the list
	bool found = false;
	GLuint foundIndex;
	for(GLuint i = 0; i < m_NPCActorList->size(); ++i)
	{	
		// If the texture path matches the one about to be used to create the new Actor
		if(m_NPCActorList->at(i)->getTexturePath() == texturePath)
		{
			found = true;
			foundIndex = i;
		}
	}
	if (found == false)
	{
		// Create a brand new actor
		std::shared_ptr<NPCActor> temp = std::shared_ptr<NPCActor>(new NPCActor(modelPath, texturePath, position, rotation, halfBounds, tickRate, phase));
		temp->initState(temp, tickRate);
		m_NPCActorList->push_back(temp);
	}
	else
	{
		// Create a new actor with the same texture and model as the existing clone
		GLuint vao = m_NPCActorList->at(foundIndex)->getVAO();
		GLuint vao2 = m_NPCActorList->at(foundIndex)->getVAO2();
		GLuint indexCount = m_NPCActorList->at(foundIndex)->getIndexCount();
		GLuint indexCount2 = m_NPCActorList->at(foundIndex)->getIndexCount2();
		GLuint tex0 = m_NPCActorList->at(foundIndex)->getTexture();
		GLuint tex1 = m_NPCActorList->at(foundIndex)->getNormalTexture();
		GLuint tex2 = m_NPCActorList->at(foundIndex)->getSpecularTexture();
		GLuint tex3 = m_NPCActorList->at(foundIndex)->getTexture2();
		GLuint tex4 = m_NPCActorList->at(foundIndex)->getNormalTexture2();
		GLuint tex5 = m_NPCActorList->at(foundIndex)->getSpecularTexture2();

		GLuint wvao = m_NPCActorList->at(foundIndex)->getWeapon().getVAO();
		GLuint windexCount = m_NPCActorList->at(foundIndex)->getWeapon().getIndexCount();
		GLuint wtex0 = m_NPCActorList->at(foundIndex)->getWeapon().getTexture();
		GLuint wtex1 = m_NPCActorList->at(foundIndex)->getWeapon().getNormalTexture();
		GLuint wtex2 = m_NPCActorList->at(foundIndex)->getWeapon().getSpecularTexture();


		std::shared_ptr<NPCActor> temp = std::shared_ptr<NPCActor>(new NPCActor(modelPath, texturePath, vao, vao2, indexCount, indexCount2, tex0, tex1, tex2, tex3, tex4, tex5, position, rotation, halfBounds, tickRate, phase, wvao, windexCount, wtex0, wtex1, wtex2));
		temp->initState(temp, tickRate);
		m_NPCActorList->push_back(temp);
	}
}

std::shared_ptr<PlayerActor> ActorManager::getPlayer(unsigned int playerID)
{
	// Gets selected using parameter passed in
	if(playerID == 1)
		return m_player1;
	else return m_player2;
}


bool ActorManager::actorExists(unsigned int actorID)
{
	bool exists = false;

	if (actorID < m_NPCActorList->size())
	{
		exists = true;
	}
	return exists;
}


std::shared_ptr<NPCActor> ActorManager::getActor(unsigned int actorID)
{
	return m_NPCActorList->at(actorID);
}


void ActorManager::setPlayer(std::shared_ptr<PlayerActor> actor, bool isPlayer1)
{
	if (isPlayer1)
		m_player1 = actor;
	else
		m_player2 = actor;

}


void ActorManager::addActor(std::shared_ptr<NPCActor> actor)
{
	m_NPCActorList->push_back(actor);
}


std::shared_ptr<std::vector<std::shared_ptr<NPCActor>>> ActorManager::getActorVector()
{
	return m_NPCActorList;
}


bool ActorManager::update()
{
	m_player1->update();
	m_player2->update();	
	int temp = 0;

	//makes sure both players share the same health pool
	if(m_player1->getHealth() < m_player2->getHealth()) m_player2->setHealth(m_player1->getHealth());
	if(m_player2->getHealth() < m_player1->getHealth()) m_player1->setHealth(m_player2->getHealth());

	//if a second has past
	if(m_timer.checkCooldown()== true)
	{
		//loop through every actor taking a second off their cooldown time
		for(size_t i = 0; i < getActorVector()->size(); ++i)
		{
			getActor(i)->setCooldown();
		} 
		m_player1->setCooldown();
		m_player2->setCooldown();
		//then reset timer again
		m_timer.resetCooldownTimer();
	}
	

	
	//if players health <=0 then kill them and game over screen
	if(m_player1->getHealth() <=0) m_player1->dead();
	if(m_player2->getHealth() <=0) m_player2->dead();
	
	for(GLuint i = 0; i < m_NPCActorList->size(); ++i)
	{
		m_NPCActorList->at(i)->update(m_player1, m_player2);

		// if moving or attacking
		if(m_NPCActorList->at(i)->getStatePointer()->getState() == 2 || m_NPCActorList->at(i)->getStatePointer()->getState() == 3)
		{
			// if not already playing
			if(m_audioManager->getPlaying("background") == false)
			{
			// play sample very low so it can fade in
			m_audioManager->playSample("background",0.05f,"1");
					
					
			}
			// fade upwards
			m_audioManager->setSampleFade("background",2);
					
		}
		else
		{
			//if not in moving or attacking add 1 to temp
			temp++;
		}
	}
	// if all actors are not moving or attacking then fade downwards
	if (temp == m_NPCActorList->size()) m_audioManager->setSampleFade("background",1);
	return true;
}
