#include "AbstractActor.h"

AbstractActor::AbstractActor()
{
}

Weapon& AbstractActor::getWeapon()
{
	// Returns selected weapon for chosen actor
	return m_weapon; 
}

int AbstractActor::getHealth()
{
	// Returns health for selected actor
	return m_health;
}


float AbstractActor::getSpeed()
{
	// Gets movement speed for selected actor
	return m_walkSpeed;
}


float AbstractActor::getRotation()
{
	// Gets actors current rotation in degrees
	return m_rotation;
}


float AbstractActor::getSight()
{
	// Gets actor sight range
	return m_sight;
}


bool AbstractActor::setHealth(int health)
{
	// Sets health for selected actor
	if (m_health >= 0)
	{
		m_health = health;
		return true;
	}
	return false;
}

bool AbstractActor::setCooldown()
{
	m_cooldown = m_cooldown--;
	return true;
}
bool AbstractActor::resetCooldown()
{
	m_cooldown = (float)(rand()%(1)+2);
	return true;
}

float AbstractActor::getCooldown()
{
	return m_cooldown;
}

bool AbstractActor::getParry()
{
	return m_parry;
}

bool AbstractActor::setParry(bool parry)
{
	m_parry = parry;
	return true;
}

bool AbstractActor::setSpeed(float speed)
{
	// Sets movement speed for selected actor
	if (m_walkSpeed >= 0 && m_walkSpeed < 10000.0f)
	{
		m_walkSpeed = speed;
		return true;
	}
	return false;
}


bool AbstractActor::update()
{
	if(glm::length(m_velocity) > (0.05f * m_walkSpeed * 1/60.0f)) // if velocity is greater than 1/20 of the walkspeed (tickrate adjusted), remove the speed, else stop the character(to avoid negative speed)
	{
		float magnitude = glm::length(m_velocity);
		glm::vec3 forward = glm::normalize(m_velocity);
		magnitude -= 0.05f * m_walkSpeed * 1/60.0f;
		m_forward = forward;
		
		m_velocity = magnitude * forward;
	}
	else
		m_velocity = glm::vec3(0.0f);

	// if velocity is not 0, adjust orientation so that the character faces the new velocity direction.
	

	if (glm::length(m_velocity) != 0.0) // if new velocity is not 0, adjust orientation to match new movement direction
	{
		float yRotationRadians = atan2(-m_velocity.z,m_velocity.x);
		float yRotationDegrees = glm::degrees(yRotationRadians);
		m_orientation = glm::vec3(0.0f,yRotationDegrees+90,0.0f);
	}
		
	m_position = m_position + m_velocity;

	updateModelMatrix();
	updateMapNodes();

	// Update weapon with new actor position, once animations are on this would be the position and orientation of the weapon bone.
	m_weapon.setPosition(m_position);
	m_weapon.setOrientation(m_orientation);
	m_weapon.updateModelMatrix();
	
	return true;
}

glm::vec3 AbstractActor::getForward()
{
	return m_forward;
}

void AbstractActor::addMapNode(std::shared_ptr<MapNode> node)
{
	m_mapNodes.push_back(node);
}

void AbstractActor::clearMapNodes()
{
	m_mapNodes.clear();
}

void AbstractActor::updateMapNodes()
{
	//Get it's parameters for computing the nodes it should be in
	glm::vec2 position = glm::vec2(m_position.x,m_position.z);
	unsigned int radius = 1 + (m_halfBounds.x / 1);
	glm::vec2 startNode = glm::floor(position);
						
	// Remove this actor from each of it's old nodes
	for(size_t i = 0; i < m_mapNodes.size() ; ++i)
	{
		m_mapNodes[i]->removeActor(shared_from_this());
	}
	// Clear the vector of old nodes
	m_mapNodes.clear();

	// Add updated nodes to vector and tell each node that this actor is now on it.
	for(size_t column = startNode.x - radius; column <= (startNode.x + radius) && column < m_mapNodeGrid->size() && column >= 0; ++column)
	{
		for(size_t row = startNode.y - radius; row <= (startNode.y + radius) && row < m_mapNodeGrid->at(column).size() && row >= 0; ++row)
		{
			if(m_mapNodeGrid->at(column).at(row) == nullptr) 
			{
				std::shared_ptr<MapNode> tempNode = std::shared_ptr<MapNode>(new MapNode(column,row, 1));
				m_mapNodeGrid->at(column).at(row) = tempNode;
				tempNode->addActor(shared_from_this());
				addMapNode(tempNode);
			}
			else
			{
				m_mapNodeGrid->at(column).at(row)->addActor(shared_from_this());
				addMapNode(m_mapNodeGrid->at(column).at(row));
			}
		}
	}
}

vector<std::shared_ptr<MapNode>> AbstractActor::getMapNodes()
{
	return m_mapNodes;
}

std::shared_ptr<vector<vector<std::shared_ptr<MapNode>>>> AbstractActor::getMapNodeGrid()
{
	return m_mapNodeGrid;
}

void AbstractActor::setMapNodeGridPointer(std::shared_ptr<vector<vector<std::shared_ptr<MapNode>>>> grid)
{
	m_mapNodeGrid = grid;
}

bool AbstractActor::rayCheck(glm::vec3 target)
{
	std::shared_ptr<vector<glm::vec2>> points = BresenhamLine(m_position.x, m_position.z, target.x, target.z);
	for(int i = 0; i < points->size(); ++i)
	{
		if (m_mapNodeGrid->at(points->at(i).x).at(points->at(i).y)->getAccessability() == false) return false;
	}
	return true;
}

// Based on http://rosettacode.org/wiki/Bitmap/Bresenham's_line_algorithm#C.2B.2B
std::shared_ptr<vector<glm::vec2>> AbstractActor::BresenhamLine(const float tempx1, const float tempy1, const float tempx2, const float tempy2) 
{
    vector<glm::vec2> points;
	
	float x1 = tempx1;
	float y1 = tempy1;
	float x2 = tempx2;
	float y2 = tempy2;

	const bool steep = (fabs(y2 - y1) > fabs(x2 - x1));
	
	if(steep)
	{
		std::swap(x1, y1);
		std::swap(x2, y2);
	}
 
	if(x1 > x2)
	{
		std::swap(x1, x2);
		std::swap(y1, y2);
	}
 
	const float dx = x2 - x1;
	const float dy = fabs(y2 - y1);
 
	float error = dx / 2.0f;
	const int ystep = (y1 < y2) ? 1 : -1;
	int y = (int)y1;
 
	const int maxX = (int)x2;
 
	for(int x=(int)x1; x<maxX; x++)
	{
		if(steep)
		{
			points.push_back(glm::vec2(y,x));
		}
		else
		{
			points.push_back(glm::vec2(x,y));
		}
 
		error -= dy;
		if(error < 0)
		{
			y += ystep;
			error += dx;
		}
	}

	return std::make_shared<vector<glm::vec2>>(points);
}