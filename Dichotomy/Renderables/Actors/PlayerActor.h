#pragma once

#include <memory>
#include <utility>

#include "AbstractActor.h"
#include "../../Audio/AudioManager.h"

class PlayerActor :	public AbstractActor
{
public:
	// Constructor and Deconstructor
	PlayerActor(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const GLuint phase, const bool isPlayer1);
	~PlayerActor();

public:
	// Currently does nothing
	void attack();

	// Player is hit by a weapon subtract the players health by the weapon damage
	void onHit(int weaponDamage,  std::shared_ptr<AudioManager> audioManager);

	// Player has died
	void dead();

	// Moves player based on input from controller analog stick
	bool move(std::pair<float, float> direction);

	// Resets player cooldown
	bool resetCooldown();

	// Is this Player player1
	bool isPlayer1();
};

