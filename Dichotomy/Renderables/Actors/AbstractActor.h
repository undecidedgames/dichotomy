#pragma once

#include "../../Levels/MapNode.h"
#include "../AbstractRenderable.h"
#include "../Weapon.h"
#include "../../Audio/AudioManager.h"

#define DEG_TO_RAD 0.017453293

class AbstractActor : public AbstractRenderable, public std::enable_shared_from_this<AbstractActor>
{
public: 
	// Default Constuctor and Deconstructor
	AbstractActor();
	~AbstractActor(void) {};

	// Gets a weapon object to store in this Actor
	Weapon& getWeapon();

	// Get and Set m_health
	int getHealth();
	bool setHealth(int health);

	// Get and Set m_speed
	float getSpeed();
	bool setSpeed(float speed);
	
	// Get m_rotation
	float getRotation();

	// Get m_sight
	float getSight();
	
	// Cast a ray from Actor to target and then checks if ray collided with anything
	// inbetween the Actor and target then returns if the ray path was clear(TRUE) or blocked(FALSE)
	bool rayCheck(glm::vec3 target);

	// Get and Set m_cooldown
	float getCooldown();
	bool setCooldown();
	// Resets m_cooldown value
	bool resetCooldown();
	
	// Get and Set m_parry
	bool getParry();
	bool setParry(bool parry);

	bool update();

	// Get m_forward
	glm::vec3 getForward();

	// Adds a mapNode to m_mapNodes
	void addMapNode(std::shared_ptr<MapNode> node);

	// Clears m_mapNodes
	void clearMapNodes();
	
	void updateMapNodes();
	
	// Get m_mapNodes
	vector<std::shared_ptr<MapNode>> getMapNodes();

	// Get pointer to mapNode grid
	std::shared_ptr<vector<vector<std::shared_ptr<MapNode>>>> getMapNodeGrid();
	// Set the pointer to the mapNode grid
	void setMapNodeGridPointer(std::shared_ptr<vector<vector<std::shared_ptr<MapNode>>>> grid);

	// Damages Actor when they are hit by a weapon
	virtual void onHit(int weaponDamage,  std::shared_ptr<AudioManager> audioManager)=0;

protected:
	Weapon m_weapon;	// Weapon object the Actor holds

	glm::vec3 m_attackVector;	// The direction the Actor Attacks

	float m_walkSpeed;
	float m_rotation;
	float m_sight;
	float m_cooldown;

	glm::vec3 m_forward;	// The direction the Actor faces

	int m_health;

	bool m_attacked;	// Has the Actor attacked
	bool m_parry;		// Is the Actor parrying
	bool m_isPlayer1;	// Is this Actor player1

	vector<std::shared_ptr<MapNode>> m_mapNodes;	// Vector of nearby mapNodes

	std::shared_ptr<vector<vector<std::shared_ptr<MapNode>>>> m_mapNodeGrid;	// The mapNode grid

	std::shared_ptr<vector<glm::vec2>> BresenhamLine(const float x1, const float y1, const float x2, const float y2); // Ray casting calculation
};

