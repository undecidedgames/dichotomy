#include "PlayerActor.h"


PlayerActor::PlayerActor(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const GLuint phase, const bool playerID)
{
	m_phase = phase;
	m_parry = false;
	m_isOccluded = false;
	m_cooldown = 0.0f;
	m_position = position;
	m_orientation = orientation;

	m_velocity = glm::vec3(0.0f);

	m_modelPath = modelPath;
	m_texturePath = texturePath;
	m_halfBounds = halfBounds;

	m_textureID = loadImage(m_texturePath + ".bmp");
	m_normalTextureID = loadImage(m_texturePath + "-Normals.bmp");
	m_specularTextureID = loadImage(m_texturePath + "-Specular.bmp");

	m_textureID2 = loadImage(m_texturePath + "-2.bmp");
	m_normalTextureID2 = loadImage(m_texturePath + "-2-Normals.bmp");
	m_specularTextureID2 = loadImage(m_texturePath + "-2-Specular.bmp");

	m_indexCount = 0;
	m_indexCount2 = 0;
	m_vertexArrayObject = 0;
	m_vertexArrayObject2 = 0;

	importFiletoAiScene("assets/models/" + m_modelPath, &m_vertexArrayObject, &m_indexCount);
	importFiletoAiScene("assets/models/alt-" + m_modelPath, &m_vertexArrayObject2, &m_indexCount2);

    m_walkSpeed = 3.2f;
	m_rotation = 0.0f;
	m_sight = 20.0f;
	m_mass = 100.0f;
	m_health = 2000;
	
	m_weapon = Weapon("Viking_Sword_LP.obj", "Viking_Sword_LP", glm::vec3(0.0, 0.0f, 0.0f), glm::vec3(0.0, 0.0f, 0.0f), true);
	updateModelMatrix();
	m_isPlayer1 = playerID;
}


PlayerActor::~PlayerActor(void)
{
	GLuint textures[] = {m_textureID, m_textureID2, m_normalTextureID, m_normalTextureID2, m_specularTextureID, m_specularTextureID2};
	glDeleteTextures(6,textures);
	
	GLuint vaos[] = {m_vertexArrayObject, m_vertexArrayObject2};
	glDeleteVertexArrays(1,vaos);
}


void PlayerActor::attack()
{
	// Sets the bounding spheres for the attack
	// radius of sphere depends on which weapon is used.
	// Runs collision check.(Call to Collision manager)
	// Runs the attack animation.
}


void PlayerActor::dead()
{
	// Changes screen to GameOverScreen
	setPosition(glm::vec3( -m_position.x, 50, -m_position.z));
}


bool PlayerActor::move(std::pair<float, float> direction)
{
	glm::vec3 tempVel = m_velocity;
	setVelocity(glm::vec3( direction.first * m_walkSpeed * 1.0f/60.0f * 2, 0.0f, direction.second * m_walkSpeed * 1.0f/60.0f * 2));
	if (tempVel == m_velocity) return false;

	return true;
}

bool PlayerActor::resetCooldown()
{
	m_cooldown = (float)(rand()%(5)+5);
	m_cooldown = (m_cooldown/10);
	return true;
}


void PlayerActor::onHit(int weaponDamage,  std::shared_ptr<AudioManager> audioManager)
{
	if (m_parry == false)
	{
		m_health -= weaponDamage;
		audioManager->playSample("pain",0.0f,"2");
	}
	else
	{
		m_health -= (GLuint)floor(weaponDamage*0.2f);
		audioManager->playSample("attack",0.0f,"2");
	}
	std::cout << "Health: " << m_health << endl;	
}	

bool PlayerActor::isPlayer1()
{
	return m_isPlayer1;
}