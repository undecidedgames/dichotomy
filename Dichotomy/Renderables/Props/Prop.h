#pragma once

#include "../AbstractRenderable.h"

using namespace std;

class Prop : public AbstractRenderable
{
public:
	Prop();
	Prop(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const GLuint phase);
	~Prop();

public:
	void addProp(const glm::vec3& position, const glm::vec3& orientation);
	void updateTBO();

	GLuint m_modelMatrixTBO;
	GLuint m_propCount;
	GLuint m_drawCount;
	GLuint m_modelMatrixBuffer;
	vector<bool> m_culledList;
	vector<glm::vec3> m_positions;
	vector<glm::vec3> m_orientations;

	GLuint m_positionsCount;
	GLuint m_orientationsCount;
	vector<glm::mat4> modelMatrixBufferData;

};

