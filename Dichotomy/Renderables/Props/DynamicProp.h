#pragma once

#include <memory>

#include "../Actors/PlayerActor.h"
#include "../Actors/NPCActor.h"

#include "prop.h"

class DynamicProp : public Prop
{
public:
	// Constructors and deconstructor
	DynamicProp();
	DynamicProp(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id);
	~DynamicProp();

public:
	virtual void update() = 0;

	// Collision functions - Exectues unique code based on the type of dynamic prop
	virtual void onCollision(std::shared_ptr<NPCActor> npc, const std::shared_ptr<AudioManager> audioManager) = 0;
	virtual void onCollision(std::shared_ptr<PlayerActor> player, const std::shared_ptr<AudioManager> audioManager) = 0;

	// Trigger Functions (alters activation count)
	// Decrement the activation count
	virtual void onTrigger(const std::shared_ptr<AudioManager> audioManager) = 0;
	// Increment the activation count
	virtual void addTrigger() = 0;

	// Get and Set for m_id
	void setID(const int index);
	int getID();

	// Get for m_triggered
	bool getTriggered();

protected:
	int m_activationCount;	// The number of triggers that can affect the object

	int m_id;	// The objects ID that allows triggers to affect them

	bool m_triggered;	// To determine if a dynamic prop has been triggered
						// (its activation count is zero and onTrigger function is called)

	bool m_isToggleable;	// If the dynamic prop only activate/ deactivate or it can switch between
};

