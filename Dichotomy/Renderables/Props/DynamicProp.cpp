#include "DynamicProp.h"

DynamicProp::DynamicProp()
{
	m_activationCount = 0;
}

DynamicProp::DynamicProp(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id)
{
	m_isOccluded = false;
	m_id = id;
	m_isSolid = true;

	m_position = position;
	m_orientation = orientation;
	m_velocity = glm::vec3(0.0f);
	
	m_propCount = 0;

	m_modelPath = modelPath;
	m_texturePath = texturePath;
	m_halfBounds = halfBounds;

	m_textureID = loadImage(m_texturePath + ".bmp");
	m_normalTextureID = loadImage(m_texturePath + "-Normals.bmp");
	m_specularTextureID = loadImage(m_texturePath + "-Specular.bmp");

	m_textureID2 = loadImage(m_texturePath + "-2.bmp");
	m_normalTextureID2 = loadImage(m_texturePath + "-2-Normals.bmp");
	m_specularTextureID2 = loadImage(m_texturePath + "-2-Specular.bmp");

	m_modelMatrixTBO = NULL;
	glGenTextures(1, &m_modelMatrixTBO);
	m_modelMatrixBuffer = NULL;
	glGenBuffers(1,&m_modelMatrixBuffer);

	m_indexCount = 0;
	m_indexCount2 = 0;
	m_vertexArrayObject = 0;
	m_vertexArrayObject2 = 0;

	importFiletoAiScene("assets/models/" + m_modelPath, &m_vertexArrayObject, &m_indexCount);
	importFiletoAiScene("assets/models/alt-" + m_modelPath, &m_vertexArrayObject2, &m_indexCount2);

	m_positionsCount = 0;
	m_orientationsCount = 0;

	m_isSphereCollider = false;

	m_triggered = false;
}


DynamicProp::~DynamicProp()
{
}


void DynamicProp::setID(const int index)
{
	m_id = index;
}


int DynamicProp::getID()
{
	return m_id;
}

bool DynamicProp::getTriggered()
{
	return m_triggered;
}