#include "Tile.h"


Tile::Tile(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const GLuint phase)
{
	m_phase = phase;
	m_isOccluded = false;
	m_position = position;
	m_orientation = orientation;
	m_velocity = glm::vec3(0.0f);
	m_tileCount = 0;
	m_modelPath = modelPath;
	m_texturePath = texturePath;
	m_halfBounds = halfBounds;
	m_textureID = loadImage(m_texturePath + ".bmp");
	m_normalTextureID = loadImage(m_texturePath + "-Normals.bmp");
	m_specularTextureID = loadImage(m_texturePath + "-Specular.bmp");
	m_textureID2 = loadImage(m_texturePath + "-2.bmp");
	m_normalTextureID2 = loadImage(m_texturePath + "-2-Normals.bmp");
	m_specularTextureID2 = loadImage(m_texturePath + "-2-Specular.bmp");
	m_modelMatrixTBO = NULL;
	glGenTextures(1, &m_modelMatrixTBO);
	m_modelMatrixBuffer = NULL;
	glGenBuffers(1,&m_modelMatrixBuffer);
	
	m_indexCount = 0;
	m_indexCount2 = 0;
	m_vertexArrayObject = 0;
	m_vertexArrayObject2 = 0;

	importFiletoAiScene("assets/models/" + m_modelPath, &m_vertexArrayObject, &m_indexCount);
	importFiletoAiScene("assets/models/alt-" + m_modelPath, &m_vertexArrayObject2, &m_indexCount2);

	m_isSphereCollider = false;
	m_drawCount = 0;
}

Tile::Tile()
{
	m_halfBounds = glm::vec3(0.5f,0.5f,0.5f);
}


Tile::~Tile(void)
{	
	GLuint textures[] = {m_textureID, m_textureID2, m_normalTextureID, m_normalTextureID2, m_specularTextureID, m_specularTextureID2};
	glDeleteTextures(6,textures);
	
	GLuint vaos[] = {m_vertexArrayObject, m_vertexArrayObject2};
	glDeleteVertexArrays(1,vaos);

	glDeleteBuffers(1,&m_modelMatrixBuffer);
}

void Tile::addTile(const glm::vec3& position, const glm::vec3& orientation)
{
	m_positions.push_back(position);
	m_orientations.push_back(orientation);
	m_tileCount++;
	m_culledList.push_back(false);
}


void Tile::updateTBO()
{
	vector<glm::mat4> modelMatrixBufferData;
	modelMatrixBufferData.reserve(m_tileCount);
	m_drawCount = 0;

	for(size_t i=0; i < m_tileCount; ++i)
	{
		if(m_culledList[i] == false)
		{
			glm::mat4 temp(1.0f);
			temp = glm::translate(temp, m_positions[i]);
			temp = glm::rotate(temp, m_orientations[i].x, glm::vec3(1.0f,0.0f,0.0f));
			temp = glm::rotate(temp, m_orientations[i].y, glm::vec3(0.0f,1.0f,0.0f));
			temp = glm::rotate(temp, m_orientations[i].z, glm::vec3(0.0f,0.0f,1.0f));
			modelMatrixBufferData.push_back(temp);
			m_drawCount++;
		}
	}

	modelMatrixBufferData.shrink_to_fit();

	// gen buffer
	glBindBuffer(GL_TEXTURE_BUFFER, m_modelMatrixBuffer);
	
	// fill the buffer with the data
	glBufferData(GL_TEXTURE_BUFFER, (m_drawCount) * sizeof(glm::mat4), modelMatrixBufferData.data(), GL_DYNAMIC_DRAW);
	glBindBuffer(GL_TEXTURE_BUFFER, 0);
	
	// bind TBO
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_BUFFER, m_modelMatrixTBO);
	
	// attach the buffer
	glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, m_modelMatrixBuffer);

	modelMatrixBufferData.clear();
}