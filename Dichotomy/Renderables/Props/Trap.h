#pragma once
#include "dynamicprop.h"

class Trap : public DynamicProp
{
public:
	// Constructors and Destructors
	// Constructor reads these parameters from the config and the bitmap
	// config file - modelPath, texturePath, orientation, halfBounds
	// bitmap file - position, id, phase, continous, active
	Trap(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const bool continuous, const bool active);
	// Constructor same as above but takes in the textures ID (texture has already been loaded in by a previous constructor call)
	Trap(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const bool continuous, const bool active, const GLuint tex0, const GLuint tex1,const GLuint tex2,const GLuint tex3,const GLuint tex4,const GLuint tex5);
	~Trap();

public:
	// Runs animation if the trap is active (animation will cycle if trap is continous)
	void update();

	// Collision functions - Subtracts health from actor that collides with trap (Dependent on the activation count)
	void onCollision(std::shared_ptr<NPCActor> npc, const std::shared_ptr<AudioManager> audioManager);
	void onCollision(std::shared_ptr<PlayerActor> player, const std::shared_ptr<AudioManager> audioManager);

	// Trigger Functions (alters activation count)
	// Decrement the activation count, exectues unique code based on m_continuous and phase when a trigger is activated
	void onTrigger(const std::shared_ptr<AudioManager> audioManager);
	// Increment the activation count
	void addTrigger();

	// Get and Set m_continous
	void setContinuous(const bool continuous);
	bool getContinuous();

	// Get and Set m_isActive
	void setActive(const bool active);
	bool getActive();

	// Get and Set m_damage
	void setDamage(const GLuint damage);
	GLuint getDamage();

private:
	bool m_continuous;	// Sates if a trap will repeat animations in update

	bool m_isActive;	// If trap is activated

	GLuint m_damage; // Damage trap does to actors
};

