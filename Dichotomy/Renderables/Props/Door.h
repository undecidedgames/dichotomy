#pragma once

#include "DynamicProp.h"

class Door : public DynamicProp
{
public:
	// Constructors and Deconstructor
	// Constructor reads these parameters from the config and the bitmap
	//		config file	-	modelPath, texturePath, orientation, halfBounds
	//		bitmap file	-	position, id, phase, toggleable, solid
	Door(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const bool toggleable);
	// Constructor same as above but takes in the textures ID (texture has already been loaded in by a previous constructor call)
	Door(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const bool toggleable, const GLuint tex0, const GLuint tex1,const GLuint tex2,const GLuint tex3,const GLuint tex4,const GLuint tex5);
	~Door();

public:	
	void update();

	// Collision functions - Do nothing as of yet. May added functionality at a later date
	void onCollision(std::shared_ptr<NPCActor> npc, const std::shared_ptr<AudioManager> audioManager);
	void onCollision(std::shared_ptr<PlayerActor> player, const std::shared_ptr<AudioManager> audioManager);

	// Trigger Functions (alters activation count)
	void onTrigger(const std::shared_ptr<AudioManager> audioManager);
	// Increment the activation count
	void addTrigger();

	// Get and Set m_isActive
	void setActive(const bool active);
	bool getActive();

private:
	bool m_isActive;	// Flag to detemine if a door is open or closed
};

