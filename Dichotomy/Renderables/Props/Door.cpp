#include "Door.h"


Door::Door(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const bool toggleable)
{
	m_isOccluded = false;
	m_isSolid = true;
	m_isActive = false;
	m_triggered = false;
	m_phase = phase;
	m_isToggleable = toggleable;
	m_id = id;
	m_position = position;
	m_orientation = orientation;
	m_velocity = glm::vec3(0.0f);
	
	m_propCount = 0;

	m_modelPath = modelPath;
	m_texturePath = texturePath;
	m_halfBounds = halfBounds;

	m_textureID = loadImage(m_texturePath + ".bmp");
	m_normalTextureID = loadImage(m_texturePath + "-Normals.bmp");
	m_specularTextureID = loadImage(m_texturePath + "-Specular.bmp");

	m_textureID2 = loadImage(m_texturePath + "-2.bmp");
	m_normalTextureID2 = loadImage(m_texturePath + "-2-Normals.bmp");
	m_specularTextureID2 = loadImage(m_texturePath + "-2-Specular.bmp");

	updateModelMatrix();

	m_indexCount = 0;
	m_indexCount2 = 0;
	m_vertexArrayObject = 0;
	m_vertexArrayObject2 = 0;

	importFiletoAiScene("assets/models/" + m_modelPath, &m_vertexArrayObject, &m_indexCount);
	importFiletoAiScene("assets/models/alt-" + m_modelPath, &m_vertexArrayObject2, &m_indexCount2);

	m_positionsCount = 0;
	m_orientationsCount = 0;

	m_isSphereCollider = false; 
}

Door::Door(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const bool toggleable, const GLuint tex0, const GLuint tex1,const GLuint tex2,const GLuint tex3,const GLuint tex4,const GLuint tex5)
{
	m_isOccluded = false;
	m_isSolid = true;
	m_isActive = false;
	m_triggered = false;
	m_phase = phase;
	m_isToggleable = toggleable;
	m_id = id;
	m_position = position;
	m_orientation = orientation;
	m_velocity = glm::vec3(0.0f);
	
	m_propCount = 0;

	m_modelPath = modelPath;
	m_texturePath = texturePath;
	m_halfBounds = halfBounds;

	m_textureID = tex0;
	m_normalTextureID = tex1;
	m_specularTextureID = tex2;

	m_textureID2 = tex3;
	m_normalTextureID2 = tex4;
	m_specularTextureID2 = tex5;

	updateModelMatrix();

	m_indexCount = 0;
	m_indexCount2 = 0;
	m_vertexArrayObject = 0;
	m_vertexArrayObject2 = 0;

	importFiletoAiScene("assets/models/" + m_modelPath, &m_vertexArrayObject, &m_indexCount);
	importFiletoAiScene("assets/models/alt-" + m_modelPath, &m_vertexArrayObject2, &m_indexCount2);

	m_positionsCount = 0;
	m_orientationsCount = 0;

	m_isSphereCollider = false; 
}

Door::~Door()
{
}


void Door::update()
{
}


void Door::onCollision(std::shared_ptr<NPCActor> npc, const std::shared_ptr<AudioManager> audioManager)
{
	// Now Defunct
}


void Door::onCollision(std::shared_ptr<PlayerActor> player, const std::shared_ptr<AudioManager> audioManager)
{
	// Now Defunct
}


void Door::onTrigger(const std::shared_ptr<AudioManager> audioManager)
{
	// If activation count  is less than or equal to zero
	// depending on phase either switch phase or open
	m_activationCount--;
	
	if( m_activationCount <= 0)
	{
		if(m_phase == 2)
		{
			m_triggered = true;
			m_isActive = true;
			m_isSolid = false;
			m_position += glm::vec3(0.0,5.0f,0.0f);	// Placeholder for animations
			updateModelMatrix();
			audioManager->playSample("doorOpen",1.0f,"2");
		}
		else
		{
			if(m_phase == 0)
			{
				m_phase = 1;
			}
			else
			{
				m_phase = 0;
			}
		}
	}
}


void Door::addTrigger()
{
	m_activationCount++;
}


void Door::setActive(const bool active)
{
	m_isActive = active;
}


bool Door::getActive()
{
	return m_isActive;
}