#pragma once

#include "../AbstractRenderable.h"

using namespace std;

class Tile : public AbstractRenderable
{
public:
	Tile(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const GLuint phase);
	Tile();
	~Tile();

public:
	void addTile(const glm::vec3& position, const glm::vec3& orientation);
	void updateTBO();
	GLuint m_drawCount;
	GLuint m_modelMatrixTBO;
	GLuint m_tileCount;
	GLuint m_modelMatrixBuffer;
	vector<glm::vec3> m_positions;
	vector<glm::vec3> m_orientations;
	vector<bool> m_culledList;
	GLuint m_positionsCount;
	GLuint m_orientationsCount;
	vector<glm::mat4> modelMatrixBufferData;
};

