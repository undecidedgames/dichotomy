#include "Trigger.h"


Trigger::Trigger(const string&  modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const int targetID, const bool toggleable, const bool solid)
{
	m_isOccluded = false;
	m_id = id;
	m_targetID = targetID;
	m_isSolid = solid;
	m_phase = phase;
	m_position = position;
	m_orientation = orientation;
	m_velocity = glm::vec3(0.0f);
	m_isToggleable = toggleable;
	m_triggered = false;

	m_propCount = 0;

	m_modelPath = modelPath;
	m_texturePath = texturePath;
	m_halfBounds = halfBounds;

	m_textureID = loadImage(m_texturePath + ".bmp");
	m_normalTextureID = loadImage(m_texturePath + "-Normals.bmp");
	m_specularTextureID = loadImage(m_texturePath + "-Specular.bmp");
	m_textureID2 = loadImage(m_texturePath + "-2.bmp");
	m_normalTextureID2 = loadImage(m_texturePath + "-2-Normals.bmp");
	m_specularTextureID2 = loadImage(m_texturePath + "-2-Specular.bmp");

	updateModelMatrix();

	m_indexCount = 0;
	m_indexCount2 = 0;
	m_vertexArrayObject = 0;
	m_vertexArrayObject2 = 0;

	importFiletoAiScene("assets/models/" + m_modelPath, &m_vertexArrayObject, &m_indexCount);
	importFiletoAiScene("assets/models/alt-" + m_modelPath, &m_vertexArrayObject2, &m_indexCount2);

	m_positionsCount = 0;
	m_orientationsCount = 0;

	m_isSphereCollider = false;
}

Trigger::Trigger(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const int targetID, const bool toggleable, const bool solid, const GLuint tex0, const GLuint tex1,const GLuint tex2,const GLuint tex3,const GLuint tex4,const GLuint tex5)
{
	m_isOccluded = false;
	m_id = id;
	m_targetID = targetID;
	m_isSolid = solid;
	m_phase = phase;
	m_position = position;
	m_orientation = orientation;
	m_velocity = glm::vec3(0.0f);
	m_isToggleable = toggleable;
	m_triggered = false;

	m_propCount = 0;

	m_modelPath = modelPath;
	m_texturePath = texturePath;
	m_halfBounds = halfBounds;

	m_textureID = tex0;
	m_normalTextureID = tex1;
	m_specularTextureID = tex2;

	m_textureID2 = tex3;
	m_normalTextureID2 = tex4;
	m_specularTextureID2 = tex5;

	updateModelMatrix();

	m_indexCount = 0;
	m_indexCount2 = 0;
	m_vertexArrayObject = 0;
	m_vertexArrayObject2 = 0;

	importFiletoAiScene("assets/models/" + m_modelPath, &m_vertexArrayObject, &m_indexCount);
	importFiletoAiScene("assets/models/alt-" + m_modelPath, &m_vertexArrayObject2, &m_indexCount2);

	m_positionsCount = 0;
	m_orientationsCount = 0;

	m_isSphereCollider = false;
}



Trigger::~Trigger()
{
	m_targets.clear();
}


void Trigger::update()
{
}


void Trigger::onCollision(std::shared_ptr<NPCActor> npc, const std::shared_ptr<AudioManager> audioManager)
{
	// do nothing ?
	// Add if Npc need this behaviour
}


void Trigger::onCollision(std::shared_ptr<PlayerActor> player, const std::shared_ptr<AudioManager> audioManager)
{
	// If activation is zero call onTrigger
	audioManager->playSample("lever",1.0f,"2");	// Play sample
	if(m_activationCount == 0)
	{
		onTrigger(audioManager);
		m_orientation.y += 180;		// Rotate trigger this is a placeholder for animation
		if(m_orientation.y > 360)
		{
			m_orientation.y -= 360;
		}
		updateModelMatrix();
	}		

}


void Trigger::setTargets()
{
	for(std::shared_ptr<DynamicProp> x : m_targets)	// For each target
	{
		x->addTrigger();
	}	
}


void Trigger::addTarget(std::shared_ptr<Trigger> target)
{
	if(target->getTargetID() != m_id)	// If a targets targetID does not this triggers ID
	{
		m_targets.push_back(target);
	}
	else
	{
		// Error Message for debugging
		std::cout << "ERROR: Circle trigger targeting! " << "TriggerIDs: " << m_id << " [" << m_position.x << "," << m_position.z << "] " 
		<< "and " << target->getTargetID() << " [" << target->getPosition().x << "," << target->getPosition().z << "]" << std::endl;
	}
}


void Trigger::addTarget(std::shared_ptr<DynamicProp> target)
{
	m_targets.push_back(target);
}


void Trigger::onTrigger(const std::shared_ptr<AudioManager> audioManager)
{
	if(m_activationCount > 0) // Decrement activation account if it is greater than zero
	{
		m_activationCount--;
	}
	else
	{
		// If the object is toggleable increament or decreament the activation count
		if(m_isToggleable)
		{
			if(!m_triggered)
			{
				m_triggered = true;
				for(std::shared_ptr<DynamicProp> target : m_targets) // For each Target
				{
					target->onTrigger(audioManager);
				}
			}		
			else
			{
				m_triggered = false;
				for(std::shared_ptr<DynamicProp> target : m_targets) // For each Target
				{
					target->addTrigger();
				}
			}
		}
		else
		{
			if(!m_triggered) 
			{
				for(std::shared_ptr<DynamicProp> target : m_targets) // For each Target
				{
					target->onTrigger(audioManager);
				}
				m_triggered = true;
			}
		}
	}
	cout << "Trigger ID: " << m_id << " Target ID: " << m_targetID << "Count: " << m_activationCount << ", player collided" << endl;
}


void Trigger::addTrigger()
{
	m_activationCount++;

}

GLuint Trigger::getTargetID()
{
	return m_targetID;
}
