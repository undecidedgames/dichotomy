#include "Trap.h"


Trap::Trap(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const bool continuous, const bool active)
{
	m_isOccluded = false;
	m_isToggleable = true;
	m_continuous = continuous;
	m_phase = phase;
	m_id = id;
	m_position = position;
	m_orientation = orientation;
	m_velocity = glm::vec3(0.0f);

	m_propCount = 0;

	m_modelPath = modelPath;
	m_texturePath = texturePath;
	m_halfBounds = halfBounds;

	m_textureID = loadImage(m_texturePath + ".bmp");
	m_normalTextureID = loadImage(m_texturePath + "-Normals.bmp");
	m_specularTextureID = loadImage(m_texturePath + "-Specular.bmp");
	m_textureID2 = loadImage(m_texturePath + "-2.bmp");
	m_normalTextureID2 = loadImage(m_texturePath + "-2-Normals.bmp");
	m_specularTextureID2 = loadImage(m_texturePath + "-2-Specular.bmp");;

	updateModelMatrix();

	m_indexCount = 0;
	m_indexCount2 = 0;
	m_vertexArrayObject = 0;
	m_vertexArrayObject2 = 0;

	importFiletoAiScene("assets/models/" + m_modelPath, &m_vertexArrayObject, &m_indexCount);
	importFiletoAiScene("assets/models/alt-" + m_modelPath, &m_vertexArrayObject2, &m_indexCount2);

	m_positionsCount = 0;
	m_orientationsCount = 0;

	m_isSphereCollider = false;

	m_isActive = active;
	m_isSolid = false;

	m_damage = 1;
}

Trap::Trap(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const bool continuous, const bool active, const GLuint tex0, const GLuint tex1,const GLuint tex2,const GLuint tex3,const GLuint tex4,const GLuint tex5)
{
	m_isOccluded = false;
	m_isToggleable = true;
	m_continuous = continuous;
	m_phase = phase;
	m_id = id;
	m_position = position;
	m_orientation = orientation;
	m_velocity = glm::vec3(0.0f);

	m_propCount = 0;

	m_modelPath = modelPath;
	m_texturePath = texturePath;
	m_halfBounds = halfBounds;

	m_textureID = tex0;
	m_normalTextureID = tex1;
	m_specularTextureID = tex2;

	m_textureID2 = tex3;
	m_normalTextureID2 = tex4;
	m_specularTextureID2 = tex5;

	updateModelMatrix();

	m_indexCount = 0;
	m_indexCount2 = 0;
	m_vertexArrayObject = 0;
	m_vertexArrayObject2 = 0;

	importFiletoAiScene("assets/models/" + m_modelPath, &m_vertexArrayObject, &m_indexCount);
	importFiletoAiScene("assets/models/alt-" + m_modelPath, &m_vertexArrayObject2, &m_indexCount2);

	m_positionsCount = 0;
	m_orientationsCount = 0;

	m_isSphereCollider = false;

	m_isActive = active;
	m_isSolid = false;

	m_damage = 1;
}

Trap::~Trap()
{
}


void Trap::update()
{
	// if(m_triggered)
		// cycle animation
	// check if(!m_continous)
		// if at end of animation
			// stop animation
			// m_isActive = false
}


void Trap::onCollision(std::shared_ptr<NPCActor> npc, const std::shared_ptr<AudioManager> audioManager)
{
	// if active do damage to player based on trap damage and phase
	if(m_isActive)
	{
		if(npc->getStatePointer()->getState() != 0)
		{
			npc->setHealth(npc->getHealth() - m_damage);
			cout << "NPC hurt: " << npc->getHealth() << endl;
		}
	}
}


void Trap::onCollision(std::shared_ptr<PlayerActor> player, const std::shared_ptr<AudioManager> audioManager)
{
	// if active do damage to player based on trap damage and phase
	if(m_isActive)
	{
		player->setHealth(player->getHealth() - m_damage);
		cout << "Player hurt: " << player->getHealth() << endl;
	}
}


void Trap::onTrigger(const std::shared_ptr<AudioManager> audioManager)
{
	// Flip active boolean if activation count is less than or equal to zero
	if(m_activationCount <= 0 && m_triggered == false)
	{
		m_triggered = true;
		m_isActive = !m_isActive;
	}
}


void Trap::addTrigger()
{
	m_activationCount++;
}


void Trap::setActive(const bool active)
{
	m_isActive = active;
}


bool Trap::getActive()
{
	return m_isActive;
}


void Trap::setDamage(const GLuint damage)
{
	m_damage = damage;
}


GLuint Trap::getDamage()
{
	return m_damage;
}
