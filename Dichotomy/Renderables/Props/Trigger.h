#pragma once

#include "DynamicProp.h"

class Trigger : public DynamicProp
{
public:
	// Constructors and Deconstructor
	// Constructor reads these parameters from the config and the bitmap
	//		config file	-	modelPath, texturePath, orientation, halfBounds
	//		bitmap file	-	position, id, targetID, phase, toggleable, solid
	Trigger(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const int targetID, const bool toggleable, const bool solid);
	// Constructor same as above but takes in the textures ID (texture has already been loaded in by a previous constructor call)
	Trigger(const string& modelPath, const string& texturePath, const glm::vec3& position, const glm::vec3& orientation, const glm::vec3& halfBounds, const int id, const int phase, const int targetID, const bool toggleable, const bool solid, const GLuint tex0, const GLuint tex1,const GLuint tex2,const GLuint tex3,const GLuint tex4,const GLuint tex5);
	~Trigger();

public:
	void update();

	// Collision functions - Call onTrigger if the activation count is zero
	void onCollision(std::shared_ptr<NPCActor> npc, const std::shared_ptr<AudioManager> audioManager);
	void onCollision(std::shared_ptr<PlayerActor> player, const std::shared_ptr<AudioManager> audioManager);

	// Target setup function
	void setTargets();	// Adds initial activation count to each dynamic prop in m_targets
	void addTarget(std::shared_ptr<DynamicProp> target);
	// Overloaded function to check if triggers create a closed loop (triggers don't target each other)
	void addTarget(std::shared_ptr<Trigger> target);

	// Trigger Functions (alters activation count)
	void onTrigger(const std::shared_ptr<AudioManager> audioManager);
	// Increment the activation count
	void addTrigger();

	// Get for m_targetID
	GLuint getTargetID();

private:
	std::vector <std::shared_ptr<DynamicProp>> m_targets;	// Vector of props (propsID == this TargetID)

	GLuint m_targetID;	// The dynamic props that this trigger affects

};

