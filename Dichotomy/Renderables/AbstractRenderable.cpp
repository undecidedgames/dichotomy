#include "AbstractRenderable.h"

AbstractRenderable::AbstractRenderable()
{
	//std::cout << "AbstractRenderable contructor called" << std::endl;
	m_phase = 2;
}


AbstractRenderable::~AbstractRenderable()
{
	//std::cout << "AbstractRenderable destructor called" << std::endl;
}


void AbstractRenderable::importFiletoAiScene(const std::string& pFile, GLuint* meshID, GLuint* indexCount) // from B00216209's AGP project
{
	// Create an instance of the Importer class
	Assimp::Importer importer;
	// And have it read the given file with some example postprocessing
	// Usually - if speed is not the most important aspect for you - you'll
	// propably to request more postprocessing than we do in this example.
	const aiScene* scene = importer.ReadFile( 
		pFile,
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_ImproveCacheLocality);
	// If the import failed, report it
	if( !scene)
	{
		std::cout << importer.GetErrorString() << std::endl;
	}
	setupVAOfromAiScene(scene,meshID,indexCount);	
	// We're done. Everything will be cleaned up by the importer destructor
}


void AbstractRenderable::setupVAOfromAiScene(const aiScene* aiScene,  GLuint* meshID, GLuint* indexCount) // from B00216209's AGP project
{
	// create aimesh pointer
	aiMesh* mesh = aiScene->mMeshes[0];

	// generate and set up a VAO for the mesh
	GLuint VAO;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	// create enums for VAO vertex attribute positions
	enum { AISCENE_VERTICES, AISCENE_NORMALS, AISCNE_UVS, AISCENE_TANGENTS, AISCENE_BITANGENTS, AISCENE_INDICES };

	// setup flags for the various vertex attributes
	bool hasVertices, hasNormals, hasUVs, hasTangents;
	hasVertices = mesh->HasPositions();
	hasNormals = mesh->HasNormals();
	hasUVs = mesh->HasTextureCoords(0);
	hasTangents = mesh->HasTangentsAndBitangents();

	if (hasVertices == false) {
		// cant create a mesh without vertices... oops
		std::cout << "Attempt to create a mesh with no vertices" << std::endl;
	}

	std::vector<GLuint> faceArray;
	
	// for each face
	for(GLuint i=0; i < mesh->mNumFaces; ++i)
	{
		const aiFace face = mesh->mFaces[i];
		// add face to array
		faceArray.push_back(face.mIndices[0]);
		faceArray.push_back(face.mIndices[1]);
		faceArray.push_back(face.mIndices[2]);
	}

	// generate and set up the VBOs for the data
	GLuint VBOlist[6];
	glGenBuffers(6, VBOlist);
	
	
	// VBO for vertex data
	glBindBuffer(GL_ARRAY_BUFFER, VBOlist[0]);
	glBufferData(GL_ARRAY_BUFFER, 3*mesh->mNumVertices * sizeof(GL_FLOAT), mesh->mVertices, GL_STATIC_DRAW); // 3*3 represents 3 vertices with 3 floats each XYZ
	glVertexAttribPointer(AISCENE_VERTICES, 3, GL_FLOAT, GL_FALSE, 0, NULL); 
	glEnableVertexAttribArray(AISCENE_VERTICES);
			
	// VBO for normal data
	if (hasNormals == true) {
		glBindBuffer(GL_ARRAY_BUFFER, VBOlist[1]);
		glBufferData(GL_ARRAY_BUFFER, 3*mesh->mNumVertices * sizeof(GLfloat), mesh->mNormals, GL_STATIC_DRAW);
		glVertexAttribPointer(AISCENE_NORMALS, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AISCENE_NORMALS);
	}

	// VBO for tex-coord data
	if (hasUVs == true) {
		glBindBuffer(GL_ARRAY_BUFFER, VBOlist[2]);
		glBufferData(GL_ARRAY_BUFFER, 3*mesh->mNumVertices  * sizeof(GLfloat), mesh->mTextureCoords[0], GL_STATIC_DRAW);
		glVertexAttribPointer(AISCNE_UVS, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AISCNE_UVS);
	}

	// VBO for tangents
	if (hasTangents == true) {
		glBindBuffer(GL_ARRAY_BUFFER, VBOlist[3]);
		glBufferData(GL_ARRAY_BUFFER, 3*mesh->mNumVertices  * sizeof(GLfloat), mesh->mTangents, GL_STATIC_DRAW);
		glVertexAttribPointer(AISCENE_TANGENTS, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AISCENE_TANGENTS);
	}

	// VBO for biTangents
	if (hasTangents == true) {
		glBindBuffer(GL_ARRAY_BUFFER, VBOlist[4]);
		glBufferData(GL_ARRAY_BUFFER, 3*mesh->mNumVertices  * sizeof(GLfloat), mesh->mBitangents, GL_STATIC_DRAW);
		glVertexAttribPointer(AISCENE_BITANGENTS, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AISCENE_BITANGENTS);
	}
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOlist[5]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->mFaces->mNumIndices * mesh->mNumFaces * sizeof(GLuint), faceArray.data(), GL_STATIC_DRAW);
	
	//glDeleteBuffers(6,VBOlist);
	
	// unbind vertex array
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	
	// return the identifier needed to draw this mesh
	*indexCount = mesh->mNumFaces*mesh->mFaces->mNumIndices;
	*meshID = VAO;
}


GLuint AbstractRenderable::getVAO()
{
	return m_vertexArrayObject;
}


GLuint AbstractRenderable::getVAO2()
{
	return m_vertexArrayObject2;
}


void AbstractRenderable::setModelPath(const std::string& path)
{
	m_modelPath = path;
}


std::string AbstractRenderable::getModelPath()
{
	return m_modelPath;
}


void AbstractRenderable::setTexturePath(const std::string& path)
{
	m_texturePath = path;
}


std::string AbstractRenderable::getTexturePath()
{
	return m_texturePath;
}


GLuint AbstractRenderable::getTexture()
{
	return m_textureID;
}


GLuint AbstractRenderable::getNormalTexture()
{
	return m_normalTextureID;
}


GLuint AbstractRenderable::getSpecularTexture()
{
	return m_specularTextureID;
}


GLuint AbstractRenderable::getTexture2()
{
	return m_textureID2;
}


GLuint AbstractRenderable::getNormalTexture2()
{
	return m_normalTextureID2;
}


GLuint AbstractRenderable::getSpecularTexture2()
{
	return m_specularTextureID2;
}


GLuint AbstractRenderable::getIndexCount()
{
	return m_indexCount;
}


GLuint AbstractRenderable::getIndexCount2()
{
	return m_indexCount2;
}


GLuint AbstractRenderable::loadImage(const std::string& path)
{
	std::string fname = "assets/textures/" + path;
	
	GLuint texID;
	glGenTextures(1, &texID); // generate texture ID

	// load file - using core SDL library
	SDL_Surface *tmpSurface = nullptr;
	tmpSurface = SDL_LoadBMP(fname.c_str());
	if (!tmpSurface) {
		std::cout << "Error loading bitmap" << std::endl;
	}

	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, texID);
	//glEnable(GL_TEXTURE_2D);
	
	SDL_PixelFormat *format = tmpSurface->format;

	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA8;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB8;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}
	//glTexStorage2D(GL_TEXTURE_2D, 6, GL_SRGB8_ALPHA8, tmpSurface->w, tmpSurface->h);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, tmpSurface->w, tmpSurface->h, 0, externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	//glTexSubImage2D(GL_TEXTURE_2D,0,0,0,tmpSurface->w,tmpSurface->h, externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16);
	

	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer
	return texID;	// return value of texture ID
}


bool AbstractRenderable::setModelMatrix(const glm::mat4& modelMatrix)
{
	m_modelMatrix = modelMatrix;
	return true;
}


glm::mat4 AbstractRenderable::getModelMatrix()
{
	return m_modelMatrix;
}


void AbstractRenderable::updateModelMatrix()
{
	glm::mat4 temp(1.0f);
	temp = glm::translate(temp,m_position);
	temp = glm::rotate(temp,m_orientation.x,glm::vec3(1.0f,0.0f,0.0f));
	temp = glm::rotate(temp,m_orientation.y,glm::vec3(0.0f,1.0f,0.0f));
	temp = glm::rotate(temp,m_orientation.z,glm::vec3(0.0f,0.0f,1.0f));
	m_modelMatrix = temp;
}


void AbstractRenderable::setPhase(const GLuint phase)
{
	m_phase = phase;
}


GLuint AbstractRenderable::getPhase()
{
	return m_phase;
}


bool AbstractRenderable::setOcclusion(const bool occlusion)
{
	m_isOccluded = occlusion;
	return true;
}


bool AbstractRenderable::getOcculusion()
{
	return m_isOccluded;
}


void AbstractRenderable::setSolid(const bool collideable)
{
	m_isSolid = collideable;
}


bool AbstractRenderable::getSolid()
{
	return m_isSolid;
}


bool AbstractRenderable::setPosition(const glm::vec3& position)
{
	m_position = position;
	updateModelMatrix();
	return true;
}


glm::vec3 AbstractRenderable::getPosition()
{
	return m_position;
}


bool AbstractRenderable::setOrientation(const glm::vec3& orientation)
{
	m_orientation = orientation;
	updateModelMatrix();
	return true;
}


glm::vec3 AbstractRenderable::getOrientation()
{
	return m_orientation;
}


void AbstractRenderable::setVelocity(const glm::vec3& iVelocity)
{
	m_velocity = iVelocity;
}


glm::vec3 AbstractRenderable::getVelocity()
{
	return m_velocity;
}


GLfloat AbstractRenderable::getMass()
{
	return m_mass;
}


bool AbstractRenderable::getSphereCollider()
{
	return m_isSphereCollider;
}


glm::vec3 AbstractRenderable::getHalfBounds()
{
	return m_halfBounds;
}

