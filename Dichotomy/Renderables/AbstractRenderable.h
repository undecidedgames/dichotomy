#pragma once

#include <vector>
#include <string>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>
#include <SDL.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>


class AbstractRenderable
{
		/* Object set up */
public:
	AbstractRenderable();
	virtual ~AbstractRenderable() = 0;

	void importFiletoAiScene(const std::string& pFile, GLuint* meshID, GLuint* indexCount);
	void setupVAOfromAiScene(const aiScene* aiScene, GLuint* meshID, GLuint* indexCount);

	std::string getModelPath();
	std::string getTexturePath();
	void setModelPath(const std::string& path);

	GLuint getVAO();
	GLuint getVAO2();

	GLuint getIndexCount();
	GLuint getIndexCount2();

protected:
	std::string m_modelPath;

	GLuint m_vertexArrayObject;
	GLuint m_vertexArrayObject2;

	GLuint m_indexCount;
	GLuint m_indexCount2;

		/* Object Texture + Model set up */
public:
	GLuint loadImage(const std::string& path);

	void setTexturePath(const std::string& path);
	
	GLuint getTexture();

	GLuint getNormalTexture();
	GLuint getSpecularTexture();

	GLuint getTexture2();
	GLuint getNormalTexture2();
	GLuint getSpecularTexture2();

protected:
	std::string m_texturePath;

	GLuint m_textureID;
	GLuint m_normalTextureID;
	GLuint m_specularTextureID;

	GLuint m_textureID2;
	GLuint m_normalTextureID2;
	GLuint m_specularTextureID2;

		/* Model Matrix */
public:
	bool setModelMatrix(const glm::mat4& modelMatrix);
	glm::mat4 getModelMatrix();

	void updateModelMatrix();

protected:
	glm::mat4 m_modelMatrix;

		/* Objects Scence Attributes */
public:
	bool setPosition(const glm::vec3& position);
	glm::vec3 getPosition();

	bool setOrientation(const glm::vec3& orientation);
	glm::vec3 getOrientation();

	void setVelocity(const glm::vec3& iVelocity);
	glm::vec3 getVelocity();

	glm::vec3 getHalfBounds();

	bool getSphereCollider();	

protected:
	glm::vec3 m_position;
	glm::vec3 m_orientation;
	glm::vec3 m_velocity;
	glm::vec3 m_halfBounds;

	bool m_isSphereCollider;	// Use sphere instead of AABB

		/* Objects Attributes */
public:
	void setPhase(const GLuint phase);
	GLuint getPhase();

	bool setOcclusion(bool occlusion);
	bool getOcculusion();

	void setSolid(bool collideable);
	bool getSolid();

	GLfloat getMass();
	GLfloat m_mass;

protected:
	GLuint m_phase;

	bool m_isOccluded;	// For frustum culling and lighting calculations
	bool m_isSolid;		// is the object collideable

};

